package models

import "testing"

func TestNewBalance(t *testing.T) {
	_ = NewBalance(0.1, 0.05)
}

func TestBoard(t *testing.T) {
	asks := []BoardBar{{
		Type:   Ask,
		Price:  150,
		Amount: 100,
	}, {
		Type:   Ask,
		Price:  120,
		Amount: 10000,
	}, {
		Type:   Ask,
		Price:  130,
		Amount: 100,
	}}
	bids := []BoardBar{{
		Type:   Bid,
		Price:  90,
		Amount: 100,
	}, {
		Type:   Bid,
		Price:  110,
		Amount: 10000,
	}, {
		Type:   Bid,
		Price:  60,
		Amount: 100,
	}}
	board := &Board{
		Asks: asks,
		Bids: bids,
	}
	bestBidPrice := board.BestBidPrice()
	if bestBidPrice != 110 {
		t.Errorf("failed to get BestBidPrice %f", bestBidPrice)
	}
	bestAskPrice := board.BestAskPrice()
	if bestAskPrice != 120 {
		t.Errorf("failed to get BestAskPrice %f", bestAskPrice)
	}
	bestBidAmount := board.BestBidAmount()
	if bestBidAmount != 10000 {
		t.Errorf("failed to get BestBidAmount %f", bestBidAmount)
	}
	bestAskAmount := board.BestAskAmount()
	if bestAskAmount != 10000 {
		t.Errorf("failed to get BestAskAmount %f", bestAskAmount)
	}

}
