package repository

import (
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
)

import "cloud.google.com/go/firestore"

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

func NewArbitragePairFirestoreRepository(cli *firestore.Client) (interfaces.ArbitragePairRepository, error) {
	return &ArbitragePairFirestoreRepository{
		cli: cli,
	}, nil
}

type ArbitragePairFirestoreRepository struct {
	cli            *firestore.Client
	arbitragePairs model.ArbitragePairs
}

func (r *ArbitragePairFirestoreRepository) GetAll(ctx context.Context) (model.ArbitragePairs, error) {
	if r.arbitragePairs != nil {
		return r.arbitragePairs, nil
	}
	res, err := r.cli.Collection("tradewolf/public/arbitrage_pair").Documents(ctx).GetAll()
	if err != nil {
		return nil, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}

	ret := make(model.ArbitragePairs, 0, len(res))
	for _, v := range res {
		ret = append(ret, model.ArbitragePair{
			ExchangeA: v.Data()["exchangeA"].(string),
			ExchangeB: v.Data()["exchangeB"].(string),
			CurrencyPair: model.CurrencyPair{
				Trading:    v.Data()["trading"].(string),
				Settlement: v.Data()["settlement"].(string),
			},
		})
	}
	r.arbitragePairs = ret
	return ret, nil
}
