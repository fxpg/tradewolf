package controller

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
)

type filterView struct {
	*tview.DropDown
}

type FilterView interface {
	Primitive() tview.Primitive
	SetDoneFunc(f func(tcell.Key)) *tview.DropDown
	SetSelectedFunc(handler func(text string, index int)) *tview.DropDown
	GetCurrentOption() (int, string)
}

func (n *filterView) Primitive() tview.Primitive {
	return n.DropDown
}

func (n *filterView) GetCurrentOption() (int, string) {
	return n.DropDown.GetCurrentOption()
}

func (n *filterView) SetDoneFunc(f func(tcell.Key)) *tview.DropDown {
	return n.DropDown.SetDoneFunc(f)
}

func (n *filterView) SetSelectedFunc(handler func(text string, index int)) *tview.DropDown {
	return n.DropDown.SetSelectedFunc(handler)
}

func NewFilterView() FilterView {
	n := &filterView{
		DropDown: tview.NewDropDown().SetLabel("Select a filter:").SetOptions(model.GetFilterOptions(), nil),
		// TextView: tview.NewTextView().SetTextAlign(tview.AlignLeft).SetDynamicColors(true),
	}
	n.SetTitleAlign(tview.AlignLeft)
	return n
}
