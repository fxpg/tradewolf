import {
  Module,
  VuexModule,
  Mutation,
  Action,
  getModule
} from 'vuex-module-decorators'
import { CurrencyPair, OrderBookTick, KeySetting } from '@/models/domain_pb'
import store from '@/stores/index'
import { ExchangeRepository } from '@/repositories/exchange'

@Module({ dynamic: true, store, namespaced: true, name: 'exchangeModule' })
export class exchangeModule extends VuexModule {
  orderBookTicks: OrderBookTick[] = Array<OrderBookTick>()
  keySettings: KeySetting[] = Array<KeySetting>()
  exchanges: string[] = Array<string>()

  get getExchanges(): string[] {
    return this.exchanges
  }

  get getKeySettings(): { [key: string]: KeySetting } {
    let ret: { [key: string]: KeySetting } = {}
    this.keySettings.forEach((ks: KeySetting) => {
      ret[ks.getExchange()] = ks
    })
    return ret
  }
  get getOrderBookTicks(): {
    [key: string]: { [key: string]: OrderBookTick[] }
  } {
    const payload: { [key: string]: { [key: string]: OrderBookTick[] } } = {}
    const orderBookTicks = new Array<OrderBookTick>()
    for (const o of this.orderBookTicks) {
      orderBookTicks.push(o)
    }
    return payload
  }

  @Mutation
  public UPDATE_ORDERBOOKTICKS(orderBookTicks: OrderBookTick[]) {
    this.orderBookTicks = orderBookTicks
  }
  @Mutation
  public UPDATE_EXCHANGE_SETTING_KEYS(exchangeKeySettings: KeySetting[]) {
    this.keySettings = exchangeKeySettings
  }
  @Mutation
  public UPDATE_EXCHANGES(exchanges: string[]) {
    this.exchanges = exchanges
  }
  @Action({ commit: 'UPDATE_EXCHANGE_SETTING_KEYS' })
  public async setExchangeSettingKeys(exchanges: string[]) {
    let ret = new Array<KeySetting>()
    for (let exchange of exchanges) {
      try {
        const key = await ExchangeRepository.GetKey(exchange)
        ret.push(key)
      } catch (err) {
        //console.log(err)
      }
    }
    return ret
  }
  @Action({ commit: 'UPDATE_EXCHANGES' })
  public async setExchanges() {
    return await ExchangeRepository.GetExchanges()
  }
  @Action({ commit: 'UPDATE_ORDERBOOKTICKS' })
  public async setOrderBookTicks() {
    return await ExchangeRepository.GetOrderBookTicks()
  }
}

export const ExchangeModule = getModule(exchangeModule)
