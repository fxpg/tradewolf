import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './stores/index'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import HighchartsVue from 'highcharts-vue'
import stockInit from 'highcharts/modules/stock'
import Highcharts from 'highcharts'

import 'typeface-roboto'
//import 'material-design-icons-iconfont'
// import darkUnica from 'highcharts/themes/dark-unica'
import Wails from '@wailsapp/runtime'
//import Bridge from './wailsbridge'
stockInit(Highcharts)
// darkUnica(Highcharts)
Vue.use(HighchartsVue)
Vue.config.productionTip = false

Wails.Init(() => {
  new Vue({
    router,
    store,
    vuetify,
    i18n,
    render: h => h(App)
  }).$mount('#app')
})
