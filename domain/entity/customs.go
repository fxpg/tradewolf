package entity

import (
	"database/sql/driver"
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes"
)

func (ts *Timestamp) Scan(value interface{}) error {
	switch t := value.(type) {
	case time.Time:
		var err error
		ts.Timestamp, err = ptypes.TimestampProto(t)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Not a protobuf Timestamp")
	}
	return nil
}

func (ts Timestamp) Value() (driver.Value, error) {
	return ptypes.Timestamp(ts.Timestamp)
}

func (lt LogType) Value() (driver.Value, error) {
	return int64(lt), nil
}
func (lt *LogType) Scan(value interface{}) error {
	val := value.(int64)
	*lt = LogType(int(val))
	return nil
}
