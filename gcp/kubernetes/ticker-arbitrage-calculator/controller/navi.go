package controller

import (
	"fmt"
	"github.com/rivo/tview"
)

type naviView struct {
	*tview.TextView
}

type NaviView interface {
	UpdateView(g *gui)
	Primitive() tview.Primitive
}

func (n *naviView) Primitive() tview.Primitive {
	return n.TextView
}

func NewNaviView() NaviView {
	n := &naviView{
		TextView: tview.NewTextView().SetTextAlign(tview.AlignLeft).SetDynamicColors(true),
	}
	n.SetTitleAlign(tview.AlignLeft)
	return n
}

func (n *naviView) UpdateView(g *gui) {
	go g.App.QueueUpdateDraw(func() {
		switch g.CurrentPanelKind() {
		case InputPanel:
			n.SetText(fmt.Sprintf("%s", switchNavi))
		case ArbitragedListPanel:
			n.SetText(fmt.Sprintf("%s, %s, %s", moveNavi, switchNavi, helps[ArbitragedListPanel]))
		default:
			n.SetText("")
		}
	})
}

var (
	moveNavi   = "[red::b]j[white]: move down, [red]k[white]: move up, [red]h[white]: move left, [red]l[white]: move right, [red]g[white]: move to top, [red]G[white]: move to bottom, [red]Ctrl-f[white]: next page [red]Ctrl-b[white]: previous page, [red]Ctrl-c[white]: stop pst"
	switchNavi = `[red::b]Tab[white]: next panel, [red]Shift-Tab[white]: previous panel`
)

var helps = map[int]string{
	InputPanel:          ``,
	ArbitragedListPanel: `[red]K[white]: kill process`,
}
