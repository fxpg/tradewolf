package controller

import (
	"github.com/rivo/tview"
	"strings"
)

type logView struct {
	*tview.TextView
	linesStr string
}

type LogView interface {
	UpdateView(g *gui)
	Primitive() tview.Primitive
	AddText(lines []string)
}

func (n *logView) Primitive() tview.Primitive {
	return n.TextView
}

func NewLogView() LogView {
	n := &logView{
		TextView: tview.NewTextView().SetTextAlign(tview.AlignLeft).SetDynamicColors(true),
		linesStr: "",
	}
	n.SetTitle("Logs").SetTitleAlign(tview.AlignCenter).SetBorder(true)
	return n
}
func (n *logView) AddText(lines []string) {
	existingLines := n.TextView.GetText(true)
	existingLines = existingLines + strings.Join(lines, "\n")
	n.linesStr = existingLines
}

func (n *logView) UpdateView(g *gui) {
	go g.App.QueueUpdateDraw(func() {
		n.TextView.SetText(n.linesStr)
	})
}
