package persistence

import (
	"github.com/bluele/slack"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
	"go.uber.org/zap"
	"sync"
)

var mtx sync.Mutex

type slackClient struct {
	channel string
	client  *slack.Slack
	logger  *zap.SugaredLogger
}

func (s *slackClient) Send(message string) error {
	logger.Get().Info(message)
	return s.client.ChatPostMessage(s.channel, message, nil)
}

func (s *slackClient) BulkSend(messages []string) error {
	mtx.Lock()
	defer mtx.Unlock()
	slackMessage := "```"
	for _, m := range messages {
		slackMessage += m + "\n"
		s.logger.Info(m)
	}
	slackMessage += "```"
	return s.client.ChatPostMessage(s.channel, slackMessage, nil)
}
