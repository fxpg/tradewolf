package usecase

import (
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"

import "context"

func NewGetWithdrawalFeesUsecase(
	repo interfaces.WithdrawalFeeRepository,
	logger utils.Logger,
) *GetWithdrawalFeesUsecase {
	return &GetWithdrawalFeesUsecase{
		repo: repo,
		logger: logger,
	}
}

type GetWithdrawalFeesUsecase struct {
	repo interfaces.WithdrawalFeeRepository
	logger utils.Logger
}

func (u *GetWithdrawalFeesUsecase) Exec(ctx context.Context, exchange string) (model.WithdrawalFees, error) {
	pairs, err := u.repo.GetByExchange(ctx, exchange)
	if err != nil {
		return nil, err
	}

	return pairs, nil
}
