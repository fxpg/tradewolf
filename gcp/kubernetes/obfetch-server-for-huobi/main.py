from huobi import subscribe
import asyncio
import ccxt
import redis
import os

REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
REDIS_PORT = os.environ.get("REDIS_PORT", 6379)
cli = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=0)

class RedisWriter:
    def __init__(self, redis_client, trading, settlement):
        self.redis_client = redis_client
        self.trading = trading
        self.settlement = settlement

    async def depth_callback(self, data):
        pipe = self.redis_client.pipeline()
        pipe.delete("huobi:"+self.trading+":"+self.settlement+":asks")
        for a in data["tick"]["asks"]:
            pipe.zadd("huobi:"+self.trading+":"+self.settlement+":asks", {str(a[1]): str(a[0])})
        pipe.delete("huobi:"+self.trading+":"+self.settlement+":bids")
        for b in data["tick"]["bids"]:
            pipe.zadd("huobi:"+self.trading+":"+self.settlement+":bids", {str(b[1]): str(b[0])})
        pipe.execute()

        # print(data)

# initalize redis
keys = cli.keys("huobi:*")
pipe = cli.pipeline()
for k in keys:
    pipe.delete(k)
# initilize redis end

huobipro = ccxt.huobipro()
huobi_markets = huobipro.load_markets()

subscribe_dict = {}
for k in huobi_markets.keys():
    rw = RedisWriter(cli, huobi_markets[k]["base"], huobi_markets[k]["quote"])
    symbol_id = huobi_markets[k]["id"]
    subscribe_dict["market."+symbol_id+".depth.step0"] = { "callback" : rw.depth_callback}
task = subscribe(subscribe_dict)
asyncio.get_event_loop().run_until_complete(task)


