package usecase

var Providers = []interface{}{
	NewGetArbitragePairsUsecase,
	NewGetOrderBookByExchangeUsecase,
	NewAuthenticateUsecase,
	NewCheckCallAllowanceUsecase,
	NewUpdateNonceUsecase,
	NewGetWithdrawalFeesUsecase,
}
