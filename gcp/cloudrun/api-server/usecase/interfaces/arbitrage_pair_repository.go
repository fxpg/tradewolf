package interfaces

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

type ArbitragePairRepository interface {
	GetAll(ctx context.Context) (model.ArbitragePairs, error)
}
