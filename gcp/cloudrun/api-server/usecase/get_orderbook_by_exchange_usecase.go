package usecase

import (
	"context"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"golang.org/x/sync/errgroup"
)

func NewGetOrderBookByExchangeUsecase(
	arbitragePairRepository interfaces.ArbitragePairRepository,
	orderBookRepository interfaces.OrderBookRepository,
) *GetOrderBookByExchangeUsecase {
	return &GetOrderBookByExchangeUsecase{
		arbitragePairRepository: arbitragePairRepository,
		orderBookRepository: orderBookRepository,
	}
}

type GetOrderBookByExchangeUsecase struct {
	arbitragePairRepository interfaces.ArbitragePairRepository
	orderBookRepository interfaces.OrderBookRepository
}

func (u *GetOrderBookByExchangeUsecase) Exec(ctx context.Context, exchange string) ([]*model.OrderBook, error) {
	if exchange == "" {
		return nil, qerror.New(model.ErrSystemInvalidArgs, "exchange")
	}

	arbitragePairs, err := u.arbitragePairRepository.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	currencyPairs := arbitragePairs.GetCurrencyPairsByExchange(exchange)
	orderBooks := make([]*model.OrderBook, 0)
	eg, childCtx := errgroup.WithContext(ctx)
	ch := make(chan *model.OrderBook, len(currencyPairs))
	for _, currencyPair := range currencyPairs {
		cp := currencyPair
		eg.Go(func() error {
			orderBook, err := u.orderBookRepository.Get(childCtx, exchange, cp)
			ch <- orderBook
			return err
		})
	}
	err = eg.Wait()
	close(ch)
	if err != nil {
		return nil, err
	}

	for orderBook := range ch {
		orderBooks = append(orderBooks, orderBook)
	}

	return orderBooks, nil
}
