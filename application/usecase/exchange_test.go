package usecase

import (
	"testing"

	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
)

func TestExchangeUsecase_GetPairs(t *testing.T) {
	usecase := NewExchangeUsecase(NewPublicRepositoryMock(t), NewPrivateRepositoryMock(t), NewSettingRepositoryMock(t), &config.Config{
		Exchanges: []string{"kucoin", "binance"},
	})
	pairs, err := usecase.GetPairs("kucoin")
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if len(pairs) == 0 {
		t.Fatalf("failed test %#v", pairs)
	}
	boards, err := usecase.GetBoard("kucoin", &entity.CurrencyPair{Trading: "ETH", Settlement: "BTC"})
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if boards == nil {
		t.Fatalf("failed test %#v", boards)
	}
}
