package model

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type FilterType struct{ value string }

var FILTER_IS_PROFIT_NOW = FilterType{value: "利益有(現在)"}
var FILTER_IS_PROFITTED = FilterType{value: "利益有(全期間)"}
var FILTER_IS_SAME_TRADING_DETECTED = FilterType{value: "複数Trading検知"}

func NewFilterType(s string) FilterType {
	return FilterType{s}
}

func GetFilterOptions() []string {
	return []string{"", FILTER_IS_PROFIT_NOW.value, FILTER_IS_PROFITTED.value, FILTER_IS_SAME_TRADING_DETECTED.value}
}

type SortType int

const (
	SORT_TRIGGER_DATETIME SortType = iota
	SORT_TRIGGER_DATETIME_DESC
	SORT_ISPROFIT
)

type CurrencyPair struct {
	Trading    string
	Settlement string
	Exchange   string
}

type BoardTicker struct {
	Trading       string
	Settlement    string
	Exchange      string
	BestBidPrice  float64
	BestBidVolume float64
	BestAskPrice  float64
	BestAskVolume float64
}

type Arbitrage struct {
	ArbitragePair ArbitragePair
	BestAskA      float64
	BestAskB      float64
	BestBidA      float64
	BestBidB      float64
	Time          time.Time
}

type ArbitrageWatchCondition struct {
	ExpectedProfitRatio  float64
	ExpectedLossCutRatio float64
	Duration             time.Duration
}

type ArbitrageWatchSetting struct {
	ArbitragePair  ArbitragePair
	TriggeredTime  time.Time
	LastUpdateTime time.Time
	LimitTime      time.Time

	BuySide                string
	ArbitrageRatio         float64
	NowBuySideBestBidPrice float64
	InitialBuyPrice        float64
	ExpectedProfitRatio    float64
	ExpectedLossCutRatio   float64

	IsProfitNow bool
	IsProfitted bool

	MaxBestBidPrice float64
}

func (aws *ArbitrageWatchSetting) InfoString() string {
	lines := make([]string, 0)
	lines = append(lines, fmt.Sprintf("CurrencyPair\t:%s", aws.ArbitragePair.Trading+"/"+aws.ArbitragePair.Settlement))
	lines = append(lines, fmt.Sprintf("ExchangeA\t\t:%s", aws.ArbitragePair.ExchangeA))
	lines = append(lines, fmt.Sprintf("ExchangeB\t\t:%s", aws.ArbitragePair.ExchangeB))
	lines = append(lines, fmt.Sprintf("BuySide\t\t:%s", aws.BuySide))
	lines = append(lines, fmt.Sprintf("InitialArbitrage\t:%s", strconv.FormatFloat(aws.ArbitrageRatio, 'f', 8, 64)))
	lines = append(lines, fmt.Sprintf("NowArbitrage\t:%s", strconv.FormatFloat(aws.NowBuySideBestBidPrice/aws.InitialBuyPrice, 'f', 8, 64)))

	lines = append(lines, fmt.Sprintf("TriggerDateTime\t:%s", aws.TriggeredTime.Format("2006/01/02 15:04:05")))
	lines = append(lines, fmt.Sprintf("LastUpdateTime\t:%s", aws.LastUpdateTime.Format("2006/01/02 15:04:05")))

	return strings.Join(lines, "\n")
}

type ArbitragePair struct {
	Trading    string
	Settlement string
	ExchangeA  string
	ExchangeB  string
}
