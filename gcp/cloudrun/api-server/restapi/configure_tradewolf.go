// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"github.com/go-openapi/errors"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils/journal"
	"log"
	"net/http"
	"time"

	runtime "github.com/go-openapi/runtime"
	middleware "github.com/go-openapi/runtime/middleware"
	"go.uber.org/dig"

	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/config"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/controller"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/infra/driver"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/infra/repository"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/restapi/operations"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"

	_ "net/http/pprof"
)

//go:generate swagger generate server --target ../../api-server --name Tradewolf --spec ../swagger.yaml

func configureFlags(api *operations.TradewolfAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.TradewolfAPI) http.Handler {
	c := dig.New()

	// 初期化タイムアウト
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c.Provide(func() context.Context {
		return ctx
	})
	c.Provide(config.LoadConfig)
	c.Provide(driver.ConnectToRedis)
	c.Provide(driver.ConnectToFirestore)
	c.Provide(utils.NewStdLogger)
	for _, p := range usecase.Providers {
		c.Provide(p)
	}
	for _, p := range controller.Providers {
		c.Provide(p)
	}
	for _, p := range repository.Providers {
		c.Provide(p)
	}

	err := c.Invoke(func(
		in struct {
			dig.In
			ArbitragePairsController *controller.ArbitragePairsController
			OrderBooksController     *controller.OrderBooksController
			WithdrawalFeesController *controller.WithdrawalFeesController
			Logger                   utils.Logger
		},
	) {
		// configure the api here
		api.ServeError = func(w http.ResponseWriter, r *http.Request, err error) {
			in.Logger.Errorf(err.Error())
			errors.ServeError(w, r, err)
		}

		// Set your custom logger if needed. Default one is log.Printf
		// Expected interface func(string, ...interface{})
		//
		// Example:
		// api.Logger = log.Printf

		api.JSONConsumer = runtime.JSONConsumer()

		api.JSONProducer = runtime.JSONProducer()

		api.GetArbitragePairsHandler = operations.GetArbitragePairsHandlerFunc(in.ArbitragePairsController.Get)
		api.GetOrderBookByExchangeHandler = operations.GetOrderBookByExchangeHandlerFunc(in.OrderBooksController.Get)
		api.GetWithdrawalFeesByExchangeHandler = operations.GetWithdrawalFeesByExchangeHandlerFunc(in.WithdrawalFeesController.Get)

		if api.GetWithdrawalFeesByExchangeHandler == nil {
			api.GetWithdrawalFeesByExchangeHandler = operations.GetWithdrawalFeesByExchangeHandlerFunc(func(params operations.GetWithdrawalFeesByExchangeParams) middleware.Responder {
				return middleware.NotImplemented("operation .GetWithdrawalFeesByExchange has not yet been implemented")
			})
		}

		api.ServerShutdown = func() {}
	})
	if err != nil {
		log.Fatal(err)
	}

	// ジャーナルライター
	c.Invoke(func(cfg *config.Config) {
		switch cfg.Mode {
		case config.Production:
		case config.Debug:
			// TODO: Productionのログ出力先の変更(stackdriver?)
			c.Provide(journal.NewHttpRequestJournalLogWriter)
			c.Provide(journal.NewHttpResponseJournalLogWriter)
		default:
			panic("unknown mode")
		}
	})

	// Middlewareの登録
	c.Provide(middlewares.NewAuthenticateMiddleware, dig.Name("authenticateMiddleware"))
	c.Provide(middlewares.NewCheckCallAllowanceMiddleware, dig.Name("checkCallAllowanceMiddleware"))
	c.Provide(middlewares.NewUpdateNonceAuthenticateMiddleware, dig.Name("updateNonceMiddleware"))
	c.Provide(middlewares.NewDoNothingMiddleware, dig.Name("doNothingMiddleware"))
	c.Provide(func() middlewares.Middleware {
		return middlewares.NewRequestTimeoutMiddleware(5 * time.Second)
	}, dig.Name("requestTimeoutMiddleware"))
	c.Provide(middlewares.NewRequestIdMiddleware, dig.Name("requestIdMiddleware"))
	c.Provide(func(
		reqW middlewares.HttpRequestJournalWriter,
		resW middlewares.HttpResponseJournalWriter,
	) (middlewares.Middleware, error) {
		return middlewares.NewHttpJournalMiddleware(reqW, resW,
			[]string{"application/json"}, []string{"application/json"})
	}, dig.Name("httpJournalMiddleware"))
	var mid middleware.Builder
	err = c.Invoke(func(
		in struct {
			dig.In
			AuthenticateMiddleware       middlewares.Middleware `name:"authenticateMiddleware"`
			CheckCallAllowanceMiddleware middlewares.Middleware `name:"checkCallAllowanceMiddleware"`
			UpdateNonceMiddleware        middlewares.Middleware `name:"updateNonceMiddleware"`
			DoNothingMiddleware          middlewares.Middleware `name:"doNothingMiddleware"`
			RequestTimeoutMiddleware     middlewares.Middleware `name:"requestTimeoutMiddleware"`
			RequestIdMiddleware          middlewares.Middleware `name:"requestIdMiddleware"`
			HttpJournalMiddleware        middlewares.Middleware `name:"httpJournalMiddleware"`
		},
	) {
		// 上から順番に実行
		ms := []middlewares.Middleware{
			in.DoNothingMiddleware,
			in.RequestTimeoutMiddleware,
			in.RequestIdMiddleware,
			in.HttpJournalMiddleware,
			in.AuthenticateMiddleware,
			in.CheckCallAllowanceMiddleware,
			// in.UpdateNonceMiddleware,
		}
		mid = createMiddleware(ms)
	})
	if err != nil {
		log.Fatal(err)
	}

	c.Invoke(func(c *config.Config, logger utils.Logger) {
		if c.Mode == config.Debug {
			// 開発時はプロファイラを起動
			go func() {
				logger.Printf("profile server starting: http://localhost:6060/debug/pprof")
				log.Println(http.ListenAndServe("localhost:6060", nil))
			}()
		}
	})

	return mid(api.Serve(emptyMiddleware))
}

func createMiddleware(ms []middlewares.Middleware) middleware.Builder {
	return func(handler http.Handler) http.Handler {
		// msを逆順にループ
		for i := len(ms) - 1; i >= 0; i-- {
			handler = ms[i](handler)
		}
		return handler
	}
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

func emptyMiddleware(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
