package util

import "math"

func Round(val float64, places int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(places))
	digit := pow * val
	round = math.Floor(digit)
	newVal = round / pow
	return
}
