package repository

import (
	"context"
	"sync"

	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
)

func NewArbitragePairCachedRepository(src interfaces.ArbitragePairRepository) interfaces.ArbitragePairRepository {
	return &ArbitragePairCachedRepository{
		src:            src,
		once:           new(sync.Once),
		arbitragePairs: nil,
	}
}

type ArbitragePairCachedRepository struct {
	src            interfaces.ArbitragePairRepository
	once           *sync.Once
	arbitragePairs model.ArbitragePairs
}

func (r *ArbitragePairCachedRepository) GetAll(ctx context.Context) (model.ArbitragePairs, error) {
	var reterr error
	if r.arbitragePairs == nil {
		r.once.Do(func() {
			res, err := r.src.GetAll(ctx)
			if err != nil {
				reterr = err
				return
			}
			r.arbitragePairs = res
		})
	}
	return r.arbitragePairs, reterr
}
