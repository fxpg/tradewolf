export = wailsapp__runtime

declare const wailsapp__runtime: {
  Events: {
    Acknowledge(eventName: string): void
    Emit(eventName: string): void
    Heartbeat(
      eventName: string,
      timeInMilliseconds: number,
      callback: () => void
    ): void
    On(eventName: string, callback: (...messages: string[]) => void): void
    OnMultiple(
      eventName: string,
      callback: () => void,
      maxCallbacks: number
    ): void
    Once(eventName: string, callback: () => void): void
  }
}
