package controller

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
)

type arbitrageInfoView struct {
	*tview.TextView
}

type ArbitrageInfoView interface {
	UpdateView(g *gui)
	Primitive() tview.Primitive
	SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey) *tview.Box
}

func (n *arbitrageInfoView) SetInputCapture(f func(event *tcell.EventKey) *tcell.EventKey) *tview.Box {
	return n.TextView.SetInputCapture(f)
}

func (n *arbitrageInfoView) Primitive() tview.Primitive {
	return n.TextView
}

func NewArbitrageInfoView() ArbitrageInfoView {
	n := &arbitrageInfoView{
		TextView: tview.NewTextView().SetTextAlign(tview.AlignLeft).SetDynamicColors(true),
	}
	n.SetTitleAlign(tview.AlignCenter).SetTitle("Arbitrage Info").SetBorder(true)
	return n
}

func (n *arbitrageInfoView) UpdateView(g *gui) {
	arbitrage := g.ArbitrageWatchManager.SelectedOnWatchingTable()
	if arbitrage != nil {
		n.updateViewWithArbitrage(g, arbitrage)
	}
}

func (n *arbitrageInfoView) updateViewWithArbitrage(g *gui, arbitrage *model.ArbitrageWatchSetting) {
	text := ""
	if arbitrage != nil {
		text = arbitrage.InfoString()
	}
	go g.App.QueueUpdateDraw(func() {
		n.TextView.SetText(text).ScrollToBeginning()
	})
}
