package usecase

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

func NewAuthenticateUsecase(
	repo interfaces.KeyPairRepository,
	logger utils.Logger,
) *AuthenticateUsecase {
	return &AuthenticateUsecase{
		repo:   repo,
		logger: logger,
	}
}

type AuthenticateUsecase struct {
	repo   interfaces.KeyPairRepository
	logger utils.Logger
}

func (u *AuthenticateUsecase) Exec(ctx context.Context, apiKey string, apiSignature string, apiTimestamp string, body string, path string, method string) error {
	if apiKey ==""||apiSignature==""||apiTimestamp=="" {
		return qerror.New(model.ErrLogicRequestHeaderMissing)
	}
	inputSignByte, err := hex.DecodeString(apiSignature)
	if err != nil {
		return qerror.New(model.ErrLogicSignatureInvalid)
	}
	message := apiTimestamp + path + method + body
	secretKeyFromFirestore, err := u.repo.GetSecretKeyByApiKey(ctx, apiKey)
	if err != nil {
		fmt.Errorf(err.Error())
		return qerror.WrapWith(err,model.ErrSystemFirestoreRequestFailed, "secret key get failed")
	}
	mac := hmac.New(sha256.New, []byte(secretKeyFromFirestore))
	mac.Write([]byte(message))
	validSignByte := mac.Sum(nil)
	if !hmac.Equal(inputSignByte,validSignByte){
		return qerror.New(model.ErrLogicSignatureInvalid)
	}
	return nil
}
