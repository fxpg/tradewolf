package config

import "os"

import "strconv"

import "log"

type Mode int
const (
	Debug Mode = iota
	Production
)

// Config : api server configuration
type Config struct {
	Mode Mode
	RedisConfig struct {
		Host     string
		Port     int
		Password string
		DB       int
	}
	FireStoreConfig struct {
		ProjectID string
	}
}

func getEnvOrDefault(key string, defaultValue string) string {
	value, ok := os.LookupEnv(key)
	if !ok {
		return defaultValue
	}
	return value
}

// LoadConfig : load configuration
func LoadConfig() *Config {
	c := &Config{}

	// Mode
	{
		mode := getEnvOrDefault("RUN_MODE", "debug")
		switch mode {
		case "production":
			c.Mode = Production
		case "debug":
			c.Mode = Debug
		default:
			c.Mode = Debug
		}
	}

	// RedisConfig
	{
		host := getEnvOrDefault("REDIS_HOST", "localhost")
		portStr := getEnvOrDefault("REDIS_PORT", "6379")
		password := getEnvOrDefault("REDIS_PASSWORD", "")
		dbStr := getEnvOrDefault("REDIS_DB", "0")

		port, err := strconv.Atoi(portStr)
		if err != nil {
			log.Fatalf("redis port is not integer: %s", portStr)
		}

		db, err := strconv.Atoi(dbStr)
		if err != nil {
			log.Fatalf("redis db is not integer: %s", dbStr)
		}

		c.RedisConfig.Host = host
		c.RedisConfig.Port = port
		c.RedisConfig.Password = password
		c.RedisConfig.DB = db
	}

	// FireStoreConfig
	{
		projectID, ok := os.LookupEnv("GCP_PROJECT_ID")
		if !ok {
			log.Fatalf("env GCP_PROJECT_ID is empty")
		}

		c.FireStoreConfig.ProjectID = projectID
	}

	return c
}
