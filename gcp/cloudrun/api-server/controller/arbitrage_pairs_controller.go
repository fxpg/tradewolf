package controller

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/controller/mapper"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/restapi/operations"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

func NewArbitragePairsController(
	u *usecase.GetArbitragePairsUsecase,
	logger utils.Logger,
) *ArbitragePairsController {
	return &ArbitragePairsController{
		u: u,
		logger: logger,
	}
}

type ArbitragePairsController struct {
	u *usecase.GetArbitragePairsUsecase
	logger utils.Logger
}

func (c *ArbitragePairsController) Get(params operations.GetArbitragePairsParams) middleware.Responder {
	res, err := c.u.Exec(params.HTTPRequest.Context())
	if err != nil {
		if model.IsSystemError(err) {
			c.logger.Errorf(err.Error())
		}
		code, resErr := toErrorResponse(err)
		return operations.NewGetArbitragePairsDefault(code).WithPayload(resErr)
	}

	return operations.NewGetArbitragePairsOK().WithPayload(mapper.MapArbitragePairs(res))
}
