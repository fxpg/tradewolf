package middlewares

import (
	"context"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares/util"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"net/http"
)

func NewCheckCallAllowanceMiddleware(uc *usecase.CheckCallAllowanceUsecase,
	logger utils.Logger) Middleware {
	return func(nextFunc http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// 日別・月別APIコール割当数を検証するミドルウェア
			ctx := r.Context()
			if ctx == nil {
				ctx = context.Background()
			}
			apiKey := r.Header.Get(requestApiKey)
			err := uc.Exec(ctx, apiKey, r.URL.Path)
			if err != nil {
				if model.IsSystemError(err) {
					logger.Errorf(err.Error())
				}
				util.WriteError(w, err)
				return
			}
			nextFunc.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
