package middlewares

import (
	"context"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares/util"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"net/http"
)

func NewUpdateNonceAuthenticateMiddleware(uc *usecase.UpdateNonceUsecase, logger utils.Logger) Middleware {
	return func(nextFunc http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// NONCEをアップデートするミドルウェア
			ctx := r.Context()
			if ctx == nil {
				ctx = context.Background()
			}
			apiKey := r.Header.Get(requestApiKey)
			apiTimestamp := r.Header.Get(requestApiTimestamp)
			err := uc.Exec(ctx, apiKey, apiTimestamp)
			if err != nil {
				if model.IsSystemError(err) {
					logger.Errorf(err.Error())
				}
				util.WriteError(w, err)
				return
			}
			nextFunc.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
