package registry

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/application/usecase"
	"gitlab.com/fxpg/tradewolf/infrastructure/api"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
	"gitlab.com/fxpg/tradewolf/infrastructure/persistence"
	"gitlab.com/fxpg/tradewolf/presenter/handler"
	"go.etcd.io/bbolt"
	"go.uber.org/dig"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func New(configPath string, db *gorm.DB, bboltDb *bbolt.DB) (*dig.Container, error) {
	c := dig.New()

	if err := c.Provide(func() *config.Config {
		conf := config.ReadConfig(configPath)
		return conf
	}); err != nil {
		return nil, errors.WithStack(err)
	}

	// Dbs
	if err := c.Provide(func() *gorm.DB {
		return db
	}); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(func() *bbolt.DB {
		return bboltDb
	}); err != nil {
		return nil, errors.WithStack(err)
	}

	// Repositories
	if err := c.Provide(api.NewPublicResourceRepository); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(api.NewPrivateResourceRepository); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(persistence.NewSettingRepository); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(persistence.NewArbitrageOrderRepository); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(persistence.NewArbitragerLogRepository); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(persistence.NewArbitragerSettingRepository); err != nil {
		return nil, errors.WithStack(err)
	}

	// UseCases
	if err := c.Provide(usecase.NewArbitrageUsecase); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(usecase.NewExchangeUsecase); err != nil {
		return nil, errors.WithStack(err)
	}

	// Handlers
	if err := c.Provide(handler.NewArbitrageHandler); err != nil {
		return nil, errors.WithStack(err)
	}
	if err := c.Provide(handler.NewExchangeHandler); err != nil {
		return nil, errors.WithStack(err)
	}

	return c, nil
}
