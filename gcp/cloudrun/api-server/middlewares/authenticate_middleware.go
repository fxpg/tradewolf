package middlewares

import (
	"bytes"
	"context"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares/util"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"io/ioutil"
	"net/http"
)

const (
	requestApiKey       = "X-API-KEY"
	requestApiSignature = "X-API-SIGNATURE"
	// TIMESTAMP: eg.1547015186532
	requestApiTimestamp = "X-API-TIMESTAMP"
)

func NewAuthenticateMiddleware(uc *usecase.AuthenticateUsecase,logger utils.Logger) Middleware {
	return func(nextFunc http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			// リクエストヘッダを紐解いてSECRET-KEYを検証するミドルウェア
			ctx := r.Context()
			if ctx == nil {
				ctx = context.Background()
			}
			buf, err := ioutil.ReadAll(r.Body)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			r.Body = ioutil.NopCloser(bytes.NewBuffer(buf))

			apiKey := r.Header.Get(requestApiKey)
			apiSignature := r.Header.Get(requestApiSignature)
			apiTimestamp := r.Header.Get(requestApiTimestamp)
			body := string(buf)
			err = uc.Exec(ctx, apiKey, apiSignature, apiTimestamp, body, r.URL.Path, r.Method)
			if err != nil {
				if model.IsSystemError(err) {
					logger.Errorf(err.Error())
				}
				util.WriteError(w, err)
				return
			}
			nextFunc.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}
