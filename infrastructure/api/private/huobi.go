package private

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/antonholmquist/jason"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/public"
)

const (
	HUOBI_BASE_URL = "https://api.huobi.pro"
)

func NewHuobiApi(apikey func() (string, error), apisecret func() (string, error)) (*HuobiApi, error) {
	hitbtcPublic, err := public.NewHuobiPublicApi()
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize public client")
	}
	pairs, err := hitbtcPublic.CurrencyPairs()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get pairs")
	}
	var settlements []string
	for _, v := range pairs {
		settlements = append(settlements, v.Settlement)
	}
	m := make(map[string]bool)
	uniq := []string{}
	for _, ele := range settlements {
		if !m[ele] {
			m[ele] = true
			uniq = append(uniq, ele)
		}
	}

	return &HuobiApi{
		BaseURL:           HUOBI_BASE_URL,
		RateCacheDuration: 30 * time.Second,
		ApiKeyFunc:        apikey,
		SecretKeyFunc:     apisecret,
		settlements:       uniq,
		rateMap:           nil,
		volumeMap:         nil,
		rateLastUpdated:   time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC),
		rt:                &http.Transport{},

		m: new(sync.Mutex),
	}, nil
}

type HuobiApi struct {
	ApiKeyFunc        func() (string, error)
	SecretKeyFunc     func() (string, error)
	BaseURL           string
	RateCacheDuration time.Duration
	HttpClient        http.Client
	rt                *http.Transport
	settlements       []string

	volumeMap       map[string]map[string]float64
	rateMap         map[string]map[string]float64
	transferFeeMap  *huobiTransferFeeSyncMap
	rateLastUpdated time.Time

	m *sync.Mutex
}

func (h *HuobiApi) privateApiUrl() string {
	return h.BaseURL
}

func (h *HuobiApi) privateApi(method string, path string, params *url.Values) ([]byte, error) {

	apiKey, err := h.ApiKeyFunc()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create request command %s", path)
	}
	secretKey, err := h.SecretKeyFunc()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to create request command %s", path)
	}
	params.Set("AccessKeyId", apiKey)
	params.Set("SignatureMethod", "HmacSHA256")
	params.Set("SignatureVersion", "2")
	params.Set("Timestamp", time.Now().UTC().Format("2006-01-02T15:04:05"))
	domain := strings.Replace(h.BaseURL, "https://", "", len(h.BaseURL))
	payload := fmt.Sprintf("%s\n%s\n%s\n%s", method, domain, path, params.Encode())
	sign, _ := GetParamHmacSHA256Base64Sign(secretKey, payload)
	params.Set("Signature", sign)
	urlStr := h.BaseURL + path + "?" + params.Encode()
	resBody, err := NewHttpRequest(&http.Client{}, method, urlStr, "", nil)
	return resBody, err
}

func (h *HuobiApi) TradeFeeRates() (map[string]map[string]TradeFee, error) {
	cli, err := public.NewClient("huobi")
	if err != nil {
		return nil, err
	}
	pairs, err := cli.CurrencyPairs()
	if err != nil {
		return nil, err
	}
	traderFeeMap := make(map[string]map[string]TradeFee)
	for _, p := range pairs {
		n := make(map[string]TradeFee)
		n[p.Settlement] = TradeFee{0.002, 0.002}
		traderFeeMap[p.Trading] = n
	}
	return traderFeeMap, nil
}

func (b *HuobiApi) TradeFeeRate(trading string, settlement string) (TradeFee, error) {
	feeMap, err := b.TradeFeeRates()
	if err != nil {
		return TradeFee{}, err
	}
	return feeMap[trading][settlement], nil
}

type HuobiTransferFeeResponse struct {
	response []byte
	Currency string
	err      error
}

type huobiTransferFeeMap map[string]float64
type huobiTransferFeeSyncMap struct {
	huobiTransferFeeMap
	m *sync.Mutex
}

func (sm *huobiTransferFeeSyncMap) Set(currency string, fee float64) {
	sm.m.Lock()
	defer sm.m.Unlock()
	sm.huobiTransferFeeMap[currency] = fee
}
func (sm *huobiTransferFeeSyncMap) GetAll() map[string]float64 {
	sm.m.Lock()
	defer sm.m.Unlock()
	return sm.huobiTransferFeeMap
}

func (h *HuobiApi) TransferFee() (map[string]float64, error) {
	if h.transferFeeMap != nil {
		return h.transferFeeMap.GetAll(), nil
	}
	transferFeeMap := huobiTransferFeeSyncMap{make(huobiTransferFeeMap), new(sync.Mutex)}
	transferFeeMap.Set("USDT", 1)
	transferFeeMap.Set("BTC", 0.0005)
	transferFeeMap.Set("ETH", 0.005)
	transferFeeMap.Set("EOS", 0.1)
	transferFeeMap.Set("HT", 0.1)
	transferFeeMap.Set("HPT", 89.40890000)
	transferFeeMap.Set("GUSD", 2)
	transferFeeMap.Set("TUSD", 2)
	transferFeeMap.Set("PAX", 2)
	transferFeeMap.Set("USDC", 2)
	transferFeeMap.Set("XRP", 0.1)
	transferFeeMap.Set("BCH", 0.00010000)
	transferFeeMap.Set("LTC", 0.00100000)
	transferFeeMap.Set("ETC", 0.03000000)
	transferFeeMap.Set("ADA", 1)
	transferFeeMap.Set("DASH", 0.00200000)
	transferFeeMap.Set("ZEC", 0.00100000)
	transferFeeMap.Set("NEO", 0)
	transferFeeMap.Set("TRX", 1)
	transferFeeMap.Set("QTUM", 0.01000000)
	transferFeeMap.Set("XEM", 4.00000000)
	transferFeeMap.Set("OMG", 0.60950000)
	transferFeeMap.Set("HC", 0.00500000)
	transferFeeMap.Set("LSK", 0.10000000)
	transferFeeMap.Set("DCR", 0.01000000)
	transferFeeMap.Set("BTG", 0.00100000)
	transferFeeMap.Set("STEEM", 0.01000000)
	transferFeeMap.Set("BTS", 1)
	transferFeeMap.Set("WAVES", 0.00200000)
	transferFeeMap.Set("SNT", 42.13450000)
	transferFeeMap.Set("SALT", 9.74160000)
	transferFeeMap.Set("GNT", 12.59590000)
	transferFeeMap.Set("CMT", 20.00000000)
	transferFeeMap.Set("BTM", 5)
	transferFeeMap.Set("PAY", 8.13980000)
	transferFeeMap.Set("KNC", 2.10070000)
	transferFeeMap.Set("POWR", 10.25150000)
	transferFeeMap.Set("BAT", 2.38490000)
	transferFeeMap.Set("DGD", 0.02210000)
	transferFeeMap.Set("VET", 150.00000000)
	transferFeeMap.Set("QASH", 8.32830000)
	transferFeeMap.Set("XZC", 0.10000000)
	transferFeeMap.Set("ZRX", 2.05100000)
	transferFeeMap.Set("GAS", 0.00500000)
	transferFeeMap.Set("MANA", 17.00450000)
	transferFeeMap.Set("ENG", 0.92230000)
	transferFeeMap.Set("CVC", 18.80250000)
	transferFeeMap.Set("MCO", 0.10010000)
	transferFeeMap.Set("MTL", 1.61430000)
	transferFeeMap.Set("RDN", 3.52110000)
	transferFeeMap.Set("STORJ", 3.94790000)
	transferFeeMap.Set("CHAT", 263.80180000)
	transferFeeMap.Set("LINK", 0.21100000)
	transferFeeMap.Set("SRN", 72.28080000)
	transferFeeMap.Set("ACT", 0.01000000)
	transferFeeMap.Set("TNB", 377.71790000)
	transferFeeMap.Set("QSP", 40.51430000)
	transferFeeMap.Set("REQ", 36.59270000)
	transferFeeMap.Set("PHX", 0.50000000)
	transferFeeMap.Set("APPC", 16.32360000)
	transferFeeMap.Set("RCN", 10.27210000)
	transferFeeMap.Set("SMT", 7.00000000)
	transferFeeMap.Set("TNT", 10.12360000)
	transferFeeMap.Set("OST", 35.93820000)
	transferFeeMap.Set("ITC", 4.08560000)
	transferFeeMap.Set("LUN", 0.52160000)
	transferFeeMap.Set("GNX", 63.41030000)
	transferFeeMap.Set("AST", 20.73990000)
	transferFeeMap.Set("EVX", 1.65470000)
	transferFeeMap.Set("MDS", 131.56640000)
	transferFeeMap.Set("SNC", 27.82240000)
	transferFeeMap.Set("PROPY", 5.77530000)
	transferFeeMap.Set("EKO", 326.89020000)
	transferFeeMap.Set("NAS", 0.20000000)
	transferFeeMap.Set("WAXP", 5.00000000)
	transferFeeMap.Set("TOPC", 73.67850000)
	transferFeeMap.Set("SWFTC", 446.77540000)
	transferFeeMap.Set("DBC", 5.00000000)
	transferFeeMap.Set("ELF", 7.65320000)
	transferFeeMap.Set("AIDOC", 124.40270000)
	transferFeeMap.Set("QUN", 95.30650000)
	transferFeeMap.Set("IOST", 1)
	transferFeeMap.Set("YEE", 490.54450000)
	transferFeeMap.Set("DAT", 538.76780000)
	transferFeeMap.Set("THETA", 10)
	transferFeeMap.Set("LET", 157.72140000)
	transferFeeMap.Set("DTA", 1620.85970000)
	transferFeeMap.Set("UTK", 29.93930000)
	transferFeeMap.Set("MEET", 42.29880000)
	transferFeeMap.Set("ZIL", 1)
	transferFeeMap.Set("SOC", 129.80410000)
	transferFeeMap.Set("RUFF", 78.23660000)
	transferFeeMap.Set("OCN", 901.01850000)
	transferFeeMap.Set("ELA", 0.00500000)
	transferFeeMap.Set("ZLA", 71.37980000)
	transferFeeMap.Set("STK", 230.43780000)
	transferFeeMap.Set("WPR", 64.21050000)
	transferFeeMap.Set("MTN", 142.94880000)
	transferFeeMap.Set("MTX", 23.42980000)
	transferFeeMap.Set("EDU", 5011.25710000)
	transferFeeMap.Set("BLZ", 23.04590000)
	transferFeeMap.Set("ABT", 3.33350000)
	transferFeeMap.Set("ONT", 1)
	transferFeeMap.Set("BFT", 35.39000000)
	transferFeeMap.Set("WAN", 0.10000000)
	transferFeeMap.Set("KAN", 224.68980000)
	transferFeeMap.Set("LBA", 21.76650000)
	transferFeeMap.Set("POLY", 19.46950000)
	transferFeeMap.Set("PAI", 5.00000000)
	transferFeeMap.Set("WTC", 0.95210000)
	transferFeeMap.Set("BOX", 133.76500000)
	transferFeeMap.Set("GXC", 0.50000000)
	transferFeeMap.Set("BIX", 4.54030000)
	transferFeeMap.Set("XLM", 0.01)
	transferFeeMap.Set("XVG", 0.1)
	transferFeeMap.Set("HIT", 9491.05910000)
	transferFeeMap.Set("NCASH", 501.63570000)
	transferFeeMap.Set("EGCC", 2016.65930000)
	transferFeeMap.Set("SHE", 616.93340000)
	transferFeeMap.Set("MEX", 1472.03140000)
	transferFeeMap.Set("IIC", 3753.87620000)
	transferFeeMap.Set("GSC", 116.68800000)
	transferFeeMap.Set("UC", 3322.88310000)
	transferFeeMap.Set("UIP", 61.88950000)
	transferFeeMap.Set("CNN", 10015.96740000)
	transferFeeMap.Set("AAC", 180.25540000)
	transferFeeMap.Set("UUU", 928.48010000)
	transferFeeMap.Set("CDC", 2270.70340000)
	transferFeeMap.Set("LXT", 155.65430000)
	transferFeeMap.Set("BUT", 315.12600000)
	transferFeeMap.Set("18C", 171.59910000)
	transferFeeMap.Set("DATX", 1714.71170000)
	transferFeeMap.Set("PORTAL", 488.35720000)
	transferFeeMap.Set("GTC", 134.20990000)
	transferFeeMap.Set("HOT", 148.59800000)
	transferFeeMap.Set("MAN", 10.91940000)
	transferFeeMap.Set("GET", 84.36850000)
	transferFeeMap.Set("PC", 4411.76470000)
	transferFeeMap.Set("REN", 11.87400000)
	transferFeeMap.Set("BKBT", 1077.11520000)
	transferFeeMap.Set("INC", 5.64730000)
	transferFeeMap.Set("GVE", 2599.84920000)
	transferFeeMap.Set("SEELE", 3.07670000)
	transferFeeMap.Set("FTI", 1917.19920000)
	transferFeeMap.Set("EKT", 10.00000000)
	transferFeeMap.Set("XMX", 445.01770000)
	transferFeeMap.Set("YCC", 45.83950000)
	transferFeeMap.Set("FAIR", 305.30970000)
	transferFeeMap.Set("SSP", 1998.84120000)
	transferFeeMap.Set("EON", 10.00000000)
	transferFeeMap.Set("EOP", 5.00000000)
	transferFeeMap.Set("LYM", 152.24730000)
	transferFeeMap.Set("ZJLT", 1644.03140000)
	transferFeeMap.Set("MEETONE", 50.00000000)
	transferFeeMap.Set("PNT", 5458.86070000)
	transferFeeMap.Set("IDT", 96.87880000)
	transferFeeMap.Set("DAC", 137.80430000)
	transferFeeMap.Set("BCV", 50.23360000)
	transferFeeMap.Set("SEXC", 50.00000000)
	transferFeeMap.Set("TOS", 373.43720000)
	transferFeeMap.Set("MUSK", 300.00000000)
	transferFeeMap.Set("ADD", 100.00000000)
	transferFeeMap.Set("MT", 405.21490000)
	transferFeeMap.Set("KCASH", 67.07290000)
	transferFeeMap.Set("NCC", 327.57310000)
	transferFeeMap.Set("RCCC", 39.29270000)
	transferFeeMap.Set("CVCOIN", 10.23890000)
	transferFeeMap.Set("RTE", 567.85440000)
	transferFeeMap.Set("TRIO", 338.02650000)
	transferFeeMap.Set("GRS", 0.05000000)
	transferFeeMap.Set("ARDR", 2.00000000)
	transferFeeMap.Set("NANO", 0.01000000)
	transferFeeMap.Set("ZEN", 0.00200000)
	transferFeeMap.Set("RBTC", 0.00015000)
	transferFeeMap.Set("BSV", 0.00050000)
	transferFeeMap.Set("ONG", 1.00000000)
	transferFeeMap.Set("MXC", 270.54900000)
	transferFeeMap.Set("COVA", 1898.21180000)
	transferFeeMap.Set("LAMB", 14.57920000)
	transferFeeMap.Set("CVNT", 92.43010000)
	transferFeeMap.Set("DOCK", 51.26300000)
	transferFeeMap.Set("BTT", 100.00000000)
	transferFeeMap.Set("SC", 0.10000000)
	transferFeeMap.Set("KMD", 0.00200000)
	transferFeeMap.Set("LOOM", 22.74970000)
	transferFeeMap.Set("NEXO", 4.61410000)
	transferFeeMap.Set("ETN", 150.00000000)
	transferFeeMap.Set("NPXS", 2965.19120000)
	transferFeeMap.Set("TOP", 297.53510000)
	transferFeeMap.Set("DOGE", 20)
	transferFeeMap.Set("IRIS", 1)
	transferFeeMap.Set("TFUEL", 1.00000000)
	transferFeeMap.Set("UGAS", 5.00000000)
	transferFeeMap.Set("TT", 1.00000000)
	transferFeeMap.Set("NEW", 5.00000000)
	transferFeeMap.Set("RSR", 295.39870000)
	transferFeeMap.Set("ATP", 2.00000000)
	transferFeeMap.Set("NKN", 22.16430000)
	transferFeeMap.Set("OGO", 41.56450000)
	transferFeeMap.Set("CRO", 10)
	transferFeeMap.Set("EGT", 258.53280000)
	transferFeeMap.Set("PVT", 862.66490000)
	transferFeeMap.Set("CNNS", 92.03360000)
	transferFeeMap.Set("GT", 1.13800000)
	transferFeeMap.Set("BHT", 16.50130000)
	transferFeeMap.Set("PIZZA", 25.00000000)
	transferFeeMap.Set("VSYS", 1.00000000)
	transferFeeMap.Set("CRE", 192.98120000)
	transferFeeMap.Set("FTT", 0.22760000)
	transferFeeMap.Set("LOL", 500.00000000)
	transferFeeMap.Set("ARPA", 45.85050000)
	transferFeeMap.Set("FOR", 28.37400000)
	transferFeeMap.Set("VIDY", 338.10200000)
	transferFeeMap.Set("NODE", 67.37460000)
	transferFeeMap.Set("BHD", 0.05000000)
	transferFeeMap.Set("ONE", 200.00000000)
	transferFeeMap.Set("EM", 107.10050000)
	transferFeeMap.Set("MX", 7.07890000)
	transferFeeMap.Set("CKB", 1)
	transferFeeMap.Set("SKM", 215.12990000)
	h.transferFeeMap = &transferFeeMap
	return transferFeeMap.GetAll(), nil
}

func (h *HuobiApi) Balances() (map[string]float64, error) {
	accountId, err := h.getAccountId()
	if err != nil {
		return nil, err
	}
	params := &url.Values{}
	params.Set("account-id", accountId)
	byteArray, err := h.privateApi("GET", "/v1/account/accounts/"+accountId+"/balance", params)
	if err != nil {
		return nil, err
	}
	json, err := jason.NewObjectFromBytes(byteArray)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json")
	}
	data, err := json.GetObject("data")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json key data")
	}
	balances, err := data.GetObjectArray("list")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json key list")
	}
	m := make(map[string]float64)
	for _, v := range balances {
		currency, err := v.GetString("currency")
		if err != nil {
			continue
		}
		t, err := v.GetString("type")
		if err != nil {
			continue
		}
		if t == "frozen" {
			continue
		}
		availableStr, err := v.GetString("balance")
		if err != nil {
			continue
		}
		available, err := strconv.ParseFloat(availableStr, 10)
		if err != nil {
			return nil, err
		}
		m[currency] = available
	}
	return m, nil
}

type HuobiBalance struct {
	T       string
	Balance float64
}

func (h *HuobiApi) getAccountId() (string, error) {
	byteArray, err := h.privateApi("GET", "/v1/account/accounts", &url.Values{})
	if err != nil {
		return "", err
	}
	json, err := jason.NewObjectFromBytes(byteArray)
	if err != nil {
		return "", errors.Wrapf(err, "failed to parse json: raw data")
	}
	data, err := json.GetObjectArray("data")
	if err != nil {
		return "", errors.Wrapf(err, "failed to parse json: key data")
	}
	if len(data) == 0 {
		return "", errors.New("there is no data")
	}
	accountIdInt, err := data[0].GetInt64("id")
	if err != nil {
		return "", errors.New("there is no available account")
	}
	accountId := strconv.Itoa(int(accountIdInt))
	return accountId, nil
}

func (h *HuobiApi) CompleteBalances() (map[string]*models.Balance, error) {
	accountId, err := h.getAccountId()
	if err != nil {
		return nil, err
	}
	params := &url.Values{}
	params.Set("account-id", accountId)
	byteArray, err := h.privateApi("GET", "/v1/account/accounts/"+accountId+"/balance", params)
	if err != nil {
		return nil, err
	}
	json, err := jason.NewObjectFromBytes(byteArray)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json")
	}
	data, err := json.GetObject("data")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json key data")
	}
	balances, err := data.GetObjectArray("list")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json")
	}
	m := make(map[string]*models.Balance)
	var previousCurrency string
	previousBalance := &models.Balance{}
	var available float64
	for _, v := range balances {
		currency, err := v.GetString("currency")
		if err != nil {
			continue
		}
		t, err := v.GetString("type")
		if err != nil {
			continue
		}
		availableStr, err := v.GetString("balance")
		if err != nil {
			continue
		}
		available, err = strconv.ParseFloat(availableStr, 10)
		if err != nil {
			return nil, err
		}
		if previousCurrency != "" && previousCurrency != currency {
			m[previousCurrency] = previousBalance
			previousCurrency = currency
			previousBalance = &models.Balance{}
		}
		if t == "trade" {
			previousBalance.Available = available
		} else {
			previousBalance.OnOrders = available
		}
		previousCurrency = currency
	}
	return m, nil
}

func (h *HuobiApi) CompleteBalance(coin string) (*models.Balance, error) {
	accountId, err := h.getAccountId()
	if err != nil {
		return nil, err
	}
	params := &url.Values{}
	params.Set("account-id", accountId)
	byteArray, err := h.privateApi("GET", "/v1/account/accounts/"+accountId+"/balance", params)
	if err != nil {
		return nil, err
	}
	json, err := jason.NewObjectFromBytes(byteArray)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json")
	}
	data, err := json.GetObject("data")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json key data")
	}
	balances, err := data.GetObjectArray("list")
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse json")
	}
	m := make(map[string]*models.Balance)
	var previousCurrency string
	previousBalance := &models.Balance{}
	var available float64
	for _, v := range balances {
		currency, err := v.GetString("currency")
		if err != nil {
			continue
		}
		if currency == coin {
			continue
		}
		t, err := v.GetString("type")
		if err != nil {
			continue
		}
		availableStr, err := v.GetString("balance")
		if err != nil {
			continue
		}
		available, err = strconv.ParseFloat(availableStr, 10)
		if err != nil {
			return nil, err
		}
		if previousCurrency != "" && previousCurrency != currency {
			m[previousCurrency] = previousBalance
			previousCurrency = currency
			previousBalance = &models.Balance{}
		}
		if t == "trade" {
			previousBalance.Available = available
		} else {
			previousBalance.OnOrders = available
		}
		previousCurrency = currency
	}
	return m[coin], nil
}

type HuobiActiveOrderResponse struct {
	response   []byte
	Trading    string
	Settlement string
	err        error
}

func (h *HuobiApi) ActiveOrders() ([]*models.Order, error) {
	return nil, errors.New("not implemented")
}

func (h *HuobiApi) Order(trading string, settlement string, ordertype models.OrderType, price float64, amount float64) (string, error) {
	accountId, err := h.getAccountId()
	if err != nil {
		return "", err
	}
	params := &url.Values{}
	if ordertype == models.Ask {
		params.Set("type", "buy-limit")
	} else if ordertype == models.Bid {
		params.Set("type", "sell-limit")
	} else {
		return "", errors.Errorf("unknown order type %d", ordertype)
	}
	params.Set("symbol", strings.ToLower(fmt.Sprintf("%s%s", trading, settlement)))
	params.Set("account-id", accountId)
	amountStr := strconv.FormatFloat(amount, 'f', 4, 64)
	priceStr := strconv.FormatFloat(price, 'f', 4, 64)
	params.Set("amount", amountStr)
	params.Set("price", priceStr)
	byteArray, err := h.privateApi("GET", "/v1/order/orders/place", params)
	if err != nil {
		return "", err
	}
	json, err := jason.NewObjectFromBytes(byteArray)
	if err != nil {
		return "", errors.Wrapf(err, "failed to parse json")
	}
	orderId, err := json.GetString("data")
	if err != nil {
		return "", errors.Wrapf(err, "failed to parse json")
	}
	return orderId, nil
}

func (h *HuobiApi) Transfer(typ string, addr string, amount float64, additionalFee float64) error {
	params := &url.Values{}
	amountStr := strconv.FormatFloat(amount, 'f', 4, 64)
	additionalFeeStr := strconv.FormatFloat(additionalFee, 'f', 4, 64)
	params.Set("address", addr)
	params.Set("amount", amountStr)
	params.Set("currency", typ)
	params.Set("fee", additionalFeeStr)
	_, err := h.privateApi("GET", "/v1/dw/withdraw/api/create", params)
	return err
}

func (h *HuobiApi) CancelOrder(trading string, settlement string,
	ordertype models.OrderType, orderNumber string) error {
	params := &url.Values{}
	params.Set("order-id", orderNumber)
	_, err := h.privateApi("POST", "/v1/order/orders/"+orderNumber+"/submitcancel", params)
	if err != nil {
		return errors.Wrapf(err, "failed to cancel order")
	}
	return nil
}

func (h *HuobiApi) IsOrderFilled(trading string, settlement string, orderNumber string) (bool, error) {
	params := &url.Values{}
	params.Set("order-id", orderNumber)
	bs, err := h.privateApi("POST", "/v1/order/orders/"+orderNumber, params)
	if err != nil {
		return false, errors.Wrapf(err, "failed to cancel order")
	}
	json, err := jason.NewObjectFromBytes(bs)
	if err != nil {
		return false, errors.Wrap(err, "failed to parse json")
	}
	data, err := json.GetObject("data")
	if err != nil {
		return false, err
	}
	status, err := data.GetString("state")
	if err != nil {
		return false, err
	}
	if status == "filled" {
		return true, nil
	}
	return false, nil
}

func (h *HuobiApi) Address(c string) (string, error) {
	params := &url.Values{}
	params.Set("currency", strings.ToLower(c))
	params.Set("type", "deposit")

	bs, err := h.privateApi("GET", "/v1/dw/deposit-virtual/addresses", params)
	if err != nil {
		return "", errors.Wrap(err, "failed to fetch deposit address")
	}
	json, err := jason.NewObjectFromBytes(bs)
	if err != nil {
		return "", errors.Wrap(err, "failed to parse json")
	}
	fmt.Println(json)
	address, err := json.GetString("data")
	if err != nil {
		return "", errors.Wrapf(err, "failed to take address of %s", c)
	}
	return address, nil
}
