export function RandomString(num: number): string {
  var c = 'abcdefghijklmnopqrstuvwxyz0123456789'

  var cl = c.length
  var r = ''
  for (let i = 0; i < num; i++) {
    r += c[Math.floor(Math.random() * cl)]
  }
  return r
}
