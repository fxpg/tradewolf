import ccxws from 'ccxws'
import ccxt from 'ccxt'
import cloudscraper from 'cloudscraper'
import Redis from 'ioredis'
import IORedis from 'ioredis';
import { Exchange, GetCcxtClient, GetCcxwsClientConstructor, IsExchange, GetArbitragableMarkets } from './utils'
import * as cluster from "cluster";
import mongoose from 'mongoose'
import * as os from "os";
import retry from 'async-retry';

let REDIS_HOST = "localhost"
let REDIS_PORT = 6379
let MONGODB_SETTING = "mongodb://mongo:27017/tradewolf"

if (process.env.REDIS_HOST) {
  REDIS_HOST = process.env.REDIS_HOST
}
if (process.env.REDIS_PORT) {
  REDIS_PORT = Number(process.env.REDIS_PORT)
}
const NUM_CPUS = os.cpus().length;

type Ticker = {
  exchange: string
  base: string
  quote: string
  timestamp: number
  last: string
  open:string
  low:string
  high:string
  volume:string
  quoteVolume:string
  change: string
  changePercent: string
  bid: string
  bidVolume: string
  ask: string
  askVolume: string
}
type BoardBar = [number, number]
type OrderBook = {
  asks : BoardBar[]
  bids : BoardBar[]
}
const TickerModel = mongoose.model("Ticker", new mongoose.Schema({exchange: String, trading: String, settlement: String, datetime:{ type : Date, default: Date.now }, bestAsk: Number, bestBid: Number,settleVolume: Number,}))
const ArbitrageTickerModel = mongoose.model("ArbitrageTicker", new mongoose.Schema({exchangeA: String, exchangeB: String, trading: String, settlement: String, datetime:{ type : Date, default: Date.now }, bestAskA: Number, bestBidA: Number, bestAskB:Number, bestBidB: Number}))
const ArbitrageModel = mongoose.model("Arbitrage", new mongoose.Schema({buySide:String, sellSide:String, trading:String, settlement:String, datetime:{ type : Date, default: Date.now }, buySideAsk: Number, buySideBid: Number, sellSideAsk: Number, sellSideBid:Number}))

const scrapeCloudflareHttpHeaderCookie = (url: string) =>
	(new Promise ((resolve, reject) =>
		(cloudscraper.get (url, function (error:any, response:any, _body:any) {
			if (error) {
				reject (error)
			} else {
        resolve (response.request.headers)
      }
		}))
  ))  

class TickerFetcher{
  redis: IORedis.Redis
  pubsubClient: IORedis.Redis
  exchange: Exchange
  ccxtClient: ccxt.Exchange
  ccxwsClients: any[]
  markets!: ccxt.Market[];
  mongo: mongoose.Connection

  constructor(exchange:Exchange, ccxtClient:ccxt.Exchange, ccxwsClientConstructor:any) {
    this.redis = new Redis({port:REDIS_PORT, host: REDIS_HOST,  retryStrategy: function(times:number) {
        var delay = Math.min(times * 50, 2000);
        return delay;
      }
    });
    this.pubsubClient = new Redis({port:REDIS_PORT, host: REDIS_HOST,  retryStrategy: function(times:number) {
        var delay = Math.min(times * 50, 2000);
        return delay;
      }
    });
    this.exchange = exchange
    ccxtClient.enableRateLimit = true
    ccxtClient.timeout = 30000
    ccxtClient.rateLimit = 3000
    this.ccxtClient = ccxtClient
    const ccxwsClients:any[]=[]
    const ccxwsClientsNum = Math.ceil(10 / NUM_CPUS)
    for(let i=0;i<ccxwsClientsNum;i++) {
      ccxwsClients.push(new ccxwsClientConstructor())
    }
    this.ccxwsClients = ccxwsClients
    this.ccxtClient.has['fetchCurrencies'] = false
    this.mongo = mongoose.connection
  }

  rotateCcxwsClient(): any {
    return this.ccxwsClients[Math.floor(Math.random() * this.ccxwsClients.length)]
  }
  
  setMarkets(markets: ccxt.Market[]) {
    this.markets = markets
  }
  
  async fetchMarketData():Promise<ccxt.Dictionary<ccxt.Market>>{
    // this.ccxtClient.headers = await scrapeCloudflareHttpHeaderCookie(this.ccxtClient.urls.www)
    return await this.ccxtClient.loadMarkets()    
  }

  async initialize() {
    await this.redis.keys(this.exchange+":*").then((keys:string[])=>{
      let pipeline = this.redis.pipeline()
      keys.forEach((key:string)=>{
        pipeline.del(key)
      })
      return pipeline.exec()
    })
    console.log('existing data deleted')
  }

  private getKeyPrefix(exchange:string, trading:string, settlement:string) :string {
    return "ticker:"+exchange+":"+trading+":"+settlement+":"
  }

  public async resetOrderBook(exchange:string, trading:string, settlement:string) {
    await this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"asks")
    await this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"bids")
  }

  public async SetTicker(exchange:string, trading:string, settlement:string, ticker: Ticker) {
    this.redis.multi({ pipeline: false })
    this.redis.set(this.getKeyPrefix(exchange,trading,settlement)+"ask", ticker.ask)
    this.redis.set(this.getKeyPrefix(exchange,trading,settlement)+"askVolume", ticker.askVolume)    
    this.redis.set(this.getKeyPrefix(exchange,trading,settlement)+"bid", ticker.bid)
    this.redis.set(this.getKeyPrefix(exchange,trading,settlement)+"bidVolume", ticker.bidVolume)
    this.redis.exec((_err: Error|null, _res: [Error|null, string][])=>{
    })
    this.pubsubClient.publish(`${exchange}:${trading}:${settlement}`, "test")
  }
}

class TickerFetcherWrapper extends TickerFetcher {
  constructor(exchange:Exchange) {
    const ccxwsConstructor = GetCcxwsClientConstructor(exchange)
    const ccxtClient = GetCcxtClient(exchange)
    if(ccxtClient===undefined) {
      throw Error(`no exchange ${exchange}`)
    }
    super(exchange, ccxtClient, ccxwsConstructor)
  }

  addHandler() {
    const cli = this.rotateCcxwsClient()
    cli.on("ticker", async (ticker:any, market:any)=>{
      const t = {
        exchange: ticker.exchange,
        base: ticker.base,
        quote: ticker.quote,
        timestamp: ticker.timestamp,
        last: ticker.last,
        open: ticker.open,
        low: ticker.low,
        high: ticker.high,
        volume: ticker.volume,
        quoteVolume: ticker.quoteVolume,
        change: ticker.change,
        changePercent: ticker.changePercent,
        ask: ticker.ask,
        askVolume: ticker.askVolume,
        bid:ticker.bid,
        bidVolume:ticker.bidVolume,
      }
      await this.SetTicker(this.exchange, market.base, market.quote, t)
      TickerModel.create({exchange:ticker.exchange, trading:ticker.base, settlement:ticker.quote, bestAsk:ticker.ask, bestBid:ticker.bid,settleVolume:ticker.quoteVolume}).then(()=>{
      }).catch(err=>{
        console.log(err)
      })
      // const tm = new TickerModel()
      // await tm.save()
    });
    cli.on("error", (err:any)=>{
      console.log(err)
    })
  }
  async fetch() {
    for(let market of this.markets) {
      const cli = this.rotateCcxwsClient()
      cli.subscribeTicker({id:market.id, base:market.base,quote:market.quote});
      cli.subscribe
    }
    console.log("fetch triggered")
  }
}

const NewTickerFetcher = (EXCHANGE_TARGET:Exchange): TickerFetcherWrapper => {
  return new TickerFetcherWrapper(EXCHANGE_TARGET)
}

let EXCHANGE_TARGET :Exchange
if (process.env.EXCHANGE_TARGET === undefined) {
  console.log("not set env $EXCHANGE_TARGET")
  process.exit(1)
}
if (!IsExchange(process.env.EXCHANGE_TARGET)) {
  console.log("invalid $EXCHANGE_TARGET")
  console.log(process.env.EXCHANGE_TARGET)
  process.exit(1)
}
EXCHANGE_TARGET = process.env.EXCHANGE_TARGET

const masterFunc = (async()=>{
  console.log("master func")  
  const obFetcher = NewTickerFetcher(EXCHANGE_TARGET)
  let markets: ccxt.Market[]
  try {
    markets = await GetArbitragableMarkets(EXCHANGE_TARGET)
  } catch(e) {
    console.log(e)
    throw(e)
  }
  console.log(`target markets ${markets.length}`)
  
  const slicedTarget:ccxt.Market[][] = []
  const bundle = Math.ceil(markets.length / NUM_CPUS)
  for (let i = 0; i < Object.keys(markets).length; i += bundle) {
      slicedTarget.push(markets.slice(i, i + bundle))
  }
  for(let i =0; i<slicedTarget.length;i++) {    
    let worker = cluster.fork()
    worker.on('online', () => {
        worker.send(slicedTarget[i])
    })
    worker.on('exit', (worker:cluster.Worker, code:number, _signal:any) => {
      console.log('| Main Thread | worker stopped');
      cluster.fork();
    });
  }
});

const workerFunc = (async() => {
  console.log("worker func")  
  process.on("message", async (targetMarkets:ccxt.Market[])=>{
    const obFetcher = NewTickerFetcher(EXCHANGE_TARGET)
    await mongoose.connect(MONGODB_SETTING,{ useNewUrlParser: true, useUnifiedTopology: true })
    await obFetcher.initialize()
    obFetcher.setMarkets(targetMarkets)
    obFetcher.addHandler()
    try {
      await obFetcher.fetch()
    } catch (e) {
      throw e
    }
  })
})

if(cluster.isMaster){
  masterFunc().then(()=>{
  }).catch(e=>{
    process.exit(1)
  })
} else {
  workerFunc()
}
