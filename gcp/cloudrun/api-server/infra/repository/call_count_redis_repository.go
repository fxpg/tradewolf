package repository

import (
	"context"
	"github.com/go-redis/redis"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"time"
)

func NewCallCountRedisRepository(cli *redis.Client) interfaces.CallCountRepository {
	return &CallCountRedisRepository{cli: cli}
}

type CallCountRedisRepository struct {
	cli *redis.Client
}

func (r *CallCountRedisRepository) GetByDay(ctx context.Context, uid string) (int, error) {
	now := time.Now()
	callCountRecord := r.cli.WithContext(ctx).Get("call_count:"+uid+":day:"+now.Format("20060102"))
	if callCountRecord.Err() == redis.Nil {
		return 0, nil
	}
	if callCountRecord.Err() != nil {
		return 0, qerror.WrapWith(callCountRecord.Err(), model.ErrSystemRedisCommandFailed)
	}
	dayCount,err:= callCountRecord.Int()
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemRedisCommandFailed)
	}
	return dayCount,nil
}

func (r *CallCountRedisRepository) GetByMonth(ctx context.Context, uid string) (int, error) {
	now := time.Now()
	callCountRecord := r.cli.WithContext(ctx).Get("call_count:"+uid+":month:"+now.Format("200601"))
	if callCountRecord.Err() == redis.Nil {
		return 0, nil
	}
	if callCountRecord.Err() != nil {
		return 0, qerror.WrapWith(callCountRecord.Err(), model.ErrSystemRedisCommandFailed)
	}
	monthCount,err:= callCountRecord.Int()
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemRedisCommandFailed)
	}
	return monthCount,nil
}

func (r *CallCountRedisRepository) Increment(ctx context.Context, uid string, path string) error {
	now := time.Now()
	incrementDayCmd := r.cli.WithContext(ctx).Incr("call_count:"+uid+":day:"+now.Format("20060102"))
	if incrementDayCmd.Err() != nil {
		return qerror.WrapWith(incrementDayCmd.Err(), model.ErrSystemRedisCommandFailed)
	}
	incrementMonthCmd := r.cli.WithContext(ctx).Incr("call_count:"+uid+":month:"+now.Format("200601"))
	if incrementMonthCmd.Err() != nil {
		return qerror.WrapWith(incrementMonthCmd.Err(), model.ErrSystemRedisCommandFailed)
	}
	return nil
}

func (r *CallCountRedisRepository) GetNonceByUid(ctx context.Context, uid string) (time.Time, error) {
	nonce := r.cli.WithContext(ctx).Get("nonce:"+uid)
	if nonce.Err() != nil {
		return time.Time{}, qerror.WrapWith(nonce.Err(), model.ErrSystemRedisCommandFailed)
	}
	unixtime,err := nonce.Int64()
	if err != nil {
		return time.Time{}, qerror.WrapWith(err, model.ErrSystemRedisCommandFailed)
	}
	return time.Unix(unixtime,0), nil
}

func (r *CallCountRedisRepository) UpdateNonceByUid(ctx context.Context, uid string, nonce time.Time) error {
	cmd := r.cli.WithContext(ctx).Set("nonce:"+uid,nonce.UnixNano(), 0)
	if cmd.Err() != nil {
		return qerror.WrapWith(cmd.Err(), model.ErrSystemRedisCommandFailed)
	}
	return nil
}
