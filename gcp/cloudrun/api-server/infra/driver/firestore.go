package driver

import (
	"context"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"log"

	"cloud.google.com/go/firestore"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/config"
)

func ConnectToFirestore(ctx context.Context, c *config.Config) (*firestore.Client, error) {
	cli, err := firestore.NewClient(ctx, c.FireStoreConfig.ProjectID)
	if err != nil {
		return nil, qerror.WrapWith(err, model.ErrSystemFirestoreConnectionError)
	}
	log.Print("connected to firestore")
	return cli, nil
}
