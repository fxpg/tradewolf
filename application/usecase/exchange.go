package usecase

import (
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/domain/service"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
	"sort"
)

type ExchangeUsecase interface {
	GetPairs(string) ([]entity.CurrencyPair, error)
	GetBoard(string, *entity.CurrencyPair) (*entity.Board, error)
	GetOrderBookTicks() ([]entity.OrderBookTick, error)
	GetOrderBookTicksByExchange(string) ([]entity.OrderBookTick, error)
	GetPrecise(exchange string, trading string, settlement string) (*models.Precisions, error)

	GetExchanges() ([]string, error)
	SetKey(setting *entity.KeySetting) error
	SetApiKey(exchange string, apiKey string) error
	SetSecKey(exchange string, secKey string) error
	GetKey(exchange string) (*entity.KeySetting, error)
	GetApiKey(exchange string) (string, error)
	GetSecKey(exchange string) (string, error)
	DeleteKey(exchange string) (bool, error)
}
type exchangeUsecase struct {
	PublicResourceRepository  service.PublicResourceRepository
	PrivateResourceRepository service.PrivateResourceRepository
	SettingRepository         service.SettingRepository
	config                    *config.Config
}

func (e *exchangeUsecase) SetKey(setting *entity.KeySetting) error {
	return e.SettingRepository.SetKey(setting)
}

func (e *exchangeUsecase) SetApiKey(exchange string, apiKey string) error {
	return e.SettingRepository.SetApiKey(exchange, apiKey)
}

func (e *exchangeUsecase) SetSecKey(exchange string, secKey string) error {
	return e.SettingRepository.SetSecKey(exchange, secKey)
}

func (e *exchangeUsecase) GetKey(exchange string) (*entity.KeySetting, error) {
	return e.SettingRepository.GetLastKey(exchange)
}

func (e *exchangeUsecase) GetApiKey(exchange string) (string, error) {
	return e.SettingRepository.GetApiKey(exchange)
}

func (e *exchangeUsecase) GetSecKey(exchange string) (string, error) {
	return e.SettingRepository.GetSecKey(exchange)
}

func (e *exchangeUsecase) DeleteKey(exchange string) (bool, error) {
	return e.SettingRepository.Delete(&entity.KeySetting{
		Exchange: exchange,
	})
}
func (e *exchangeUsecase) GetExchanges() ([]string, error) {
	return e.config.Exchanges, nil
}

func (e *exchangeUsecase) GetOrderBookTicksByExchange(exchange string) ([]entity.OrderBookTick, error) {
	orderBookTicks := make([]entity.OrderBookTick, 0)
	orderBookTickMap, err := e.PublicResourceRepository.OrderBookTickMap(exchange)
	if err != nil {
		return orderBookTicks, err
	}
	for trading, v := range orderBookTickMap {
		for settlement, orderBookTick := range v {
			orderBookTicks = append(orderBookTicks, entity.OrderBookTick{
				Exchange: exchange,
				CurrencyPair: &entity.CurrencyPair{
					Trading:    trading,
					Settlement: settlement,
				},
				BestAskPrice:  orderBookTick.BestAskPrice,
				BestAskAmount: orderBookTick.BestAskAmount,
				BestBidPrice:  orderBookTick.BestBidPrice,
				BestBidAmount: orderBookTick.BestBidAmount,
			})
		}
	}
	return orderBookTicks, nil
}

func (e *exchangeUsecase) GetPrecise(exchange string, trading string, settlement string) (*models.Precisions, error) {
	return e.PublicResourceRepository.Precise(exchange, trading, settlement)
}

func (e *exchangeUsecase) GetOrderBookTicks() ([]entity.OrderBookTick, error) {
	orderBookTicks := make([]entity.OrderBookTick, 0)
	for _, exchange := range e.config.Exchanges {
		orderBookTicksByExchange, err := e.GetOrderBookTicksByExchange(exchange)
		if err != nil {
			logger.Get().Error(err)
			continue
		}
		orderBookTicks = append(orderBookTicks, orderBookTicksByExchange...)
	}
	return orderBookTicks, nil
}

func (e *exchangeUsecase) GetPairs(exchange string) ([]entity.CurrencyPair, error) {
	currencyPairs := make([]entity.CurrencyPair, 0)
	cP, err := e.PublicResourceRepository.CurrencyPairs(exchange)
	if err != nil {
		return currencyPairs, err
	}
	for _, v := range cP {
		currencyPairs = append(currencyPairs, entity.CurrencyPair{Trading: v.Trading, Settlement: v.Settlement})
	}
	return currencyPairs, nil
}

func (e *exchangeUsecase) GetBoard(exchange string, pair *entity.CurrencyPair) (*entity.Board, error) {
	boardFromRep, err := e.PublicResourceRepository.Board(exchange, pair.Trading, pair.Settlement)
	if err != nil {
		return nil, err
	}
	bids := make([]*entity.BoardBar, 0)
	for _, v := range boardFromRep.Bids {
		bids = append(bids, &entity.BoardBar{
			Price:  v.Price,
			Amount: v.Amount,
		})
	}
	asks := make([]*entity.BoardBar, 0)
	for _, v := range boardFromRep.Asks {
		asks = append(asks, &entity.BoardBar{
			Price:  v.Price,
			Amount: v.Amount,
		})
	}
	sort.Slice(bids, func(i, j int) bool { return bids[i].Price > bids[j].Price })
	sort.Slice(asks, func(i, j int) bool { return asks[i].Price < asks[j].Price })

	return &entity.Board{
		DateTime:     ptypes.TimestampNow(),
		CurrencyPair: pair,
		Exchange:     exchange,
		Bids:         bids,
		Asks:         asks,
	}, nil
}

func NewExchangeUsecase(pubResRep service.PublicResourceRepository, priResRep service.PrivateResourceRepository, settingRep service.SettingRepository, conf *config.Config) ExchangeUsecase {
	return &exchangeUsecase{
		pubResRep,
		priResRep,
		settingRep,
		conf,
	}
}
