package model

type CurrencyPair struct {
	Trading    string
	Settlement string
}

func (c *CurrencyPair) String() string {
	return c.Trading + ":" + c.Settlement
}