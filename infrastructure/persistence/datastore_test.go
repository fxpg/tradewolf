package persistence

import (
	"fmt"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	mocket "github.com/selvatico/go-mocket"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fxpg/tradewolf/domain/entity"
)

func SetupTests() (*gorm.DB, error) { // or *gorm.DB
	mocket.Catcher.Register() // Safe register. Allowed multiple calls to save
	mocket.Catcher.Logging = true
	// GORM
	db, err := gorm.Open(mocket.DriverName, "connection_string") // Can be any connection string
	return db, err
}

func TestSettingRepositorySqlite(t *testing.T) {
	db, err := SetupTests()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true)

	r := SettingRepositorySqlite{DB: db}
	exchange := "binance"
	apiKey := "apiapi"
	secKey := "sikosiko"
	var setting entity.KeySetting
	setting.Exchange = exchange
	setting.ApiKey = apiKey
	setting.SecKey = secKey
	mocket.Catcher.Reset().NewMock().WithQuery("INSERT INTO 'key_settings")
	err = r.SetKey(&setting)
	if err != nil {
		t.Fatal(err)
	}
	commonReply := []map[string]interface{}{{"exchange": exchange, "api_key": apiKey, "sec_key": secKey}}
	mocket.Catcher.Reset().NewMock().WithQuery(`SELECT * FROM "key_settings"  WHERE`).WithQuery(`LIMIT 1`).WithReply(commonReply)
	lastKey, err := r.GetLastKey(exchange)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, "apiapi", lastKey.ApiKey)
	assert.Equal(t, "sikosiko", lastKey.SecKey)

	mocket.Catcher.Reset().NewMock().WithQuery(`SELECT * FROM "key_settings"  WHERE (exchange =`).WithReply(commonReply)

	keys, err := r.GetKeys(exchange)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 1, len(keys))
	//  = 'binance')
	mocket.Catcher.Reset().NewMock().WithQuery(`DELETE FROM "key_settings"  WHERE`)
	isOk, err := r.Delete(&setting)
	if err != nil {
		t.Fatal(err)
	}
	if !isOk {
		t.Fatal(errors.New(fmt.Sprintf("there is not record %v", setting)))
	}
}
