package main

import (
	"fmt"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/controller"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/driver"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/registry"
	"log"
	"net/http"

	_ "net/http/pprof"
)
import "github.com/mattn/go-runewidth"

func main() {
	go func() {
		log.Println(http.ListenAndServe("localhost:6060", nil))
	}()

	runewidth.DefaultCondition.EastAsianWidth = false

	c, err := registry.New()
	if err != nil {
		fmt.Println(err)
		return
	}
	err = c.Invoke(func(guiController controller.GuiController) {
		receiver := make(chan *model.CurrencyPair)
		var exchanges = []string{"hitbtc", "binance", "poloniex"}
		psc := driver.NewPubsubClient()
		go psc.Receive(exchanges, receiver)
		go func(r <-chan *model.CurrencyPair) {
			for {
				select {
				case cp := <-r:
					arbitrages, err := guiController.FilterWatchingArbitrages(*cp)
					if err != nil {
						guiController.Log(err.Error())
						continue
					}
					err = guiController.UpdateArbitrageData(arbitrages)
					if err != nil {
						guiController.Log(err.Error())
						continue
					}
					err = guiController.UpdateArbitrageWatchMangerView()
					if err != nil {
						guiController.Log(err.Error())
						continue
					}
				}
			}
		}(receiver)

		if err := guiController.Run(); err != nil {
			guiController.Log(err.Error())
			return
		}
	})
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}
