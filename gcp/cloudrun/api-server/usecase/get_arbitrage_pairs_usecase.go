package usecase

import (
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"

import "context"

func NewGetArbitragePairsUsecase(
	repo interfaces.ArbitragePairRepository,
	logger utils.Logger,
) *GetArbitragePairsUsecase {
	return &GetArbitragePairsUsecase{
		repo: repo,
		logger: logger,
	}
}

type GetArbitragePairsUsecase struct {
	repo interfaces.ArbitragePairRepository
	logger utils.Logger
}

func (u *GetArbitragePairsUsecase) Exec(ctx context.Context) (model.ArbitragePairs, error) {
	pairs, err := u.repo.GetAll(ctx)
	if err != nil {
		return nil, err
	}

	return pairs, nil
}
