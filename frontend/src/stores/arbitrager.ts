import {
  Module,
  VuexModule,
  Mutation,
  Action,
  getModule
} from 'vuex-module-decorators'
import {
  CurrencyPair,
  ArbitragePair,
  ArbitragerSetting
} from '@/models/domain_pb'
import store from '@/stores/index'
import { ArbitragerRepository } from '@/repositories/arbitrager'

@Module({ dynamic: true, store, namespaced: true, name: 'ArbitragerModule' })
export class arbitragerModule extends VuexModule {
  avilableCurrencyPairs: CurrencyPair[] = Array<CurrencyPair>()
  arbitragePairs: ArbitragePair[] = Array<ArbitragePair>()
  arbitragerIDs: number[] = []
  arbitragerSettings: { [key: number]: ArbitragerSetting } = {}

  get getArbitragePairs(): ArbitragePair[] {
    return this.arbitragePairs
  }
  get getArbitragePairsBySettlement(): { [key: string]: ArbitragePair[] } {
    const ret: { [key: string]: ArbitragePair[] } = {}
    const pairs: ArbitragePair[] = Array<ArbitragePair>()
    let settlement = ''
    this.arbitragePairs.forEach((pair: ArbitragePair) => {
      const currencyPair = pair.getCurrencyPair()
      if (currencyPair == undefined) {
        return
      }
      settlement = currencyPair.getSettlement()
      if (ret[settlement] === undefined) {
        ret[settlement] = Array<ArbitragePair>()
      }
      ret[settlement].push(pair)
    })
    return ret
  }
  get getArbitragerIDs(): number[] {
    return this.arbitragerIDs
  }
  get getArbitragerSettings(): { [key: number]: ArbitragerSetting } {
    return this.arbitragerSettings
  }
  @Mutation
  public SET_AVAILABLE_CURRENCY_PAIRS(pairs: CurrencyPair[]) {
    this.avilableCurrencyPairs = pairs
  }

  @Mutation
  public SET_ARBITRAGE_PAIRS(pairs: ArbitragePair[]) {
    this.arbitragePairs = pairs
  }

  @Mutation
  public UPDATE_ARBITRAGER_IDS(ids: number[]) {
    this.arbitragerIDs = ids
  }
  @Mutation
  public UPDATE_ARBITRAGERS(arbitragerSettings: {
    [key: number]: ArbitragerSetting
  }) {
    this.arbitragerSettings = arbitragerSettings
  }

  @Action({ commit: 'SET_ARBITRAGE_PAIRS' })
  public async setArbitragePairs() {
    const payload = {
      pairs: Array<ArbitragePair>()
    }
    return await ArbitragerRepository.GetArbitragePairs()
  }
  @Action({ commit: 'SET_AVAILABLE_CURRENCY_PAIRS' })
  public async setAvailableCurrencyPairs() {
    const payload = {
      pairs: Array<CurrencyPair>()
    }
    // @ts-ignore
    const pairs: any[] = await window.backend.arbitrageHandler.GetPairs()
    pairs.forEach((pair: any) => {
      const currencyPair = new CurrencyPair()
      currencyPair.setSettlement(pair.settlement)
      currencyPair.setTrading(pair.trading)
      payload.pairs.push(currencyPair)
    })
    payload.pairs.filter((x: CurrencyPair, i: number) => {
      return pairs.indexOf(i) !== pairs.lastIndexOf(i)
    })
    return payload.pairs
  }
  @Action({})
  public async createArbitrager(arbitragePair: ArbitragePair) {
    // @ts-ignore
    const arbitrageID: number = await window.backend.arbitrageHandler.CreateArbitrager(
      JSON.stringify(arbitragePair.toObject())
    )
  }
  @Action({ commit: 'UPDATE_ARBITRAGERS' })
  public async updateArbitragers() {
    // @ts-ignore
    const arbitragerIDs: number[] = await backend.arbitrageHandler.GetArbitragerIDs()
    let arbitragerSettings: { [key: number]: ArbitragerSetting } = {}
    for (let id of arbitragerIDs) {
      arbitragerSettings[id] = await ArbitragerRepository.GetArbitragerSetting(
        id
      )
    }
    return arbitragerSettings
  }
  @Action({ commit: 'UPDATE_ARBITRAGER_IDS' })
  public async updateArbitragerIDs() {
    // @ts-ignore
    const arbitragerIDs: number[] = await backend.arbitrageHandler.GetArbitragerIDs()
    return arbitragerIDs
  }
}

export const ArbitragerModule = getModule(arbitragerModule)
