module gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator

go 1.14

require (
	github.com/gdamore/tcell v1.3.0
	github.com/go-redis/redis/v8 v8.0.0-beta.7 // indirect
	github.com/gomodule/redigo v1.8.2
	github.com/google/pprof v0.0.0-20201109224723-20978b51388d // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/mattn/go-runewidth v0.0.9
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/mxschmitt/golang-combinations v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/rivo/tview v0.0.0-20200818120338-53d50e499bf9
	go.etcd.io/bbolt v1.3.5 // indirect
	go.mongodb.org/mongo-driver v1.4.0 // indirect
	go.uber.org/dig v1.10.0
	golang.org/x/sys v0.0.0-20201109165425-215b40eba54c // indirect
)
