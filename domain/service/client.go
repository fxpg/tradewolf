package service

import (
	"github.com/rs/xid"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/private"
)

type PublicResourceRepository interface {
	// Volume(exchange string, trading string, settlement string) (float64, error)
	CurrencyPairs(exchange string) ([]models.CurrencyPair, error)
	// Rate(exchange string, trading string, settlement string) (float64, error)
	// RateMap(exchange string) (map[string]map[string]float64, error)
	// VolumeMap(exchange string) (map[string]map[string]float64, error)
	OrderBookTickMap(exchange string) (map[string]map[string]models.OrderBookTick, error)
	FrozenCurrency(exchange string) ([]string, error)
	Board(exchange string, trading string, settlement string) (*models.Board, error)
	Precise(exchange string, trading string, settlement string) (*models.Precisions, error)
}

type PrivateResourceRepository interface {
	TransferFee(exchange string) (map[string]float64, error)
	TradeFeeRates(exchange string) (map[string]map[string]private.TradeFee, error)
	TradeFeeRate(exchange string, trading string, settlement string) (private.TradeFee, error)
	Balances(exchange string) (map[string]float64, error)
	CompleteBalance(exchange string, coin string) (*models.Balance, error)
	CompleteBalances(exchange string) (map[string]*models.Balance, error)
	ActiveOrders(exchange string) ([]*models.Order, error)
	IsOrderFilled(exchange string, trading string, settlement string, orderNumber string) (bool, error)
	Order(exchange string, trading string, settlement string,
		ordertype models.OrderType, price float64, amount float64) (string, error)
	CancelOrder(exchange string, trading string, settlement string, orderType models.OrderType, orderNumber string) error
	//FilledOrderInfo(orderNumber string) (models.FilledOrderInfo,error)
	Transfer(exchange string, typ string, addr string,
		amount float64, additionalFee float64) error
	Address(exchange string, c string) (string, error)
}

type privateRepositoryMock struct {
	PrivateResourceRepository
}

func (p *privateRepositoryMock) Order(exchange string, trading string, settlement string,
	ordertype models.OrderType, price float64, amount float64) (string, error) {
	return "TEST_ORDER_" + xid.New().String(), nil
}

func (p *privateRepositoryMock) CancelOrder(exchange string, trading string, settlement string, orderType models.OrderType, orderNumber string) error {
	return nil
}

func (p *privateRepositoryMock) IsOrderFilled(exchange string, trading string, settlement string, orderNumber string) (bool, error) {
	return true, nil
}

func NewPrivateRepositoryMock() PrivateResourceRepository {
	return &privateRepositoryMock{}
}
