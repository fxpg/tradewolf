package repository

var Providers = []interface{}{
	NewArbitragePairCachedRepository,
	NewArbitragePairFirestoreRepository,
	NewOrderBookRedisRepository,
	NewOrderBookCachedRepository,
	NewKeyPairFirestoreRepository,
	NewNonceRedisRepository,
	NewUserFirestoreRepository,
	NewCallCountRedisRepository,
	NewWithdrawalFeeFirestoreRepository,
}