package service

import (
	"gitlab.com/fxpg/tradewolf/domain/entity"
)

type ArbitragerSettingRepository interface {
	Insert(id int, arbitragerSetting *entity.ArbitragerSetting) error
	FindById(id int) (*entity.ArbitragerSetting, error)
	FindAll() ([]entity.ArbitragerSetting, error)
	FindIds() ([]int, error)
	Delete(id int) error
	LastId() (int, error)
}

type ArbitrageOrderRepository interface {
	CreateOrder(order *entity.ArbitrageOrder) error
	FilledOrder(order *entity.ArbitrageFilledOrder) error

	GetOrdersByArbitragerID(int) ([]entity.ArbitrageOrder, error)
	GetOrdersByCurrencyPair(string, string) ([]entity.ArbitrageOrder, error)
	GetOrdersByExchange(string) ([]entity.ArbitrageOrder, error)
	GetOrdersByArbitragePair(string, string, string) ([]entity.ArbitrageOrder, error)

	GetFilledOrdersByArbitragerID(int) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByCurrencyPair(string, string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByExchange(string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByArbitragePair(string, string, string) ([]entity.ArbitrageFilledOrder, error)
}

type SettingRepository interface {
	SetKey(setting *entity.KeySetting) error
	SetApiKey(exchange string, apiKey string) error
	SetSecKey(exchange string, apiKey string) error
	GetKeys(exchange string) ([]entity.KeySetting, error)
	GetApiKey(exchange string) (string, error)
	GetSecKey(exchange string) (string, error)
	GetLastKey(exchange string) (*entity.KeySetting, error)
	Delete(setting *entity.KeySetting) (bool, error)
}

type ArbitragerLogRepository interface {
	Insert(log *entity.ArbitragerLog) error
	FindByArbitragerId(arbitragerId int64) ([]entity.ArbitragerLog, error)
}

type arbitragerLogRepositoryMock struct {
	ArbitragerLogRepository
}

type arbitrageOrderRepositoryMock struct {
	ArbitrageOrderRepository
}

type settingRepositoryMock struct {
	SettingRepository
}

func (s *settingRepositoryMock) GetApiKey(exchange string) (string, error) {
	return "TEST_API_KEY", nil
}
func (s *settingRepositoryMock) GetSecKey(exchange string) (string, error) {
	return "TEST_SEC_KEY", nil
}

func NewSettingRepositoryMock() SettingRepository {
	return &settingRepositoryMock{}
}
