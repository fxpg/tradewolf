# VULTR_API_KEY

data "vultr_plan" "light" {
  filter {
    name   = "price_per_month"
    values = ["12.00"]
  }

  filter {
    name   = "ram"
    values = ["2048"]
  }
}

resource "vultr_server" "develop_rancher_node" {
  count             = 3
  region_id         = data.vultr_region.main_cite.id
  plan_id           = data.vultr_plan.starter.id
  os_id             = data.vultr_os.ubuntu.id
  ssh_key_ids       = [vultr_ssh_key.develop.id]
  hostname          = format("FXPG-N%02d", count.index + 1)
  label             = format("FXPG-N%02d", count.index + 1)
  tag               = "rancher_node_develop"
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
}

resource "vultr_firewall_group" "for_rancher_node_develop" {
  description = "firewall group for rancher node develop"
}

resource "vultr_firewall_rule" "node_traffic_10" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 443
}
resource "vultr_firewall_rule" "node_traffic_11" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 80
}

resource "vultr_firewall_rule" "node_traffic_1" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 2379
  to_port           = 2380
}
resource "vultr_firewall_rule" "node_traffic_2" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 6443
}
resource "vultr_firewall_rule" "node_traffic_3" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "udp"
  from_port         = 8472
}
resource "vultr_firewall_rule" "node_traffic_4" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "udp"
  from_port         = 4789
}
resource "vultr_firewall_rule" "node_traffic_5" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 9099
}
resource "vultr_firewall_rule" "node_traffic_6" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 10250
}
resource "vultr_firewall_rule" "node_traffic_7" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 10254
}
resource "vultr_firewall_rule" "node_traffic_8" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 30000
  to_port           = 32767
}
resource "vultr_firewall_rule" "node_traffic_9" {
  firewall_group_id = vultr_firewall_group.for_rancher_node_develop.id
  network           = "0.0.0.0/0"
  protocol          = "udp"
  from_port         = 30000
  to_port           = 32767
}