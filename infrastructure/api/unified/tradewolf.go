package unified

import (
	"fmt"
	"github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	"github.com/tidwall/gjson"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"io/ioutil"
	"net/http"
	url2 "net/url"
	"sync"
	"time"
)

const (
	TRADEWOLF_BASE_URL = "https://us-central1-tradewolf.cloudfunctions.net"
)

func NewTradewolfApi() (*TradewolfApiClient, error) {
	cli := &http.Client{}
	cli.Timeout = 20 * time.Second
	api := &TradewolfApiClient{
		BaseURL:         TRADEWOLF_BASE_URL,
		rateLastUpdated: time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC),
		HttpClient:      cli,
	}
	return api, nil
}

type TradewolfApiClient struct {
	BaseURL           string
	RateCacheDuration time.Duration
	rateLastUpdated   time.Time
	volumeMap         map[string]map[string]float64
	rateMap           map[string]map[string]float64
	orderBookTickMap  map[string]map[string]models.OrderBookTick
	precisionMap      map[string]map[string]models.Precisions
	boardCache        *cache.Cache
	boardTickerCache  *cache.Cache
	currencyPairs     []models.CurrencyPair

	HttpClient *http.Client

	settlements  []string
	m            *sync.Mutex
	rateM        *sync.Mutex
	currencyM    *sync.Mutex
	boardTickerM *sync.Mutex
}

func (h *TradewolfApiClient) publicApiUrl(command string) string {
	return h.BaseURL + command
}

func (h *TradewolfApiClient) getRequest(path string) ([]byte, error) {
	url := h.publicApiUrl(path)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return []byte{}, err
	}
	resp, err := h.HttpClient.Do(req)
	if err != nil {
		return []byte{}, errors.Wrapf(err, "failed to fetch %s", url)
	}
	defer resp.Body.Close()
	byteArray, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, errors.Wrapf(err, "failed to fetch %s", url)
	}
	return byteArray, nil
}

func (h *TradewolfApiClient) GetBoards(exchange string) (map[string]map[string]models.Board, error) {
	args := url2.Values{}
	args.Add("exchange", exchange)
	byteArray, err := h.getRequest("/GetOrderBook?" + args.Encode())
	if err != nil {
		return nil, errors.Wrapf(err, "failed to fetch %s", exchange)
	}
	value := gjson.ParseBytes(byteArray)
	if !value.Exists() {
		return nil, errors.New("failed to parse json: this is not exists")
	}
	if !value.IsArray() {
		return nil, errors.New(fmt.Sprintf("failed to parse json: this is not array %s", string(byteArray)))
	}
	boards := make(map[string]map[string]models.Board)

	for _, v := range value.Array() {
		settlement := v.Get("settlement").String()
		trading := v.Get("trading").String()
		if !v.Get("asks").IsArray() || !v.Get("asks").Exists() {
			continue
		}
		asks := v.Get("asks").Array()
		if !v.Get("bids").IsArray() || !v.Get("bids").Exists() {
			continue
		}
		bids := v.Get("bids").Array()

		bidBoardBars := make([]models.BoardBar, 0)
		askBoardBars := make([]models.BoardBar, 0)
		for _, ask := range asks {
			price := ask.Get("price").Float()
			amount := ask.Get("amount").Float()
			askBoardBars = append(askBoardBars, models.BoardBar{
				Price:  price,
				Amount: amount,
				Type:   models.Ask,
			})
		}
		for _, bid := range bids {
			price := bid.Get("price").Float()
			amount := bid.Get("amount").Float()
			bidBoardBars = append(bidBoardBars, models.BoardBar{
				Price:  price,
				Amount: amount,
				Type:   models.Bid,
			})
		}
		board := models.Board{
			Bids: bidBoardBars,
			Asks: askBoardBars,
		}
		m, ok := boards[trading]
		if !ok {
			m = make(map[string]models.Board)
			boards[trading] = m
		}
		m[settlement] = board
	}
	return boards, nil
}
