package usecase

import (
	"context"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"strconv"
	"time"
)

func NewUpdateNonceUsecase(
	userRepo interfaces.UserRepository,
	nonceRepo interfaces.NonceRepository,
	logger utils.Logger,
) *UpdateNonceUsecase {
	return &UpdateNonceUsecase{
		userRepo:  userRepo,
		nonceRepo: nonceRepo,
		logger:    logger,
	}
}

type UpdateNonceUsecase struct {
	userRepo  interfaces.UserRepository
	nonceRepo interfaces.NonceRepository
	logger    utils.Logger
}

func (u *UpdateNonceUsecase) Exec(ctx context.Context, apiKey string, apiTimestamp string) error {
	// apiTimestamp: UnixTime Millisecond in UTC
	timestampUnix, err := strconv.Atoi(apiTimestamp)
	if err != nil {
		return qerror.New(model.ErrLogicTimestampInvalid)
	}
	if time.Now().UTC().Add(time.Second*60).UnixNano()/int64(time.Millisecond) < int64(timestampUnix) {
		return qerror.New(model.ErrLogicTimestampInvalid)
	}
	if int64(timestampUnix) < time.Now().UTC().Add(time.Second*-60).UnixNano()/int64(time.Millisecond) {
		return qerror.New(model.ErrLogicTimestampInvalid)
	}
	uid, err := u.userRepo.GetUidByApiKey(ctx, apiKey)
	if err != nil {
		return err
	}
	nonce, err := u.nonceRepo.GetNonceByUid(ctx, uid)
	if err != nil {
		return err
	}
	if int64(timestampUnix) <= (nonce.UnixNano() / int64(time.Millisecond)) {
		return qerror.New(model.ErrLogicTimestampInvalid)
	}
	nonceToWrite := time.Unix(0, int64(timestampUnix)*int64(time.Millisecond))
	go u.nonceRepo.UpdateNonceByUid(ctx, uid, nonceToWrite)
	return nil
}
