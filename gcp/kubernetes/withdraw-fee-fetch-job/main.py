from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time, json, os
import urllib.request
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import redis

def binance():
    withdraw_fees_dict = {}
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(options=options)
 
    driver.get('https://www.binance.com/en/fee/deposit')
    #time.sleep(2)
    #driver.save_screenshot('search_results.png')
    trs = driver.find_elements_by_css_selector("#__next > div > main > div > div:nth-child(4) > div > div.bnc-table-wrapper > div > div > div.bnc-table-scroll > div > table > tbody > tr")
    for tr in trs:
        tds = tr.find_elements_by_css_selector("td")
        coins = tds[0].find_elements_by_css_selector("div > div > div")
        coin = ""
        withdraw_fee = 0
        if len(coins) > 1:
            coin = coins[0].get_attribute('innerHTML') 
            withdraw_fees = tds[5].find_elements_by_css_selector("div > div ")
            withdraw_fee = max(float(withdraw_fees[0].text),float(withdraw_fees[1].text))            
        else:
            coin = coins[0].get_attribute('innerHTML')
            withdraw_fee = float(tds[5].text)
        withdraw_fees_dict[coin] = withdraw_fee
    driver.quit()
    return withdraw_fees_dict
 
def hitbtc():
    withdraw_fees = {}
    endpoint = "https://api.hitbtc.com/api/2/public/currency"
    response = urllib.request.urlopen(endpoint)
    data = json.loads(response.read().decode('utf8'))
    for c in data:
        coin = ""
        withdraw_fee = 0
        if c["delisted"] == True or not c["transferEnabled"] or not c["payinEnabled"]:
            pass
        else:
            coin = c["id"]
            if 'payoutFee' in c:
                withdraw_fee = float(c["payoutFee"])
            withdraw_fees[coin] = withdraw_fee
    return withdraw_fees

def huobi():
    withdraw_fees = {}
    endpoint = "https://api.huobi.pro/v2/reference/currencies"
    headers = {
        "Content-Type" : "application/json",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0",
        }
    request = urllib.request.Request(endpoint, headers=headers)
    response = urllib.request.urlopen(request)
    data = json.loads(response.read().decode('utf8'))
    for c in data["data"]:
        withdraw_fee = 0
        for chain in c["chains"]:
            if chain["withdrawStatus"] == "allowed":
                if chain["withdrawFeeType"] == "fixed":
                    withdraw_fee = max(float(chain["transactFeeWithdraw"]), withdraw_fee)
                elif chain["withdrawFeeType"] == "circulated":
                    withdraw_fee = max(float(chain["minTransactFeeWithdraw"]), withdraw_fee)
                    withdraw_fee = max(float(chain["maxTransactFeeWithdraw"]), withdraw_fee)
        coin = c["currency"].upper()
        withdraw_fees[coin] = withdraw_fee
    return withdraw_fees

def kucoin():
    withdraw_fees = {}
    endpoint = "https://api.kucoin.com/api/v1/currencies"
    headers = {
        "Content-Type" : "application/json",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0",
        }
    request = urllib.request.Request(endpoint, headers=headers)
    response = urllib.request.urlopen(request)
    data = json.loads(response.read().decode('utf8'))
    for c in data["data"]:
        if c["isWithdrawEnabled"]:
            withdraw_fee = 0
            withdraw_fee = float(c["withdrawalMinFee"])
            coin = c["name"].upper()
            withdraw_fees[coin] = withdraw_fee
    return withdraw_fees

def poloniex():
    withdraw_fees = {}
    endpoint = "https://poloniex.com/public?command=returnCurrencies"
    headers = {
        "Content-Type" : "application/json",
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:47.0) Gecko/20100101 Firefox/47.0",
        }
    request = urllib.request.Request(endpoint, headers=headers)
    response = urllib.request.urlopen(request)
    data = json.loads(response.read().decode('utf8'))
    for c in data.keys():
        withdraw_fee = data[c]["txFee"]
        coin = c
        withdraw_fees[coin] = withdraw_fee
    return withdraw_fees

REDIS_HOST = "localhost"
REDIS_PORT = 6379

if __name__ == '__main__':
    if(os.environ["REDIS_HOST"]):
        REDIS_HOST = os.environ["REDIS_HOST"]
    
    redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=0)
    withdrawal_fee_fetcher = {"binance":binance, "kucoin":kucoin, "hitbtc":hitbtc, "huobi":huobi, "poloniex":poloniex}
    for exchange in withdrawal_fee_fetcher.keys():
        # key: [withdrawals]:[exchange]:[currency] <= number
        _, delete_keys = redis_client.scan(match="{}:{}:*".format("withdrawal_fees", exchange))
        if(len(delete_keys)>0):
            redis_client.delete(*delete_keys)
        fees = withdrawal_fee_fetcher[exchange]()
        for k in fees.keys():
            redis_key = "{}:{}:{}".format("withdrawal_fees", exchange, k)
            redis_client.set(redis_key, float(fees[k]))
