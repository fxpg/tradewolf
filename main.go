package main

import (
	"math"
	"math/rand"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/leaanthony/mewn"
	"github.com/wailsapp/wails"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/presenter/handler"
	"gitlab.com/fxpg/tradewolf/registry"
	"go.etcd.io/bbolt"
)

func random(min, max float64) float64 {
	rand.Seed(time.Now().UnixNano())
	return rand.Float64()*(max-min) + min
}
func roundInt(num float64) float64 {
	t := math.Trunc(num)
	if math.Abs(num-t) >= 0.5 {
		return t + math.Copysign(1, num)
	}
	return t
}

func main() {
	db, err := gorm.Open("sqlite3", "./tradewolf.db")
	if err != nil {
		panic("failed to connect sqlite")
	}
	defer db.Close()
	// db.LogMode(true)

	db.AutoMigrate(&entity.KeySetting{})
	db.AutoMigrate(&entity.ArbitrageOrder{})
	db.AutoMigrate(&entity.ArbitrageFilledOrder{})
	db.AutoMigrate(&entity.ArbitragerLog{})
	bboltDb, err := bbolt.Open("./arbitrager.db", 0600, nil)
	if err != nil {
		panic("failed to connect kvs")
	}

	c, err := registry.New("./config.yml", db, bboltDb)
	if err != nil {
		panic(err)
	}
	js := mewn.String("./frontend/dist/app.js")
	html := mewn.String("./frontend/dist/index.html")
	css := mewn.String("./frontend/dist/app.css")

	app := wails.CreateApp(&wails.AppConfig{
		Width:  1440,
		Height: 960,
		Title:  "tradewolf",
		HTML:   html,
		JS:     js,
		CSS:    css,
		Colour: "#131313",
	})
	err = c.Invoke(func(ah handler.ArbitrageHandler) {
		app.Bind(ah)
	})
	if err != nil {
		panic(err)
	}
	err = c.Invoke(func(e handler.ExchangeHandler) {
		app.Bind(e)
	})
	if err != nil {
		panic(err)
	}
	app.Run()
}
