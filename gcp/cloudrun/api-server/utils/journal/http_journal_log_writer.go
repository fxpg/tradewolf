package journal

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"io"
	"net/http"
	"strings"
	"time"
)

type mapItem struct {
	Key string
	Value interface{}
}


// https://golang.org/pkg/strconv/#Quote
var replacer = strings.NewReplacer("\t", "\\t", "\n", "\\n", "\xFF", "\\xFF", "\u0100", "\\u0100")

func escape(src string) string {
	return replacer.Replace(src)
}

func ltsv(w io.Writer, m []mapItem) {
	for i, e := range m {
		w.Write([]byte(escape(strings.ToLower(e.Key))))
		w.Write([]byte{':'})

		value := e.Value
		switch value.(type) {
		case map[string]interface{}:
		case map[string][]string:
		case http.Header:
			valueJson, err := json.Marshal(value)
			if err != nil {
				value = "ERROR"
			}
			value = string(valueJson)
		}
		w.Write([]byte(escape(fmt.Sprint(value))))
		if i != len(m)-1 {
			w.Write([]byte{'\t'})
		}
	}
}

func ltsvString(m []mapItem) string {
	buf := &bytes.Buffer{}
	ltsv(buf, m)
	return buf.String()
}

type HttpRequestJournalLogWriter struct {
	logger utils.Logger
}

func NewHttpRequestJournalLogWriter(logger utils.Logger) middlewares.HttpRequestJournalWriter {
	return &HttpRequestJournalLogWriter{
		logger: logger,
	}
}

func (w *HttpRequestJournalLogWriter) Write(req *middlewares.RequestInfo) {
	w.logger.Printf(ltsvString([]mapItem{
		{"type", "I"},
		{"timestamp", time.Now()},
		{"remoteip", req.Request.RemoteAddr},
		{"path", req.Request.URL.Path},
		{"method", req.Request.Method},
		{"useragent", req.Request.Header.Get("User-Agent")},
		{"requestid", req.RequestId},
		{"reqheaders", req.Request.Header},
		{"reqquerystring", req.Request.URL.RawQuery},
		{"reqbody", string(req.RequestBody)},
	}))
}


type HttpResponseJournalLogWriter struct {
	logger utils.Logger
}

func NewHttpResponseJournalLogWriter(logger utils.Logger) middlewares.HttpResponseJournalWriter {
	return &HttpResponseJournalLogWriter{
		logger: logger,
	}
}

func (w *HttpResponseJournalLogWriter) Write(res *middlewares.ResponseInfo) {
	w.logger.Printf(ltsvString([]mapItem{
		{"type", "O"},
		{"timestamp", time.Now()},
		{"requestid", res.RequestId},
		{"statuscode", res.StatusCode},
		{"reqtimems", res.RequestTime.Milliseconds()},
		{"resheaders", res.ResponseHeader},
		{"resbody", string(res.ResponseBody)},
	}))
}
