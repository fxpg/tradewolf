package registry

import (
	"testing"

	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/tradewolf/application/usecase"
	"go.etcd.io/bbolt"
)

func TestInvoke(t *testing.T) {
	c, err := New("../config.yml", &gorm.DB{}, &bbolt.DB{})
	if err != nil {
		t.Fatalf("error whild New: %+v", err)
	}
	if err := c.Invoke(func(ao usecase.ArbitrageUsecase) {
	}); err != nil {
		t.Fatalf("error whild c.Invoke: %+v", err)
	}
}
