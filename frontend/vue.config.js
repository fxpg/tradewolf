const webpack = require('webpack')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
let webpackPlugins = [
  // new HardSourceWebpackPlugin()
  // new VuetifyLoaderPlugin()
]

let cssConfig = {}
if (process.env.NODE_ENV == 'production') {
  cssConfig = {
    extract: {
      filename: '[name].css'
    }
  }
  webpackPlugins = [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1 // disable creating additional chunks
    })
  ]
}

module.exports = {
  devServer: {
    disableHostCheck: true
  },
  css: cssConfig,
  filenameHashing: false,
  chainWebpack: config => {
    let limit = 9999999999999999
    config.module
      .rule('images')
      .test(/\.(png|gif|jpg)(\?.*)?$/i)
      .use('url-loader')
      .loader('url-loader')
      .tap(options => Object.assign(options, { limit: limit }))
    config.module
      .rule('fonts')
      .test(/\.(woff2?|eot|ttf|otf|svg)(\?.*)?$/i)
      .use('url-loader')
      .loader('url-loader')
      .options({
        limit: limit
      })
  },
  configureWebpack: {
    output: {
      filename: 'app.js'
    },
    optimization: {
      splitChunks: false
    },
    plugins: webpackPlugins,
    resolve: {
      alias: {
        vue$: 'vue/dist/vue.esm.js'
      }
    }
  },
  transpileDependencies: ['vuetify', 'vuex-module-decorators'],
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }
}
