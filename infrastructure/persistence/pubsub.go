package persistence

import (
	"context"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/pkg/errors"
)

type PubsubRepositoryGcp struct {
	Client *pubsub.Client `inject:""`
}

func (r *PubsubRepositoryGcp) CreateTopic(topicName string) error {
	ctx := context.Background()
	topic := r.Client.Topic(topicName)
	exists, err := topic.Exists(ctx)
	if err != nil {
		return err
	}
	if !exists {
		r.Client.CreateTopic(ctx, topicName)
		return nil
	}
	return nil
}

func (r *PubsubRepositoryGcp) PublishLongOpportunity(topicName, exchange, oppositionExchange, trading, settlement, expectedValue, triggerPrice string) error {
	ctx := context.Background()
	topic := r.Client.Topic(topicName)
	result := topic.Publish(ctx, &pubsub.Message{
		Attributes: map[string]string{
			"exchange":           exchange,
			"oppositionExchange": oppositionExchange,
			"trading":            trading,
			"settlement":         settlement,
			"expectedValue":      expectedValue,
			"triggerPrice":       triggerPrice,
		},
		Data: []byte("there is a long trade opportunity"),
	})
	_, err := result.Get(ctx)
	if err != nil {
		return err
	}
	return nil
}

func (r *PubsubRepositoryGcp) PublishMessage(topicName string, message string) error {
	ctx := context.Background()
	topic := r.Client.Topic(topicName)
	result := topic.Publish(ctx, &pubsub.Message{
		Data: []byte(message),
	})
	_, err := result.Get(ctx)
	if err != nil {
		return err
	}
	return nil
}
func (r *PubsubRepositoryGcp) CreateSubscribe(subscriptionName, topicName string) (*pubsub.Subscription, error) {
	ctx := context.Background()
	topic := r.Client.Topic(topicName)
	result, err := topic.Exists(ctx)
	if err != nil {
		return nil, err
	}
	if !result {
		return nil, errors.Errorf("there is no topic %s", topicName)
	}
	subscription, err := r.Client.CreateSubscription(ctx, subscriptionName, pubsub.SubscriptionConfig{
		Topic:       topic,
		AckDeadline: 20 * time.Second,
	})
	if err != nil {
		return nil, err
	}
	return subscription, nil
}
func (r *PubsubRepositoryGcp) PullMessage(subscription *pubsub.Subscription, topicName string, cancelContext context.Context, f func(ctx context.Context, msg *pubsub.Message)) error {
	ctx := context.Background()
	topic := r.Client.Topic(topicName)
	result, err := topic.Exists(ctx)
	if err != nil {
		return err
	}
	if !result {
		return errors.Errorf("there is no topic %s", topicName)
	}
	err = subscription.Receive(cancelContext, f)
	if err != nil {
		return err
	}
	return nil
}

func (r *PubsubRepositoryGcp) DeleteSubscribe(subscriptionName string) error {
	ctx := context.Background()
	subscription := r.Client.Subscription(subscriptionName)
	err := subscription.Delete(ctx)
	if err != nil {
		return err
	}
	return nil
}
