package handler

import (
	"fmt"

	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/fxpg/tradewolf/application/usecase"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
)

type ExchangeHandler interface {
	GetPairs(string) ([]entity.CurrencyPair, error)
	GetBoard(string, string) (*entity.Board, error)
	GetOrderBookTicks() ([]entity.OrderBookTick, error)
	GetPrecise(exchange string, trading string, settlement string) (*models.Precisions, error)

	GetExchanges() ([]string, error)
	SetKey(settingProto string) error
	SetApiKey(exchange string, apiKey string) error
	SetSecKey(exchange string, secKey string) error
	GetKey(exchange string) (*entity.KeySetting, error)
	GetApiKey(exchange string) (string, error)
	GetSecKey(exchange string) (string, error)
	DeleteKey(exchange string) (bool, error)
}

func NewExchangeHandler(usecase usecase.ExchangeUsecase) ExchangeHandler {
	return &exchangeHandler{
		usecase,
	}
}

type exchangeHandler struct {
	usecase usecase.ExchangeUsecase
}

func (e *exchangeHandler) GetExchanges() ([]string, error) {
	return e.usecase.GetExchanges()
}

func (e *exchangeHandler) SetKey(settingProto string) error {
	setting := &entity.KeySetting{}
	err := jsonpb.UnmarshalString(settingProto, setting)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return e.usecase.SetKey(setting)
}

func (e *exchangeHandler) SetApiKey(exchange string, apiKey string) error {
	return e.usecase.SetApiKey(exchange, apiKey)
}

func (e *exchangeHandler) SetSecKey(exchange string, secKey string) error {
	return e.usecase.SetSecKey(exchange, secKey)
}

func (e *exchangeHandler) GetKey(exchange string) (*entity.KeySetting, error) {
	return e.usecase.GetKey(exchange)
}

func (e *exchangeHandler) GetApiKey(exchange string) (string, error) {
	return e.usecase.GetApiKey(exchange)
}

func (e *exchangeHandler) GetSecKey(exchange string) (string, error) {
	return e.usecase.GetSecKey(exchange)
}

func (e *exchangeHandler) DeleteKey(exchange string) (bool, error) {
	return e.usecase.DeleteKey(exchange)
}

func (e *exchangeHandler) GetPrecise(exchange string, trading string, settlement string) (*models.Precisions, error) {
	return e.usecase.GetPrecise(exchange, trading, settlement)
}

func (e *exchangeHandler) GetPairs(exchange string) ([]entity.CurrencyPair, error) {
	return e.usecase.GetPairs(exchange)
}

func (e *exchangeHandler) GetOrderBookTicks() ([]entity.OrderBookTick, error) {
	return e.usecase.GetOrderBookTicks()
}

func (e *exchangeHandler) GetBoard(exchange string, pairProtoString string) (*entity.Board, error) {
	currencyPair := &entity.CurrencyPair{}
	err := jsonpb.UnmarshalString(pairProtoString, currencyPair)
	if err != nil {
		return nil, err
	}

	return e.usecase.GetBoard(exchange, currencyPair)
}
