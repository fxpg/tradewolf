package controller

var Providers = []interface{}{
	NewArbitragePairsController,
	NewOrderBooksController,
	NewWithdrawalFeesController,
}
