package model

type WithdrawalFee struct {
	Currency string
	Amount   float64
}

type WithdrawalFees []WithdrawalFee