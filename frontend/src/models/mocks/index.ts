import {
  ArbitragePair,
  CurrencyPair,
  ArbitragePairArbitrage,
  OrderBookTick,
  BoardBar,
  Board,
  Log,
  LogType
} from '@/models/domain_pb'
import { permutation } from '@/utils/math'
import { RandomString } from '@/utils/strings'

class AskGenerator {
  basePrice: number
  constructor(basePrice: number) {
    this.basePrice = basePrice
  }
  generateBoardBar(): BoardBar {
    const askBoardBar = new BoardBar()
    askBoardBar.setPrice(this.basePrice)
    askBoardBar.setAmount(Math.random() * (1000 - 1) + 1)
    this.basePrice = this.basePrice + Math.floor(Math.random() * 1000) / 1000
    return askBoardBar
  }
}
class BidGenerator {
  basePrice: number
  constructor(basePrice: number) {
    this.basePrice = basePrice
  }
  generateBoardBar(): BoardBar {
    const bidBoardBar = new BoardBar()
    bidBoardBar.setPrice(this.basePrice)
    bidBoardBar.setAmount(Math.random() * (1000 - 1) + 1)
    this.basePrice = this.basePrice - Math.floor(Math.random() * 1000) / 1000
    return bidBoardBar
  }
}
export function NewMockBoard(
  exchange: string,
  currencyPair: CurrencyPair
): Board {
  const askBoardBars = Array<BoardBar>()
  const askCounter = Math.random() * (200 - 100) + 100
  const askGenerator = new AskGenerator(Math.random() * (1000 - 600) + 100)
  for (let counter = 0; counter < askCounter; ) {
    askBoardBars.push(askGenerator.generateBoardBar())
    counter++
  }
  const bidBoardBars = Array<BoardBar>()
  const bidCounter = Math.random() * (200 - 100) + 100
  const bidGenerator = new BidGenerator(Math.random() * (1000 - 600) + 100)
  for (let counter = 0; counter < bidCounter; ) {
    bidBoardBars.push(bidGenerator.generateBoardBar())
    counter++
  }
  const board = new Board()
  board.setAsksList(askBoardBars)
  board.setBidsList(bidBoardBars)
  board.setExchange(exchange)
  board.setCurrencyPair(currencyPair)
  return board
}

export function NewMockArbitragePairs(): ArbitragePair[] {
  const exchanges = ['kucoin', 'binance', 'quoine', 'hitbtc', 'kraken']
  const exchangeCombinations = permutation(exchanges, 2)
  const arbitragePairs: ArbitragePair[] = Array<ArbitragePair>()
  const currencyPairs = NewMockCurrencyPairs()
  exchangeCombinations.forEach((exchangeCombination: any) => {
    currencyPairs.forEach((currencyPair: CurrencyPair) => {
      const arbitragePair = new ArbitragePair()
      arbitragePair.setCurrencyPair(currencyPair)
      arbitragePair.setExchangeA(exchangeCombination[0])
      arbitragePair.setExchangeB(exchangeCombination[1])
      arbitragePairs.push(arbitragePair)
    })
  })
  return arbitragePairs
}

export function NewMockCurrencyPairs(): CurrencyPair[] {
  const settlements = ['BTC', 'ETH', 'USDT']
  const tradings = [
    'XRP',
    'EOS',
    'NEO',
    'LTC',
    'BCHSV',
    'TRX',
    'VSYS',
    'ADA',
    'ATOM',
    'ALGO',
    'TOMO',
    'NANO',
    'ONT',
    'MKR',
    'MTV',
    'XLM',
    'DCR',
    'CRO'
  ]
  let currencyPairs = Array<CurrencyPair>()
  settlements.forEach((settlement: string) => {
    tradings.forEach((trading: string) => {
      const currencyPair = new CurrencyPair()
      currencyPair.setSettlement(settlement)
      currencyPair.setTrading(trading)
      currencyPairs.push(currencyPair)
    })
  })
  return currencyPairs
}
function generateOrderBookTick(): number[] {
  const max = 105
  const min = 95
  const basePriceA = Math.floor(Math.random() * (max + 1 - min)) + min
  const basePriceB = Math.floor(Math.random() * (max + 1 - min)) + min
  return [Math.max(basePriceA, basePriceB), Math.min(basePriceA, basePriceB)]
}

export function NewMockArbitragePairArbitrage(): ArbitragePairArbitrage[] {
  const arbitragesPairArbitrage: ArbitragePairArbitrage[] = Array<
    ArbitragePairArbitrage
  >()
  NewMockArbitragePairs().forEach((arbitragePair: ArbitragePair) => {
    const arbitragePairArbitrage = new ArbitragePairArbitrage()
    const orderBookTickA = new OrderBookTick()
    const askBidA = generateOrderBookTick()
    orderBookTickA.setCurrencyPair(arbitragePair.getCurrencyPair())
    orderBookTickA.setExchange(arbitragePair.getExchangeA())
    orderBookTickA.setBestAskPrice(askBidA[0])
    orderBookTickA.setBestBidPrice(askBidA[1])
    const orderBookTickB = new OrderBookTick()
    const askBidB = generateOrderBookTick()
    orderBookTickB.setCurrencyPair(arbitragePair.getCurrencyPair())
    orderBookTickB.setExchange(arbitragePair.getExchangeB())
    orderBookTickB.setBestAskPrice(askBidB[0])
    orderBookTickB.setBestBidPrice(askBidB[1])
    arbitragePairArbitrage.setOrderBookTickA(orderBookTickA)
    arbitragePairArbitrage.setOrderBookTickB(orderBookTickB)
    arbitragesPairArbitrage.push(arbitragePairArbitrage)
  })
  return arbitragesPairArbitrage
}

export function NewMockLogs(arbitragerId: number): Log[] {
  const logs: Log[] = Array<Log>()
  const logCounter = Math.random() * (200 - 100) + 100
  const logType = [LogType.INFO, LogType.WARN, LogType.ERROR]
  for (let counter = 0; counter < logCounter; ) {
    const log = new Log()
    log.setCategory('arbitrager' + String(arbitragerId))
    log.setLogType(logType[Math.floor(Math.random() * logType.length)])
    log.setMessage(RandomString(255))
    logs.push(log)
    counter++
  }
  return logs
}
