package service

import (
	"context"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	mock_service "gitlab.com/fxpg/tradewolf/domain/service/mock"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
)

func NewArbitrageOrderRepositoryMock(t *testing.T, calls ...*gomock.Call) ArbitrageOrderRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockArbitrageOrderRepository(ctrl)
	m.EXPECT().CreateOrder(gomock.Any()).Return(nil).AnyTimes()
	m.EXPECT().FilledOrder(gomock.Any()).Return(nil).AnyTimes()
	return m
}

func NewPublicRepositoryMock(t *testing.T, calls ...*gomock.Call) PublicResourceRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockPublicResourceRepository(ctrl)
	bidsKucoin := []models.BoardBar{
		models.BoardBar{
			Price:  100,
			Amount: 10,
		}, models.BoardBar{
			Price:  110,
			Amount: 10,
		}, models.BoardBar{
			Price:  120,
			Amount: 10,
		},
	}
	asksKucoin := []models.BoardBar{
		models.BoardBar{
			Price:  130,
			Amount: 10,
		}, models.BoardBar{
			Price:  140,
			Amount: 10,
		}, models.BoardBar{
			Price:  150,
			Amount: 10,
		},
	}
	boardKucoin := &models.Board{
		Bids: bidsKucoin,
		Asks: asksKucoin,
	}
	bidsBinance := []models.BoardBar{
		models.BoardBar{
			Price:  125,
			Amount: 10,
		}, models.BoardBar{
			Price:  135,
			Amount: 10,
		}, models.BoardBar{
			Price:  145,
			Amount: 10,
		},
	}
	asksBinance := []models.BoardBar{
		models.BoardBar{
			Price:  155,
			Amount: 10,
		}, models.BoardBar{
			Price:  165,
			Amount: 10,
		}, models.BoardBar{
			Price:  175,
			Amount: 10,
		},
	}
	boardBinance := &models.Board{
		Bids: bidsBinance,
		Asks: asksBinance,
	}
	m.EXPECT().Board("kucoin", gomock.Any(), gomock.Any()).Return(boardKucoin, nil).AnyTimes()
	m.EXPECT().Board("binance", gomock.Any(), gomock.Any()).Return(boardBinance, nil).AnyTimes()

	return m
}

func NewPrivateRepositoryMockForTest(t *testing.T) PrivateResourceRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockPrivateResourceRepository(ctrl)
	m.EXPECT().Order(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return("2000", nil).AnyTimes()
	m.EXPECT().IsOrderFilled("kucoin", gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil).AnyTimes()
	m.EXPECT().IsOrderFilled("binance", gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil).AnyTimes()
	return m
}

func NewCurrencyPairMock() *entity.CurrencyPair {
	return &entity.CurrencyPair{
		Trading:    "ETH",
		Settlement: "BTC",
	}
}

func NewArbitragePairMock() *entity.ArbitragePair {
	return &entity.ArbitragePair{
		ExchangeA:    "kucoin",
		ExchangeB:    "binance",
		CurrencyPair: NewCurrencyPairMock(),
	}
}

func NewArbitragerSettingMock(arbitragerOperation entity.ArbitragerOperation) *entity.ArbitragerSetting {
	return &entity.ArbitragerSetting{
		ArbitragePair:       NewArbitragePairMock(),
		ArbitragerMode:      entity.ArbitragerMode_REAL,
		ArbitragerOperation: arbitragerOperation,
		ArbitragerState:     entity.ArbitragerState_STOPPED,
		ExpectedProfitRate:  0.05,
		LossCutRate:         0.1,
		ArbitrageWaitLimit:  6000,
	}
}

func NewArbitragerLogRepositoryMockForTest(t *testing.T) ArbitragerLogRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockArbitragerLogRepository(ctrl)
	m.EXPECT().Insert(gomock.Any()).Return(nil).AnyTimes()
	return m
}

func TestArbitrager_GetID(t *testing.T) {
	arbitrager := NewArbitrager(1000, NewPublicRepositoryMock(t), NewPrivateRepositoryMockForTest(t), nil, NewArbitrageOrderRepositoryMock(t), NewArbitragerLogRepositoryMockForTest(t), NewArbitragerSettingMock(entity.ArbitragerOperation_LONG_SIDE), func(log entity.ArbitragerLog) {})
	id, err := arbitrager.GetID()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if id != 1000 {
		t.Fatalf("failed test")
	}
}

func TestArbitrager_GetBoards(t *testing.T) {
	arbitrager := NewArbitrager(1000, NewPublicRepositoryMock(t), NewPrivateRepositoryMockForTest(t), nil, NewArbitrageOrderRepositoryMock(t), NewArbitragerLogRepositoryMockForTest(t), NewArbitragerSettingMock(entity.ArbitragerOperation_LONG_SIDE), func(log entity.ArbitragerLog) {})
	arbitrage, err := arbitrager.FindArbitrage()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if arbitrage == nil {
		t.Fatalf("failed test")
	}
	if arbitrage.BuySide.Rate != 130 {
		t.Fatalf("failed test buy %#v", arbitrage.BuySide)
	}
	if arbitrage.BuySide.Exchange != "kucoin" {
		t.Fatalf("failed test buy %#v", arbitrage.BuySide)
	}
	if arbitrage.SellSide.Rate != 145 {
		t.Fatalf("failed test sell %#v", arbitrage.SellSide)
	}
	if arbitrage.SellSide.Exchange != "binance" {
		t.Fatalf("failed test sell %#v", arbitrage.BuySide)
	}
	arbitrager.SetTimeDuration(time.Microsecond)
	err = arbitrager.ExecuteArbitrage(context.Background(), arbitrage)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	err = arbitrager.SetStatus(entity.ArbitragerState_STOPPED)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	status, err := arbitrager.GetStatus()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if status != entity.ArbitragerState_STOPPED {
		t.Fatalf("failed test status %#v", status)
	}
	boards, err := arbitrager.GetBoards()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if len(boards) != 2 {
		t.Fatalf("failed test boards %#v", boards)
	}
	err = arbitrager.SetStatus(entity.ArbitragerState_STOPPED)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	arbitrager.SetTimeDuration(time.Hour)
	ctx1, cancel1 := context.WithCancel(context.Background())
	done := make(chan error, 0)
	go func(ctx1 context.Context) {
		err = arbitrager.ExecuteArbitrage(ctx1, arbitrage)
		done <- err
	}(ctx1)
	cancel1()
	err = <-done
	if err == nil {
		t.Fatalf("failed test %#v", err)
	}
	arbitrager = NewArbitrager(1000, NewPublicRepositoryMock(t), NewPrivateRepositoryMockForTest(t), nil, NewArbitrageOrderRepositoryMock(t), nil, NewArbitragerSettingMock(entity.ArbitragerOperation_SHORT_SIDE), func(entity.ArbitragerLog) {})
	arbitrager.SetTimeDuration(time.Microsecond)
	arbitrage, err = arbitrager.FindArbitrage()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if arbitrage == nil {
		t.Fatalf("failed test")
	}
	err = arbitrager.ExecuteArbitrage(context.Background(), arbitrage)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	arbitrager = NewArbitrager(1000, NewPublicRepositoryMock(t), NewPrivateRepositoryMockForTest(t), nil, NewArbitrageOrderRepositoryMock(t), nil, NewArbitragerSettingMock(entity.ArbitragerOperation_BOTH_SIDE), func(entity.ArbitragerLog) {})
	arbitrager.SetTimeDuration(time.Microsecond)
	arbitrage, err = arbitrager.FindArbitrage()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if arbitrage == nil {
		t.Fatalf("failed test")
	}
	err = arbitrager.ExecuteArbitrage(context.Background(), arbitrage)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
}
