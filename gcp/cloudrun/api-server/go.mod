module gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server

go 1.13

require (
	cloud.google.com/go/firestore v1.1.1
	firebase.google.com/go v3.12.0+incompatible // indirect
	github.com/antonholmquist/jason v1.0.0
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496 // indirect
	github.com/go-openapi/analysis v0.19.7 // indirect
	github.com/go-openapi/errors v0.19.3
	github.com/go-openapi/loads v0.19.4
	github.com/go-openapi/runtime v0.19.10
	github.com/go-openapi/spec v0.19.5
	github.com/go-openapi/strfmt v0.19.4
	github.com/go-openapi/swag v0.19.6
	github.com/go-openapi/validate v0.19.5
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/golang/mock v1.3.1
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/jessevdk/go-flags v1.4.0
	github.com/kr/pretty v0.2.0 // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/tattsun/qerror v0.0.0-20200208133645-baf7cf401cc4
	go.mongodb.org/mongo-driver v1.2.1 // indirect
	go.uber.org/dig v1.8.0
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/sys v0.0.0-20200113162924-86b910548bc1 // indirect
	golang.org/x/tools v0.0.0-20200114052453-d31a08c2edf2 // indirect
	google.golang.org/api v0.14.0
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
