# tradewolf

## how to run

- return below in terminal
- NOTE: in windows, you must change `binaryname` in `package.json` from `'tradewolf'` to `'tradewolf.exe'` before run 

```bash
wails serve
```

- return below in another terminal

```bash
cd frontend
npm run-script dev
```
