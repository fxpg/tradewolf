package driver

import (
	"context"
	"fmt"
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"strings"
)

type PubsubClient interface {
	Receive(exchanges []string, receiver chan<- *model.CurrencyPair)
}

func NewPubsubClient() PubsubClient {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		panic(err)
	}
	psc := redis.PubSubConn{Conn: conn}
	return &PubsubClientImpl{
		PubSubConn: psc,
		pool: &redis.Pool{
			MaxIdle: 80,
			// MaxActive: 12000, // max number of connections
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", "localhost:6379")
				if err != nil {
					panic(err.Error())
				}
				return c, err
			},
		},
	}
}

type PubsubClientImpl struct {
	redis.PubSubConn
	pool *redis.Pool
}

func (psc *PubsubClientImpl) subscribeBoardTickerEvent(conn *redis.PubSubConn, exchanges []string) error {
	ctx := context.Background()
	for _, e := range exchanges {
		err := conn.PSubscribe(ctx, fmt.Sprintf("%s:*", e))
		if err != nil {
			return err
		}
	}
	return nil
}

func (psc *PubsubClientImpl) convertChannelToCurrencyPair(channel string) (*model.CurrencyPair, error) {
	currencyPairStrs := strings.Split(channel, ":")
	if len(currencyPairStrs) != 3 {
		return nil, errors.New("failed to parse pubsub channel")
	}
	currencyPair := &model.CurrencyPair{
		Exchange:   currencyPairStrs[0],
		Trading:    currencyPairStrs[1],
		Settlement: currencyPairStrs[2],
	}
	return currencyPair, nil
}

func (p *PubsubClientImpl) Receive(exchanges []string, receiver chan<- *model.CurrencyPair) {
	for {
		// Get a connection from a pool
		c := p.pool.Get()
		psc := &redis.PubSubConn{Conn: c}

		// Set up subscriptions
		p.subscribeBoardTickerEvent(psc, exchanges)

		// While not a permanent error on the connection.
		for c.Err() == nil {
			switch v := psc.Receive().(type) {
			case redis.Message:
				currencyPair, err := p.convertChannelToCurrencyPair(v.Channel)
				if err != nil {
					// fmt.Println(err)
					continue
				}
				receiver <- currencyPair
			case redis.Subscription:
				// fmt.Printf("%s: %s %d\n", v.Channel, v.Kind, v.Count)
			case error:
				fmt.Printf(v.Error())
			}
		}
		c.Close()
	}
}
