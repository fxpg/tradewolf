package repository

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"strconv"
)

func NewOrderBookRedisRepository(cli *redis.Client) interfaces.OrderBookRepository {
	return &OrderBookRedisRepository{cli: cli}
}

type OrderBookRedisRepository struct {
	cli *redis.Client
}

func (r *OrderBookRedisRepository) Get(ctx context.Context, exchange string, currencyPair model.CurrencyPair) (*model.OrderBook, error) {
	// Asks
	askBoardBars := make([]model.BoardBar, 0)
	asks := r.cli.WithContext(ctx).ZRevRangeWithScores(
		getKeyPrefixAsks(exchange, currencyPair),
		0, -1,
	)
	if asks.Err() != nil {
		return nil, qerror.WrapWith(asks.Err(), model.ErrSystemRedisCommandFailed)
	}

	for _, entry := range asks.Val() {
		amount, err := strconv.ParseFloat(entry.Member.(string), 64)
		if err != nil {
			return nil, qerror.WrapWith(err, model.ErrSystemUnexpected, "failed to parse amount as float")
		}
		bar := model.BoardBar{
			Price:  entry.Score,
			Amount: amount,
		}
		askBoardBars = append(askBoardBars, bar)
	}

	// Bids
	bidBoardBars := make([]model.BoardBar, 0)
	bids := r.cli.WithContext(ctx).ZRangeWithScores(
		getKeyPrefixBids(exchange, currencyPair),
		0, -1,
	)
	if bids.Err() != nil {
		return nil, qerror.WrapWith(bids.Err(), model.ErrSystemRedisCommandFailed)
	}

	for _, entry := range bids.Val() {
		amount, err := strconv.ParseFloat(entry.Member.(string), 64)
		if err != nil {
			return nil, qerror.WrapWith(err, model.ErrSystemUnexpected, "failed to parse amount as float")
		}
		bar := model.BoardBar{
			Price:  entry.Score,
			Amount: amount,
		}
		bidBoardBars = append(bidBoardBars, bar)
	}

	//
	return &model.OrderBook{
		CurrencyPair: currencyPair,
		Asks:         askBoardBars,
		Bids:        bidBoardBars,
	}, nil

}

func getKeyPrefixAsks(exchange string, pair model.CurrencyPair) string {
	return getKeyPrefixBase(exchange, pair) + "asks"
}

func getKeyPrefixBids(exchange string, pair model.CurrencyPair) string {
	return getKeyPrefixBase(exchange, pair) + "bids"
}

func getKeyPrefixBase(exchange string, pair model.CurrencyPair) string {
	return fmt.Sprintf("%s:%s:%s:", exchange, pair.Trading, pair.Settlement)
}