package repository

import (
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
)

import "cloud.google.com/go/firestore"

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

func NewWithdrawalFeeFirestoreRepository(cli *firestore.Client) (interfaces.WithdrawalFeeRepository, error) {
	return &WithdrawalFeeFirestoreRepository{
		cli: cli,
	}, nil
}

type WithdrawalFeeFirestoreRepository struct {
	cli *firestore.Client
}

func (r *WithdrawalFeeFirestoreRepository) GetByExchange(ctx context.Context, exchange string) (model.WithdrawalFees, error) {
	res, err := r.cli.Collection("tradewolf/public/withdrawal_fee").Where("exchange", "==", exchange).Documents(ctx).GetAll()
	if err != nil {
		return nil, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}

	ret := make(model.WithdrawalFees, 0, len(res))
	for _, v := range res {
		ret = append(ret, model.WithdrawalFee{
			Currency:v.Data()["currency"].(string),
			Amount:v.Data()["withdrawal_fee"].(float64),
		})
	}
	return ret, nil
}
