package interfaces

import (
	"context"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
)

type OrderBookRepository interface {
	Get(ctx context.Context, exchange string, currencyPair model.CurrencyPair) (*model.OrderBook, error)
}
