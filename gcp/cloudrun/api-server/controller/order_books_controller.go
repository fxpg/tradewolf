package controller

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/controller/mapper"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/restapi/operations"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

func NewOrderBooksController(
	u *usecase.GetOrderBookByExchangeUsecase,
	logger utils.Logger,
	) *OrderBooksController {
		return &OrderBooksController{
			u:      u,
			logger: logger,
		}
}

type OrderBooksController struct {
	u *usecase.GetOrderBookByExchangeUsecase
	logger utils.Logger
}

func (c *OrderBooksController) Get(params operations.GetOrderBookByExchangeParams) middleware.Responder {
	res, err := c.u.Exec(params.HTTPRequest.Context(), params.Exchange)
	if err != nil {
		if model.IsSystemError(err) {
			c.logger.Errorf(err.Error())
		}
		code, resErr := toErrorResponse(err)
		return operations.NewGetOrderBookByExchangeDefault(code).WithPayload(resErr)
	}

	return operations.NewGetOrderBookByExchangeOK().WithPayload(mapper.MapOrderBooks(res))
}