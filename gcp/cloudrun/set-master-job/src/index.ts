import admin, { ServiceAccount } from 'firebase-admin'
import { FirestoreSimple } from 'firestore-simple'

const ROOT_PATH = ''

admin.initializeApp();
const firestore = admin.firestore();

interface Allowance {
  id: string
  allowanceByDay: number
  allowanceByMonth: number
}

(async()=>{
  const firestoreSimple = new FirestoreSimple(firestore)
  const dao = firestoreSimple.collection<Allowance>({ path: `${ROOT_PATH}/allowance` })
  let addAllowances: Allowance[] = []
  addAllowances.push({id:"FREE PLAN", allowanceByDay:5000, allowanceByMonth: 150000})
  addAllowances.push({id:"START-UP PLAN", allowanceByDay:15000, allowanceByMonth: 450000})
  addAllowances.push({id:"BOOST PLAN", allowanceByDay:40000, allowanceByMonth: 1200000})
  const deleteAllowances: Allowance[] = await dao.fetchAll()

  let deleteList :Allowance[][] =[]
  let i = 0;
  while(i < deleteAllowances.length) {
    deleteList.push(deleteAllowances.splice(i, i+249))
    i = i + 249
  }
  for(let dl of deleteList) {
    await dao.bulkDelete(dl.map((allowance,index)=>{return allowance.id}))
  }
  let addList :Allowance[][] =[]
  let j = 0;
  while(j < addAllowances.length) {
    addList.push(addAllowances.splice(j, j+249))
    j = j + 249
  }
  for(let al of addList) {
    await dao.bulkSet(al)
  }
  console.log("done")
})();