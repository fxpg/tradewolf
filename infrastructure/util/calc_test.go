package util

import (
	"fmt"
	"testing"
)

func TestRound(t *testing.T) {

	a := Round(0.123456789, 4)
	if a != 0.1234 {
		t.Fatal(a)
	}
	fmt.Printf("%f", a)
}
