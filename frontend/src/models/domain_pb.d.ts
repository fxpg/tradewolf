// package: entity
// file: domain.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class CurrencyPairInfo extends jspb.Message {
  hasCurrencyPair(): boolean;
  clearCurrencyPair(): void;
  getCurrencyPair(): CurrencyPair | undefined;
  setCurrencyPair(value?: CurrencyPair): void;

  getClose(): number;
  setClose(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CurrencyPairInfo.AsObject;
  static toObject(includeInstance: boolean, msg: CurrencyPairInfo): CurrencyPairInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CurrencyPairInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CurrencyPairInfo;
  static deserializeBinaryFromReader(message: CurrencyPairInfo, reader: jspb.BinaryReader): CurrencyPairInfo;
}

export namespace CurrencyPairInfo {
  export type AsObject = {
    currencyPair?: CurrencyPair.AsObject,
    close: number,
  }
}

export class Ticker extends jspb.Message {
  getOpen(): number;
  setOpen(value: number): void;

  getHigh(): number;
  setHigh(value: number): void;

  getLow(): number;
  setLow(value: number): void;

  getClose(): number;
  setClose(value: number): void;

  getVolume(): number;
  setVolume(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Ticker.AsObject;
  static toObject(includeInstance: boolean, msg: Ticker): Ticker.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Ticker, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Ticker;
  static deserializeBinaryFromReader(message: Ticker, reader: jspb.BinaryReader): Ticker;
}

export namespace Ticker {
  export type AsObject = {
    open: number,
    high: number,
    low: number,
    close: number,
    volume: number,
  }
}

export class Timestamp extends jspb.Message {
  hasTimestamp(): boolean;
  clearTimestamp(): void;
  getTimestamp(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTimestamp(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Timestamp.AsObject;
  static toObject(includeInstance: boolean, msg: Timestamp): Timestamp.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Timestamp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Timestamp;
  static deserializeBinaryFromReader(message: Timestamp, reader: jspb.BinaryReader): Timestamp;
}

export namespace Timestamp {
  export type AsObject = {
    timestamp?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GormModel extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  hasCreatedat(): boolean;
  clearCreatedat(): void;
  getCreatedat(): Timestamp | undefined;
  setCreatedat(value?: Timestamp): void;

  hasUpdatedat(): boolean;
  clearUpdatedat(): void;
  getUpdatedat(): Timestamp | undefined;
  setUpdatedat(value?: Timestamp): void;

  hasDeletedat(): boolean;
  clearDeletedat(): void;
  getDeletedat(): Timestamp | undefined;
  setDeletedat(value?: Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GormModel.AsObject;
  static toObject(includeInstance: boolean, msg: GormModel): GormModel.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GormModel, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GormModel;
  static deserializeBinaryFromReader(message: GormModel, reader: jspb.BinaryReader): GormModel;
}

export namespace GormModel {
  export type AsObject = {
    id: number,
    createdat?: Timestamp.AsObject,
    updatedat?: Timestamp.AsObject,
    deletedat?: Timestamp.AsObject,
  }
}

export class KeySetting extends jspb.Message {
  hasGormModel(): boolean;
  clearGormModel(): void;
  getGormModel(): GormModel | undefined;
  setGormModel(value?: GormModel): void;

  getExchange(): string;
  setExchange(value: string): void;

  getApiKey(): string;
  setApiKey(value: string): void;

  getSecKey(): string;
  setSecKey(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): KeySetting.AsObject;
  static toObject(includeInstance: boolean, msg: KeySetting): KeySetting.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: KeySetting, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): KeySetting;
  static deserializeBinaryFromReader(message: KeySetting, reader: jspb.BinaryReader): KeySetting;
}

export namespace KeySetting {
  export type AsObject = {
    gormModel?: GormModel.AsObject,
    exchange: string,
    apiKey: string,
    secKey: string,
  }
}

export class ArbitrageOrder extends jspb.Message {
  hasGormModel(): boolean;
  clearGormModel(): void;
  getGormModel(): GormModel | undefined;
  setGormModel(value?: GormModel): void;

  getExchange(): string;
  setExchange(value: string): void;

  getTrading(): string;
  setTrading(value: string): void;

  getSettlement(): string;
  setSettlement(value: string): void;

  getOrderId(): string;
  setOrderId(value: string): void;

  getPrice(): number;
  setPrice(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  getOrderType(): string;
  setOrderType(value: string): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): Timestamp | undefined;
  setCreatedTime(value?: Timestamp): void;

  getArbitragerId(): string;
  setArbitragerId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitrageOrder.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitrageOrder): ArbitrageOrder.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitrageOrder, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitrageOrder;
  static deserializeBinaryFromReader(message: ArbitrageOrder, reader: jspb.BinaryReader): ArbitrageOrder;
}

export namespace ArbitrageOrder {
  export type AsObject = {
    gormModel?: GormModel.AsObject,
    exchange: string,
    trading: string,
    settlement: string,
    orderId: string,
    price: number,
    amount: number,
    orderType: string,
    createdTime?: Timestamp.AsObject,
    arbitragerId: string,
  }
}

export class ArbitrageFilledOrder extends jspb.Message {
  hasGormModel(): boolean;
  clearGormModel(): void;
  getGormModel(): GormModel | undefined;
  setGormModel(value?: GormModel): void;

  getExchange(): string;
  setExchange(value: string): void;

  getTrading(): string;
  setTrading(value: string): void;

  getSettlement(): string;
  setSettlement(value: string): void;

  getOrderId(): string;
  setOrderId(value: string): void;

  getPrice(): number;
  setPrice(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  getOrderType(): string;
  setOrderType(value: string): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): Timestamp | undefined;
  setCreatedTime(value?: Timestamp): void;

  getArbitragerId(): string;
  setArbitragerId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitrageFilledOrder.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitrageFilledOrder): ArbitrageFilledOrder.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitrageFilledOrder, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitrageFilledOrder;
  static deserializeBinaryFromReader(message: ArbitrageFilledOrder, reader: jspb.BinaryReader): ArbitrageFilledOrder;
}

export namespace ArbitrageFilledOrder {
  export type AsObject = {
    gormModel?: GormModel.AsObject,
    exchange: string,
    trading: string,
    settlement: string,
    orderId: string,
    price: number,
    amount: number,
    orderType: string,
    createdTime?: Timestamp.AsObject,
    arbitragerId: string,
  }
}

export class ArbitragerLog extends jspb.Message {
  getLogType(): LogTypeMap[keyof LogTypeMap];
  setLogType(value: LogTypeMap[keyof LogTypeMap]): void;

  getArbitragerId(): number;
  setArbitragerId(value: number): void;

  getMessage(): string;
  setMessage(value: string): void;

  hasCreatedTime(): boolean;
  clearCreatedTime(): void;
  getCreatedTime(): Timestamp | undefined;
  setCreatedTime(value?: Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitragerLog.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitragerLog): ArbitragerLog.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitragerLog, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitragerLog;
  static deserializeBinaryFromReader(message: ArbitragerLog, reader: jspb.BinaryReader): ArbitragerLog;
}

export namespace ArbitragerLog {
  export type AsObject = {
    logType: LogTypeMap[keyof LogTypeMap],
    arbitragerId: number,
    message: string,
    createdTime?: Timestamp.AsObject,
  }
}

export class Log extends jspb.Message {
  getLogType(): LogTypeMap[keyof LogTypeMap];
  setLogType(value: LogTypeMap[keyof LogTypeMap]): void;

  getCategory(): string;
  setCategory(value: string): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Log.AsObject;
  static toObject(includeInstance: boolean, msg: Log): Log.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Log, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Log;
  static deserializeBinaryFromReader(message: Log, reader: jspb.BinaryReader): Log;
}

export namespace Log {
  export type AsObject = {
    logType: LogTypeMap[keyof LogTypeMap],
    category: string,
    message: string,
  }
}

export class ArbitragerSetting extends jspb.Message {
  hasArbitragePair(): boolean;
  clearArbitragePair(): void;
  getArbitragePair(): ArbitragePair | undefined;
  setArbitragePair(value?: ArbitragePair): void;

  getArbitragerMode(): ArbitragerModeMap[keyof ArbitragerModeMap];
  setArbitragerMode(value: ArbitragerModeMap[keyof ArbitragerModeMap]): void;

  getArbitragerOperation(): ArbitragerOperationMap[keyof ArbitragerOperationMap];
  setArbitragerOperation(value: ArbitragerOperationMap[keyof ArbitragerOperationMap]): void;

  getArbitragerState(): ArbitragerStateMap[keyof ArbitragerStateMap];
  setArbitragerState(value: ArbitragerStateMap[keyof ArbitragerStateMap]): void;

  getExpectedProfitRate(): number;
  setExpectedProfitRate(value: number): void;

  getLossCutRate(): number;
  setLossCutRate(value: number): void;

  getAssetUseRate(): number;
  setAssetUseRate(value: number): void;

  getArbitrageWaitLimit(): number;
  setArbitrageWaitLimit(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitragerSetting.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitragerSetting): ArbitragerSetting.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitragerSetting, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitragerSetting;
  static deserializeBinaryFromReader(message: ArbitragerSetting, reader: jspb.BinaryReader): ArbitragerSetting;
}

export namespace ArbitragerSetting {
  export type AsObject = {
    arbitragePair?: ArbitragePair.AsObject,
    arbitragerMode: ArbitragerModeMap[keyof ArbitragerModeMap],
    arbitragerOperation: ArbitragerOperationMap[keyof ArbitragerOperationMap],
    arbitragerState: ArbitragerStateMap[keyof ArbitragerStateMap],
    expectedProfitRate: number,
    lossCutRate: number,
    assetUseRate: number,
    arbitrageWaitLimit: number,
  }
}

export class ArbitragePair extends jspb.Message {
  getExchangeA(): string;
  setExchangeA(value: string): void;

  getExchangeB(): string;
  setExchangeB(value: string): void;

  hasCurrencyPair(): boolean;
  clearCurrencyPair(): void;
  getCurrencyPair(): CurrencyPair | undefined;
  setCurrencyPair(value?: CurrencyPair): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitragePair.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitragePair): ArbitragePair.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitragePair, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitragePair;
  static deserializeBinaryFromReader(message: ArbitragePair, reader: jspb.BinaryReader): ArbitragePair;
}

export namespace ArbitragePair {
  export type AsObject = {
    exchangeA: string,
    exchangeB: string,
    currencyPair?: CurrencyPair.AsObject,
  }
}

export class CurrencyPair extends jspb.Message {
  getTrading(): string;
  setTrading(value: string): void;

  getSettlement(): string;
  setSettlement(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CurrencyPair.AsObject;
  static toObject(includeInstance: boolean, msg: CurrencyPair): CurrencyPair.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CurrencyPair, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CurrencyPair;
  static deserializeBinaryFromReader(message: CurrencyPair, reader: jspb.BinaryReader): CurrencyPair;
}

export namespace CurrencyPair {
  export type AsObject = {
    trading: string,
    settlement: string,
  }
}

export class Board extends jspb.Message {
  hasDateTime(): boolean;
  clearDateTime(): void;
  getDateTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setDateTime(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasCurrencyPair(): boolean;
  clearCurrencyPair(): void;
  getCurrencyPair(): CurrencyPair | undefined;
  setCurrencyPair(value?: CurrencyPair): void;

  getExchange(): string;
  setExchange(value: string): void;

  clearBidsList(): void;
  getBidsList(): Array<BoardBar>;
  setBidsList(value: Array<BoardBar>): void;
  addBids(value?: BoardBar, index?: number): BoardBar;

  clearAsksList(): void;
  getAsksList(): Array<BoardBar>;
  setAsksList(value: Array<BoardBar>): void;
  addAsks(value?: BoardBar, index?: number): BoardBar;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Board.AsObject;
  static toObject(includeInstance: boolean, msg: Board): Board.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Board, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Board;
  static deserializeBinaryFromReader(message: Board, reader: jspb.BinaryReader): Board;
}

export namespace Board {
  export type AsObject = {
    dateTime?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    currencyPair?: CurrencyPair.AsObject,
    exchange: string,
    bidsList: Array<BoardBar.AsObject>,
    asksList: Array<BoardBar.AsObject>,
  }
}

export class BoardBar extends jspb.Message {
  getPrice(): number;
  setPrice(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BoardBar.AsObject;
  static toObject(includeInstance: boolean, msg: BoardBar): BoardBar.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BoardBar, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BoardBar;
  static deserializeBinaryFromReader(message: BoardBar, reader: jspb.BinaryReader): BoardBar;
}

export namespace BoardBar {
  export type AsObject = {
    price: number,
    amount: number,
  }
}

export class OrderBookTick extends jspb.Message {
  getExchange(): string;
  setExchange(value: string): void;

  hasCurrencyPair(): boolean;
  clearCurrencyPair(): void;
  getCurrencyPair(): CurrencyPair | undefined;
  setCurrencyPair(value?: CurrencyPair): void;

  getBestAskPrice(): number;
  setBestAskPrice(value: number): void;

  getBestAskAmount(): number;
  setBestAskAmount(value: number): void;

  getBestBidPrice(): number;
  setBestBidPrice(value: number): void;

  getBestBidAmount(): number;
  setBestBidAmount(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderBookTick.AsObject;
  static toObject(includeInstance: boolean, msg: OrderBookTick): OrderBookTick.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderBookTick, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderBookTick;
  static deserializeBinaryFromReader(message: OrderBookTick, reader: jspb.BinaryReader): OrderBookTick;
}

export namespace OrderBookTick {
  export type AsObject = {
    exchange: string,
    currencyPair?: CurrencyPair.AsObject,
    bestAskPrice: number,
    bestAskAmount: number,
    bestBidPrice: number,
    bestBidAmount: number,
  }
}

export class ArbitragePairArbitrage extends jspb.Message {
  hasOrderBookTickA(): boolean;
  clearOrderBookTickA(): void;
  getOrderBookTickA(): OrderBookTick | undefined;
  setOrderBookTickA(value?: OrderBookTick): void;

  hasOrderBookTickB(): boolean;
  clearOrderBookTickB(): void;
  getOrderBookTickB(): OrderBookTick | undefined;
  setOrderBookTickB(value?: OrderBookTick): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ArbitragePairArbitrage.AsObject;
  static toObject(includeInstance: boolean, msg: ArbitragePairArbitrage): ArbitragePairArbitrage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ArbitragePairArbitrage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ArbitragePairArbitrage;
  static deserializeBinaryFromReader(message: ArbitragePairArbitrage, reader: jspb.BinaryReader): ArbitragePairArbitrage;
}

export namespace ArbitragePairArbitrage {
  export type AsObject = {
    orderBookTickA?: OrderBookTick.AsObject,
    orderBookTickB?: OrderBookTick.AsObject,
  }
}

export class NetArbitrageByArbitragePair extends jspb.Message {
  getExchangeA(): string;
  setExchangeA(value: string): void;

  getExchangeB(): string;
  setExchangeB(value: string): void;

  hasCurrencyPair(): boolean;
  clearCurrencyPair(): void;
  getCurrencyPair(): CurrencyPair | undefined;
  setCurrencyPair(value?: CurrencyPair): void;

  getOrderBookInverse(): number;
  setOrderBookInverse(value: number): void;

  getNetArbitrage(): number;
  setNetArbitrage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NetArbitrageByArbitragePair.AsObject;
  static toObject(includeInstance: boolean, msg: NetArbitrageByArbitragePair): NetArbitrageByArbitragePair.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: NetArbitrageByArbitragePair, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NetArbitrageByArbitragePair;
  static deserializeBinaryFromReader(message: NetArbitrageByArbitragePair, reader: jspb.BinaryReader): NetArbitrageByArbitragePair;
}

export namespace NetArbitrageByArbitragePair {
  export type AsObject = {
    exchangeA: string,
    exchangeB: string,
    currencyPair?: CurrencyPair.AsObject,
    orderBookInverse: number,
    netArbitrage: number,
  }
}

export interface LogTypeMap {
  INFO: 0;
  ERROR: 1;
  WARN: 2;
}

export const LogType: LogTypeMap;

export interface ArbitragerActionMap {
  CREATE: 0;
  RUN: 1;
  SETTING: 2;
  STOP: 3;
  DELETE: 4;
}

export const ArbitragerAction: ArbitragerActionMap;

export interface ArbitragerModeMap {
  TEST: 0;
  REAL: 1;
}

export const ArbitragerMode: ArbitragerModeMap;

export interface ArbitragerOperationMap {
  LONG_SIDE: 0;
  SHORT_SIDE: 1;
  BOTH_SIDE: 2;
}

export const ArbitragerOperation: ArbitragerOperationMap;

export interface ArbitragerStateMap {
  CREATING: 0;
  STOPPED: 1;
  RUNNING: 2;
  TERMINATING: 3;
}

export const ArbitragerState: ArbitragerStateMap;

