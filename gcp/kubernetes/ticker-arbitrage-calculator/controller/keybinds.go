package controller

import (
	"github.com/gdamore/tcell"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
)

func (g *gui) nextPanel() {
	idx := (g.Panels.Current + 1) % len(g.Panels.Panels)
	g.Panels.Current = idx
	g.SwitchPanel(g.Panels.Panels[g.Panels.Current])
}

func (g *gui) prePanel() {
	g.Panels.Current--

	if g.Panels.Current < 0 {
		g.Current = len(g.Panels.Panels) - 1
	} else {
		idx := (g.Panels.Current) % len(g.Panels.Panels)
		g.Panels.Current = idx
	}
	g.SwitchPanel(g.Panels.Panels[g.Panels.Current])
}

func (g *gui) GrobalKeybind(event *tcell.EventKey) {
	switch event.Key() {
	case tcell.KeyTab:
		g.nextPanel()
	case tcell.KeyBacktab:
		g.prePanel()
	}
	g.NaviView.UpdateView(g)
}

func (g *gui) ArbitrageWatchManagerKeybinds() {
	g.ArbitrageWatchManager.SetDoneFuncOnWatchingTable(func(key tcell.Key) {
		switch key {
		case tcell.KeyEscape:
			g.App.Stop()
		}
	}).SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Rune() {
		// ここに押されたときのEventを書く
		}
		// 最後にグローバルキーバインド適用する
		g.GrobalKeybind(event)
		return event
	})

	g.ArbitrageWatchManager.SetSelectionChangedFuncOnWatchingTable(func(row, col int) {
		if row < 1 {
			return
		}
		g.ArbitrageInfoView.UpdateView(g)
	})
}

func (g *gui) FilterInputKeybinds() {
	g.FilterInput.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEscape:
			g.App.Stop()
		case tcell.KeyEnter:
			g.nextPanel()
		}
	}).SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		g.GrobalKeybind(event)
		return event
	})
	g.FilterInput.SetChangedFunc(func(text string) {
		g.ArbitrageWatchManager.SetFilterWord(text)
		g.ArbitrageWatchManager.UpdateView()
	})
}

func (g *gui) FilterTypeSelectKeybinds() {
	g.FilterTypeDropDown.SetDoneFunc(func(key tcell.Key) {
		switch key {
		case tcell.KeyEscape:
			g.App.Stop()
		case tcell.KeyEnter:
			g.nextPanel()
		}
	}).SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		// _, s:= g.FilterTypeDropDown.GetCurrentOption()
		// g.ArbitrageWatchManager.SetFilterType(model.NewFilterType(s))
		g.GrobalKeybind(event)
		return event
	})
	g.FilterTypeDropDown.SetSelectedFunc(func(text string, index int) {
		g.ArbitrageWatchManager.SetFilterType(model.NewFilterType(text))
		g.ArbitrageWatchManager.UpdateView()
	})
}

func (g *gui) ArbitrageInfoViewKeybinds() {
	g.ArbitrageInfoView.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		g.GrobalKeybind(event)
		return event
	})
}

func (g *gui) SetKeybinds() {
	g.FilterInputKeybinds()
	g.ArbitrageWatchManagerKeybinds()
	g.ArbitrageInfoViewKeybinds()
	g.FilterTypeSelectKeybinds()
}
