import ccxws from 'ccxws'
import ccxt from 'ccxt'
import cloudscraper from 'cloudscraper'
import Redis from 'ioredis'
import IORedis from 'ioredis';
import { Exchange, GetCcxtClient, GetCcxwsClientConstructor, IsExchange, GetArbitragableMarkets } from './utils'
import * as cluster from "cluster";
import * as os from "os";
import retry from 'async-retry';

let REDIS_HOST = "localhost"
let REDIS_PORT = 6379
if (process.env.REDIS_HOST) {
  REDIS_HOST = process.env.REDIS_HOST
}
if (process.env.REDIS_PORT) {
  REDIS_PORT = Number(process.env.REDIS_PORT)
}
const NUM_CPUS = os.cpus().length;

type BoardBar = [number, number]
type OrderBook = {
  asks : BoardBar[]
  bids : BoardBar[]
}

const scrapeCloudflareHttpHeaderCookie = (url: string) =>
	(new Promise ((resolve, reject) =>
		(cloudscraper.get (url, function (error:any, response:any, _body:any) {
			if (error) {
				reject (error)
			} else {
        resolve (response.request.headers)
      }
		}))
  ))  

class OrderBookFetcher{
  redis: IORedis.Redis
  pubsubClient: IORedis.Redis
  exchange: Exchange
  ccxtClient: ccxt.Exchange
  ccxwsClients: any[]
  markets!: ccxt.Market[];

  constructor(exchange:Exchange, ccxtClient:ccxt.Exchange, ccxwsClientConstructor:any) {
    this.redis = new Redis({port:REDIS_PORT, host: REDIS_HOST,  retryStrategy: function(times:number) {
        var delay = Math.min(times * 50, 2000);
        return delay;
      }
    });
    this.pubsubClient = new Redis({port:REDIS_PORT, host: REDIS_HOST,  retryStrategy: function(times:number) {
        var delay = Math.min(times * 50, 2000);
        return delay;
      }
    });
    this.exchange = exchange
    ccxtClient.enableRateLimit = true
    ccxtClient.timeout = 30000
    ccxtClient.rateLimit = 3000
    this.ccxtClient = ccxtClient
    const ccxwsClients:any[]=[]
    const ccxwsClientsNum = Math.ceil(10 / NUM_CPUS)
    for(let i=0;i<ccxwsClientsNum;i++) {
      ccxwsClients.push(new ccxwsClientConstructor())
    }
    this.ccxwsClients = ccxwsClients
    this.ccxtClient.has['fetchCurrencies'] = false
  }

  rotateCcxwsClient(): any {
    return this.ccxwsClients[Math.floor(Math.random() * this.ccxwsClients.length)]
  }
  
  setMarkets(markets: ccxt.Market[]) {
    this.markets = markets
  }
  
  async fetchMarketData():Promise<ccxt.Dictionary<ccxt.Market>>{
    // this.ccxtClient.headers = await scrapeCloudflareHttpHeaderCookie(this.ccxtClient.urls.www)
    return await this.ccxtClient.loadMarkets()    
  }

  async initialize() {
    await this.redis.keys(this.exchange+":*").then((keys:string[])=>{
      let pipeline = this.redis.pipeline()
      keys.forEach((key:string)=>{
        pipeline.del(key)
      })
      return pipeline.exec()
    })
    console.log('existing data deleted')
  }

  private getKeyPrefix(exchange:string, trading:string, settlement:string) :string {
    return exchange+":"+trading+":"+settlement+":"
  }

  public async resetOrderBook(exchange:string, trading:string, settlement:string) {
    await this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"asks")
    await this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"bids")
  }

  public async SetOrderBookSnapshot(exchange:string, trading:string, settlement:string, orderBook: OrderBook) {
    this.redis.multi({ pipeline: false })
    this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"asks")
    for(let ask of orderBook.asks) {
      this.redis.zadd(this.getKeyPrefix(exchange,trading,settlement)+"asks", `${ask[0]}`, `${ask[1]}`)
    }

    this.redis.del(this.getKeyPrefix(exchange,trading,settlement)+"bids")
    for(let bid of orderBook.bids) {
      this.redis.zadd(this.getKeyPrefix(exchange,trading,settlement)+"bids", `${bid[0]}`, `${bid[1]}`)
    }
    this.redis.exec((_err: Error|null, _res: [Error|null, string][])=>{
    })
    this.pubsubClient.publish(`${exchange}:${trading}:${settlement}`, "test")
  }

  public async SetOrderBookUpdate(exchange:string, trading:string, settlement:string, updates: OrderBook) {
    this.redis.multi({ pipeline: false })
    for (let ask of updates.asks) {
      try {        
        if(Number(ask[1]) === 0) {
          this.redis.zremrangebyscore(this.getKeyPrefix(exchange,trading,settlement)+"asks",`${ask[0]}`,`${ask[0]}`)
        } else {
          this.redis.zremrangebyscore(this.getKeyPrefix(exchange,trading,settlement)+"asks",`${ask[0]}`,`${ask[0]}`)
          this.redis.zadd(this.getKeyPrefix(exchange,trading,settlement)+"asks",`${ask[0]}`,`${ask[1]}`)              
        }
      } catch(e){
        console.log(e)
      }
    }
    for (let bid of updates.bids) {
      try{        
        if(Number(bid[1]) === 0) {
          this.redis.zremrangebyscore(this.getKeyPrefix(exchange,trading,settlement)+"bids",`${bid[0]}`,`${bid[0]}`)
        } else {
          this.redis.zremrangebyscore(this.getKeyPrefix(exchange,trading,settlement)+"bids",`${bid[0]}`,`${bid[0]}`)
          this.redis.zadd(this.getKeyPrefix(exchange,trading,settlement)+"bids",`${bid[0]}`,`${bid[1]}`)
        }
      } catch(e) {
        console.log(e)
      }
    }
    this.redis.exec((_err: Error|null, _res: [Error|null, string][])=>{
    })
    this.pubsubClient.publish(`${exchange}:${trading}:${settlement}`, "test")
  }  
}

class L2SnapshotFetcher extends OrderBookFetcher {
  constructor(exchange:Exchange) {
    const ccxwsConstructor = GetCcxwsClientConstructor(exchange)
    const ccxtClient = GetCcxtClient(exchange)
    if(ccxtClient===undefined) {
      throw Error(`no exchange ${exchange}`)
    }
    super(exchange, ccxtClient, ccxwsConstructor)
  }

  addHandler() {
    const cli = this.rotateCcxwsClient()
    cli.on("l2snapshot", async (snapshot:any, market:any)=>{
      const asks = snapshot.asks.map((ask: any) => {
        return [Number(ask.price), Number(ask.size)]
      })
      const bids = snapshot.bids.map((bid: any) => {
        return [Number(bid.price), Number(bid.size)]
      })
      await this.SetOrderBookSnapshot(this.exchange, market.base, market.quote, {asks:asks, bids:bids})
    });
    cli.on("error", (err:any)=>{
      console.log(err)
    })
  }
  async fetch() {
    for(let market of this.markets) {
      const cli = this.rotateCcxwsClient()
      cli.subscribeLevel2Snapshots({id:market.id, base:market.base,quote:market.quote});
    }
    console.log("fetch triggered")
  }
}
class L2UpdateFetcher extends OrderBookFetcher {
  constructor(exchange:Exchange) {
    const ccxwsConstructor = GetCcxwsClientConstructor(exchange)
    const ccxtClient = GetCcxtClient(exchange)
    if(ccxtClient===undefined) {
      throw Error(`no exchange ${exchange}`)
    }
    super(exchange, ccxtClient, ccxwsConstructor)
  }

  addHandler() {
    const cli = this.rotateCcxwsClient()    
    cli.on("l2update", async (updates:any, market:any)=>{
      const asks = updates.asks.map((ask: any) => {
        return [Number(ask.price), Number(ask.size)]
      })
      const bids = updates.bids.map((bid: any) => {
        return [Number(bid.price), Number(bid.size)]
      })
      this.SetOrderBookUpdate(this.exchange, market.base, market.quote, {asks:asks, bids:bids})
    })
    console.log("handler added")
  }

  async fetch() {
    for(let market of this.markets) {
      let orderBook: ccxt.OrderBook | undefined
      await retry( async (bail) =>{
        orderBook = await this.tryToFetchOrderBook(market.symbol)
        if(orderBook !== undefined) {
          return          
        }        
        // bail(new Error(`failed to fetch ${market.symbol} on ${this.exchange}`))
      },{
        retries:100,
        minTimeout: 5000,
        randomize: true
      })
      if(orderBook === undefined) {
        throw new Error(`failed to fetch ${market.symbol} on ${this.exchange}`)
      }
      this.SetOrderBookSnapshot(this.exchange, market.base,market.quote, {asks:orderBook.asks, bids:orderBook.bids})
      const cli = this.rotateCcxwsClient()    
      cli.subscribeLevel2Updates({id:market.id, base:market.base, quote: market.quote});
    }
    console.log("fetch triggered")
  }

  async tryToFetchOrderBook(symbol:string):Promise<ccxt.OrderBook | undefined> {
    try {
      const orderBook = await this.ccxtClient.fetchOrderBook(symbol)
      return orderBook
    } catch(e) {
      if(e instanceof ccxt.NetworkError) {
        return undefined
      } else {
        throw e
      }
    }    
  }
}

const NewL2Fetcher = (EXCHANGE_TARGET:Exchange): L2SnapshotFetcher | L2UpdateFetcher => {
  const snapshotExchanges = [Exchange.Binance, Exchange.Bibox, Exchange.Bitstamp, Exchange.Huobi, Exchange.Okex, Exchange.Upbit, Exchange.Bitstamp, Exchange.Zb]
  // const updateExchanges = [Exchange.Kucoin, Exchange.Hitbtc, Exchange.Coinbase, Exchange.Coinex, Exchange.Poloniex, Exchange.Liquid]
  if(snapshotExchanges.includes(EXCHANGE_TARGET)) {
    return new L2SnapshotFetcher(EXCHANGE_TARGET)
  }
  return new L2UpdateFetcher(EXCHANGE_TARGET)
}

let EXCHANGE_TARGET :Exchange
if (process.env.EXCHANGE_TARGET === undefined) {
  console.log("not set env $EXCHANGE_TARGET")
  process.exit(1)
}
if (!IsExchange(process.env.EXCHANGE_TARGET)) {
  console.log("invalid $EXCHANGE_TARGET")
  console.log(process.env.EXCHANGE_TARGET)
  process.exit(1)
}
EXCHANGE_TARGET = process.env.EXCHANGE_TARGET

const masterFunc = (async()=>{
  console.log("master func")
  const obFetcher = NewL2Fetcher(EXCHANGE_TARGET)
  let markets: ccxt.Market[]
  try {
    markets = await GetArbitragableMarkets(EXCHANGE_TARGET)
  } catch(e) {
    console.log(e)
    throw(e)
  }
  console.log(`target markets ${markets.length}`)
  
  const slicedTarget:ccxt.Market[][] = []
  const bundle = Math.ceil(markets.length / NUM_CPUS)
  for (let i = 0; i < Object.keys(markets).length; i += bundle) {
      slicedTarget.push(markets.slice(i, i + bundle))
  }
  for(let i =0; i<slicedTarget.length;i++) {    
    let worker = cluster.fork()
    worker.on('online', () => {
        worker.send(slicedTarget[i])
    })
    worker.on('exit', (worker:cluster.Worker, code:number, _signal:any) => {
      console.log('| Main Thread | worker stopped');
      cluster.fork();
    });
  }
});

const workerFunc = (async() => {
  console.log("worker func")
  process.on("message", async (targetMarkets:ccxt.Market[])=>{
    const obFetcher = NewL2Fetcher(EXCHANGE_TARGET)
    await obFetcher.initialize()
    // const marketsDict = await obFetcher.fetchMarketData()
    // const markets = Object.values(marketsDict)    
    obFetcher.setMarkets(targetMarkets)
    obFetcher.addHandler()
    try {
      await obFetcher.fetch()
    } catch (e) {
      throw e
    }
  })
})

if(cluster.isMaster){
  masterFunc().then(()=>{
  }).catch(e=>{
    process.exit(1)
  })
} else {
  workerFunc()
}
