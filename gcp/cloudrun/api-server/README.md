# Read Here and implement swagger.yaml!
- https://qiita.com/o_tyazuke/items/43bd362e8e427aa0e340
- validate: `make validate`
- generate: `make generate`
- run: `make run`
- make sure `GOOGLE_APPLICATION_CREDENTIALS` is set

# redis(local)との連携方法

- redisは下記ディレクトリの`docker-compose up -d`で起動する。
  - gcp/kubernetes/obfetch-server
- redis(local)はホスト名`redis`でアクセスできる。（dockerの名前解決による）
- ポート番号はデフォルト

# Firebase Local Emulatorとの連携方法

- Emulator周りは下記リポジトリの`make serve-local-with-emulator`で起動する。
  - https://gitlab.com/fxpg/tradewolf-web
- firestoreはホスト名`localhost`、ポート番号`8080`で起動する。
- Emulator起動後に`docker-compose up -d`でemulatorに接続するコンテナを起動できる。