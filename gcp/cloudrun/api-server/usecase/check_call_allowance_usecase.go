package usecase

import (
	"context"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

func NewCheckCallAllowanceUsecase(
	userRepo interfaces.UserRepository,
	callCountRepo interfaces.CallCountRepository,
	logger utils.Logger,
) *CheckCallAllowanceUsecase {
	return &CheckCallAllowanceUsecase{
		userRepo:      userRepo,
		callCountRepo: callCountRepo,
		logger:        logger,
	}
}

type CheckCallAllowanceUsecase struct {
	userRepo      interfaces.UserRepository
	callCountRepo interfaces.CallCountRepository
	logger        utils.Logger
}

func (u *CheckCallAllowanceUsecase) Exec(ctx context.Context, apiKey string, path string) error {
	uid, err := u.userRepo.GetUidByApiKey(ctx, apiKey)
	if err != nil {
		return err
	}
	allowanceByDay, err := u.userRepo.GetAllowanceCallByDay(ctx, uid)
	if err != nil {
		return err
	}
	callCountByDay, err := u.callCountRepo.GetByDay(ctx, uid)
	if allowanceByDay <= callCountByDay {
		return qerror.New(model.ErrLogicCallAllowanceExceeded)
	}
	allowanceByMonth, err := u.userRepo.GetAllowanceCallByMonth(ctx, uid)
	if err != nil {
		return err
	}
	callCountByMonth, err := u.callCountRepo.GetByMonth(ctx, uid)
	if err != nil {
		return err
	}
	if allowanceByMonth <= callCountByMonth {
		return qerror.New(model.ErrLogicCallAllowanceExceeded)
	}
	// ホントは下記非同期処理にした隠語ネェ
	err = u.callCountRepo.Increment(ctx, path, uid)
	if err != nil {
		return err
	}
	return nil
}
