package model

import "github.com/tattsun/qerror"

const (
	ErrSystemInvalidArgs qerror.ErrorID = 1
	ErrSystemFirestoreRequestFailed = 2
	ErrSystemFirestoreConnectionError = 3
	ErrSystemRedisCommandFailed = 4
	ErrSystemRedisConnectionError = 5

	ErrSystemUnexpected = 9000

	// Logic Error
	ErrLogicCallAllowanceExceeded = 10001
	ErrLogicSignatureInvalid = 10002
	ErrLogicTimestampInvalid = 10003
	ErrLogicRequestHeaderMissing = 10004
	ErrLogicParameterInvalid = 10005
)

const (
	SystemErrorMaxBound = 9999
)

var errorMsgs = map[qerror.ErrorID]string{
	// System Error: 1-9999
	ErrSystemInvalidArgs: "invalid args: %s",

	ErrSystemFirestoreRequestFailed: "failed to request firestore",
	ErrSystemFirestoreConnectionError: "failed to connect firestore",

	ErrSystemRedisCommandFailed: "failed to execute command to redis",
	ErrSystemRedisConnectionError: "failed to connect redis",

	ErrSystemUnexpected: "unexpected error: %v",

	// Logic Error: 10001-
	ErrLogicCallAllowanceExceeded: "call allowance is exceeded",
	ErrLogicSignatureInvalid: "X-API-SIGNATURE is invalid. Please check your signature.",
	ErrLogicTimestampInvalid: "X-API-TIMESTAMP is invalid. Please check your timestamp.",
	ErrLogicRequestHeaderMissing: "Any of X-API-KEY, X-API-SIGNATURE, X-API-TIMESTAMP is missing in your request header",
	ErrLogicParameterInvalid: "You tried to access the resource with invalid parameters.",
}

func init() {
	qerror.Init(errorMsgs)
}

func IsSystemError(err error) bool {
	if qerr, ok := err.(*qerror.Error); ok {
		return qerr.ErrorID <= SystemErrorMaxBound
	} else {
		return true
	}
}

func ErrorID(err error) int64 {
	if qerr, ok := err.(*qerror.Error); ok {
		return qerr.ErrorID
	} else {
		return -1
	}
}

func ErrorMessage(err error) string {
	if qerr, ok := err.(*qerror.Error); ok {
		return qerr.Message()
	} else {
		return "unexpected error"
	}
}
