package repository

import (
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
)

import "cloud.google.com/go/firestore"

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

func NewKeyPairFirestoreRepository(cli *firestore.Client) (interfaces.KeyPairRepository, error) {
	return &KeyPairFirestoreRepository{
		cli: cli,
	}, nil
}

type KeyPairFirestoreRepository struct {
	cli *firestore.Client
}

func (r *KeyPairFirestoreRepository) GetSecretKeyByApiKey(ctx context.Context, apiKey string) (string, error) {
	res,err := r.cli.Collection("keys").Where("apikey", "==", apiKey).Documents(ctx).GetAll()
	if err != nil {
		return "", qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	if len(res) == 0 {
		return "", qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	return res[0].Data()["secretKey"].(string), nil
}
