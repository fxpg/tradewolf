#!/bin/sh
cd "$(dirname "$0")"

# Firestore Emulator Initilizer exec
(yarn) > /dev/null 2>&1 && (yarn install) > /dev/null 2>&1
(yarn build)
(yarn start)

# Then write test case below

# sh hogehoge.sh
cd ../../
env GCP_PROJECT_ID=tradewolf FIRESTORE_EMULATOR_HOST=localhost:8080 REDIS_HOST=$REDIS_HOST REDIS_PORT=6379 RUN_MODE=debug go test ./... -run WithServer
env GCP_PROJECT_ID=tradewolf FIRESTORE_EMULATOR_HOST=localhost:8080 REDIS_HOST=$REDIS_HOST REDIS_PORT=6379 RUN_MODE=debug go test ./... -run WithMock