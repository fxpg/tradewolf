package interfaces

import "context"

type KeyPairRepository interface {
	GetSecretKeyByApiKey(ctx context.Context, apiKey string) (string, error)
}
