package model

type OrderBook struct {
	CurrencyPair
	Asks []BoardBar
	Bids []BoardBar
}
