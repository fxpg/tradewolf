package repository

import (
	"context"
	"github.com/go-redis/redis"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"time"
)

func NewNonceRedisRepository(cli *redis.Client) interfaces.NonceRepository {
	return &NonceRedisRepository{cli: cli}
}

type NonceRedisRepository struct {
	cli *redis.Client
}

func (r *NonceRedisRepository) GetNonceByUid(ctx context.Context, uid string) (time.Time, error) {
	nonce := r.cli.WithContext(ctx).Get("nonce:"+uid)
	if nonce.Err() == redis.Nil {
		return time.Now().UTC().Add(time.Hour * -1), nil
	}
	if nonce.Err() != nil {
		return time.Time{}, qerror.WrapWith(nonce.Err(), model.ErrSystemRedisCommandFailed)
	}
	unixTimeNanosecond,err := nonce.Int64()
	if err != nil {
		return time.Time{}, qerror.WrapWith(err, model.ErrSystemRedisCommandFailed)
	}
	return time.Unix(0,unixTimeNanosecond), nil
}

func (r *NonceRedisRepository) UpdateNonceByUid(ctx context.Context, uid string, nonce time.Time) error {
	cmd := r.cli.WithContext(ctx).Set("nonce:"+uid, nonce.UnixNano(), 0)
	if cmd.Err() != nil {
		return qerror.WrapWith(cmd.Err(), model.ErrSystemRedisCommandFailed)
	}
	return nil
}
