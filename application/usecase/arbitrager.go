package usecase

import (
	"context"
	"fmt"
	"math"
	"sync"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/kokardy/listing"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/domain/service"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
)

func getTradeAmount(coin string) float64 {
	switch coin {
	case "BTC":
		return 0.002
	case "ETH":
		return 0.05
	}
	return 0
}

type ArbitrageUsecase interface {
	GetPairs() ([]entity.ArbitragePair, error)
	GetBoards(int) ([]*entity.Board, error)
	GetArbitrageByArbitragePair() ([]entity.ArbitragePairArbitrage, error)
	GetNetArbitragesByArbitragePair() ([]entity.NetArbitrageByArbitragePair, error)

	GetArbitragerIDs() ([]int, error)
	GetArbitragePairByArbitrager(int) (*entity.ArbitragePair, error)
	GetLoadedArbitragerSetting(id int) (*entity.ArbitragerSetting, error)

	LoadArbitrager(id int, logCallback func(log entity.ArbitragerLog)) error
	CreateArbitrager(*entity.ArbitragerSetting, func(log entity.ArbitragerLog)) (int, error)
	StartArbitrager(int) error
	SetArbitragerSetting(int, *entity.ArbitragerSetting) error
	StopArbitrager(int) error
	DeleteArbitrager(int) error

	GetTransferFee(exchange string, coin string) (float64, error)
	GetTransferFees() (map[string]map[string]float64, error)
	GetBalances() (map[string]map[string]float64, error)

	GetOrdersByArbitragerID(int) ([]entity.ArbitrageOrder, error)
	GetOrdersByCurrencyPair(string, string) ([]entity.ArbitrageOrder, error)
	GetOrdersByExchange(string) ([]entity.ArbitrageOrder, error)
	GetOrdersByArbitragePair(string, string, string) ([]entity.ArbitrageOrder, error)

	GetFilledOrdersByArbitragerID(int) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByCurrencyPair(string, string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByExchange(string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByArbitragePair(string, string, string) ([]entity.ArbitrageFilledOrder, error)

	GetArbitragerLogsByArbitragerID(int) ([]entity.ArbitragerLog, error)
}

func (ao *arbitrageUsecase) GetArbitragerLogsByArbitragerID(id int) ([]entity.ArbitragerLog, error) {
	return ao.ArbitragerLogRepository.FindByArbitragerId(int64(id))
}

func (ao *arbitrageUsecase) GetLoadedArbitragerSetting(id int) (*entity.ArbitragerSetting, error) {
	arbitrager, err := ao.findLoadedArbitrager(id)
	if err != nil {
		return nil, err
	}
	return arbitrager.GetSetting()
}

func (ao *arbitrageUsecase) GetArbitragePairByArbitrager(id int) (*entity.ArbitragePair, error) {
	arbitrager, err := ao.findLoadedArbitrager(id)
	if err != nil {
		return nil, err
	}
	setting, err := arbitrager.GetSetting()
	if err != nil {
		return nil, err
	}
	return setting.ArbitragePair, nil
}

func (ao *arbitrageUsecase) GetNetArbitragesByArbitragePair() ([]entity.NetArbitrageByArbitragePair, error) {
	orderBookTicksMap := make(map[string]map[string]map[string]models.OrderBookTick)
	for _, exchange := range ao.config.Exchanges {
		orderBookTickMap, err := ao.PublicResourceRepository.OrderBookTickMap(exchange)
		if err != nil {
			logger.Get().Error(err)
			continue
		}
		orderBookTicksMap[exchange] = orderBookTickMap
	}
	arbitragePairs, err := ao.GetPairs()
	if err != nil {
		return nil, err
	}
	netArbitragesByArbitragePair := make([]entity.NetArbitrageByArbitragePair, 0)
	for _, v := range arbitragePairs {
		orderBookTickModelA, ok := orderBookTicksMap[v.ExchangeA][v.CurrencyPair.Trading][v.CurrencyPair.Settlement]
		if ok != true {
			//logger.Get().Error(fmt.Sprintf("exchange %s trading %s settlement %s",v.ExchangeA,v.CurrencyPair.Trading, v.CurrencyPair.Settlement))
			continue
		}
		orderBookTickModelB, ok := orderBookTicksMap[v.ExchangeB][v.CurrencyPair.Trading][v.CurrencyPair.Settlement]
		if ok != true {
			//logger.Get().Error(fmt.Sprintf("exchange %s trading %s settlement %s",v.ExchangeA,v.CurrencyPair.Trading, v.CurrencyPair.Settlement))
			continue
		}
		orderBookInverse := math.Max(orderBookTickModelA.BestBidPrice/orderBookTickModelB.BestAskPrice, orderBookTickModelB.BestBidPrice/orderBookTickModelA.BestAskPrice) - 1
		if orderBookTickModelA.BestBidPrice/orderBookTickModelB.BestAskPrice >= 1 {
			logger.Get().Infof("%s-%s %s/%s:%f %f", v.ExchangeA, v.ExchangeB, v.CurrencyPair.Trading, v.CurrencyPair.Settlement, orderBookTickModelA.BestBidPrice, orderBookTickModelB.BestAskPrice)
		}
		if orderBookTickModelB.BestBidPrice/orderBookTickModelA.BestAskPrice >= 1 {
			logger.Get().Infof("%s-%s %s/%s:%f %f", v.ExchangeA, v.ExchangeB, v.CurrencyPair.Trading, v.CurrencyPair.Settlement, orderBookTickModelB.BestBidPrice, orderBookTickModelA.BestAskPrice)
		}
		var netArbitrage float64
		if orderBookTickModelA.BestBidPrice/orderBookTickModelB.BestAskPrice >= orderBookTickModelB.BestBidPrice/orderBookTickModelA.BestAskPrice {
			// buyside = B
			tradeAmount := math.Max(orderBookTickModelB.BestAskAmount, orderBookTickModelA.BestBidAmount)
			bNetValue := orderBookTickModelB.BestAskPrice * tradeAmount
			bTransferFee, err := ao.GetTransferFee(v.ExchangeB, v.CurrencyPair.Trading)
			if err != nil {
				continue
			}
			aNetValue := orderBookTickModelA.BestBidPrice * (tradeAmount - bTransferFee)
			if bNetValue == 0 {
				continue
			}
			netArbitrage = aNetValue/bNetValue - 1
		} else {
			// buyside = A
			tradeAmount := math.Max(orderBookTickModelA.BestAskAmount, orderBookTickModelB.BestBidAmount)
			aNetValue := orderBookTickModelA.BestAskPrice * tradeAmount
			aTransferFee, err := ao.GetTransferFee(v.ExchangeA, v.CurrencyPair.Trading)
			if err != nil {
				continue
			}
			bNetValue := orderBookTickModelB.BestBidPrice * (tradeAmount - aTransferFee)
			if aNetValue == 0 {
				continue
			}
			netArbitrage = bNetValue/aNetValue - 1
		}
		na := entity.NetArbitrageByArbitragePair{
			ExchangeA:        v.ExchangeA,
			ExchangeB:        v.ExchangeB,
			CurrencyPair:     v.CurrencyPair,
			OrderBookInverse: orderBookInverse,
			NetArbitrage:     netArbitrage,
		}
		netArbitragesByArbitragePair = append(netArbitragesByArbitragePair, na)
	}
	return netArbitragesByArbitragePair, nil
}

func (ao *arbitrageUsecase) GetArbitrageByArbitragePair() ([]entity.ArbitragePairArbitrage, error) {
	orderBookTicksMap := make(map[string]map[string]map[string]models.OrderBookTick)
	for _, exchange := range ao.config.Exchanges {
		orderBookTickMap, err := ao.PublicResourceRepository.OrderBookTickMap(exchange)
		if err != nil {
			logger.Get().Error(err)
			continue
		}
		orderBookTicksMap[exchange] = orderBookTickMap
	}
	arbitragePairs, err := ao.GetPairs()
	if err != nil {
		return nil, err
	}
	arbitragePairArbitrage := make([]entity.ArbitragePairArbitrage, 0)
	for _, v := range arbitragePairs {
		orderBookTickModelA, ok := orderBookTicksMap[v.ExchangeA][v.CurrencyPair.Trading][v.CurrencyPair.Settlement]
		if ok != true {
			//logger.Get().Error(fmt.Sprintf("exchange %s trading %s settlement %s",v.ExchangeA,v.CurrencyPair.Trading, v.CurrencyPair.Settlement))
			continue
		}
		orderBookTickA := &entity.OrderBookTick{
			Exchange:      v.ExchangeA,
			CurrencyPair:  v.CurrencyPair,
			BestAskPrice:  orderBookTickModelA.BestAskPrice,
			BestAskAmount: orderBookTickModelA.BestAskAmount,
			BestBidPrice:  orderBookTickModelA.BestBidPrice,
			BestBidAmount: orderBookTickModelA.BestBidAmount,
		}
		orderBookTickModelB, ok := orderBookTicksMap[v.ExchangeB][v.CurrencyPair.Trading][v.CurrencyPair.Settlement]
		if ok != true {
			//logger.Get().Error(fmt.Sprintf("exchange %s trading %s settlement %s",v.ExchangeA,v.CurrencyPair.Trading, v.CurrencyPair.Settlement))
			continue
		}
		orderBookTickB := &entity.OrderBookTick{
			Exchange:      v.ExchangeB,
			CurrencyPair:  v.CurrencyPair,
			BestAskPrice:  orderBookTickModelB.BestAskPrice,
			BestAskAmount: orderBookTickModelB.BestAskAmount,
			BestBidPrice:  orderBookTickModelB.BestBidPrice,
			BestBidAmount: orderBookTickModelB.BestBidAmount,
		}
		apa := entity.ArbitragePairArbitrage{
			OrderBookTickA: orderBookTickA,
			OrderBookTickB: orderBookTickB,
		}
		arbitragePairArbitrage = append(arbitragePairArbitrage, apa)
	}
	return arbitragePairArbitrage, nil
}

func max(i int, j int) int {
	if i > j {
		return i
	}
	return j
}

func (ao *arbitrageUsecase) maxID() (int, error) {
	return ao.ArbitragerSettingRepository.LastId()
}

func (ao *arbitrageUsecase) LoadArbitrager(id int, logCallback func(log entity.ArbitragerLog)) error {
	arbitragerSetting, err := ao.ArbitragerSettingRepository.FindById(id)
	if err != nil {
		return err
	}
	arbitragerSetting.ArbitragerState = entity.ArbitragerState_STOPPED
	logger.Get().Info(fmt.Sprintf("loaded arbitrager with:%#v", arbitragerSetting))
	ao.arbitragers.Set(id, service.NewArbitrager(id, ao.PublicResourceRepository, ao.PrivateResourceRepository, ao.SettingRepository, ao.ArbitrageOrderRepository, ao.ArbitragerLogRepository, arbitragerSetting, logCallback))
	return nil
}

func (ao *arbitrageUsecase) CreateArbitrager(setting *entity.ArbitragerSetting, logCallback func(log entity.ArbitragerLog)) (int, error) {
	lastId, err := ao.ArbitragerSettingRepository.LastId()
	if err != nil {
		return 0, err
	}
	setting.ArbitragerState = entity.ArbitragerState_STOPPED
	ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
		LogType:      entity.LogType_INFO,
		ArbitragerId: int64(lastId + 1),
		Message:      fmt.Sprintf("create arbitrager with:%#v", setting),
		CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
	})
	logger.Get().Info(fmt.Sprintf("create arbitrager with:%#v", setting))
	err = ao.ArbitragerSettingRepository.Insert(lastId+1, setting)
	if err != nil {
		return 0, err
	}
	ao.arbitragers.Set(lastId+1, service.NewArbitrager(lastId+1, ao.PublicResourceRepository, ao.PrivateResourceRepository, ao.SettingRepository, ao.ArbitrageOrderRepository, ao.ArbitragerLogRepository, setting, logCallback))
	return lastId + 1, nil
}

func (ao *arbitrageUsecase) SetArbitragerSetting(arbitragerID int, setting *entity.ArbitragerSetting) error {
	_, err := ao.findLoadedArbitrager(arbitragerID)
	if err != nil {
		return err
	}
	return nil
}

func (ao *arbitrageUsecase) findLoadedArbitrager(arbitragerID int) (service.Arbitrager, error) {
	arbitrager, ok := ao.arbitragers.Load(arbitragerID)
	if !ok {
		return nil, errors.New("could not find arbitrager")
	}
	return arbitrager, nil
}

func (ao *arbitrageUsecase) StartArbitrager(arbitragerID int) error {
	ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
		LogType:      entity.LogType_INFO,
		ArbitragerId: int64(arbitragerID),
		Message:      fmt.Sprintf("started arbitrager id:%#v", arbitragerID),
		CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
	})
	logger.Get().Info(fmt.Sprintf("started arbitrager id:%#v", arbitragerID))
	arbitrager, err := ao.findLoadedArbitrager(arbitragerID)
	if err != nil {
		ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
			LogType:      entity.LogType_ERROR,
			ArbitragerId: int64(arbitragerID),
			Message:      err.Error(),
			CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
		})
		logger.Get().Error(err)
		return err
	}
	status, err := arbitrager.GetStatus()
	if err != nil {
		ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
			LogType:      entity.LogType_ERROR,
			ArbitragerId: int64(arbitragerID),
			Message:      err.Error(),
			CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
		})
		logger.Get().Error(err)
		return err
	}
	if status != entity.ArbitragerState_STOPPED {
		logger.Get().Info("change status to run")
	}
	arbitrager.SetStatus(entity.ArbitragerState_RUNNING)
	ctx, cancel := context.WithCancel(context.Background())
	ao.arbitragerCancellers.Set(arbitragerID, cancel)
ArbitrageLoop:
	for range time.Tick(3 * time.Second) {
		select {
		case <-ctx.Done():
			break ArbitrageLoop
		default:
			arbitrage, err := arbitrager.FindArbitrage()
			if err != nil {
				return err
			}
			if arbitrage == nil {
				continue
			}
			ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
				LogType:      entity.LogType_INFO,
				ArbitragerId: int64(arbitragerID),
				Message:      fmt.Sprintf("found arbitrage:%#v", arbitrage),
				CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
			})
			logger.Get().Info(fmt.Sprintf("found arbitrage:%#v", arbitrage))
			err = arbitrager.ExecuteArbitrage(ctx, arbitrage)
			if err != nil {
				ao.ArbitragerLogRepository.Insert(&entity.ArbitragerLog{
					LogType:      entity.LogType_ERROR,
					ArbitragerId: int64(arbitragerID),
					Message:      err.Error(),
					CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
				})
				return err
			}
		}
	}
	return nil
}

func (ao *arbitrageUsecase) GetArbitragerIDs() ([]int, error) {
	return ao.ArbitragerSettingRepository.FindIds()
}

func (ao *arbitrageUsecase) GetBoards(id int) ([]*entity.Board, error) {
	arbitrager, err := ao.findLoadedArbitrager(id)
	if err != nil {
		return nil, err
	}
	return arbitrager.GetBoards()
}

func (ao *arbitrageUsecase) StopArbitrager(id int) error {
	arbitrager, err := ao.findLoadedArbitrager(id)
	if err != nil {
		return err
	}
	cancelFunc, ok := ao.arbitragerCancellers.Load(id)
	if ok != true {
		return errors.New(fmt.Sprintf("failed to stop arbitrager %#v", ao.arbitragerCancellers.m))
	}
	cancelFunc()
	err = arbitrager.SetStatus(entity.ArbitragerState_STOPPED)
	if err != nil {
		return errors.Wrap(err, "failed to stop arbitrager")
	}
	return nil
}

func (ao *arbitrageUsecase) DeleteArbitrager(id int) error {
	err := ao.ArbitragerSettingRepository.Delete(id)
	if err != nil {
		return err
	}
	ao.arbitragers.Delete(id)
	return nil
}

func NewArbitragerMap() *arbitragerMap {
	return &arbitragerMap{}
}

type arbitragerMap struct {
	m sync.Map
}

func (acm *arbitragerMap) Delete(arbitragerId int) {
	acm.m.Delete(arbitragerId)
}
func (acm *arbitragerMap) Load(arbitragerId int) (arbitrager service.Arbitrager, ok bool) {
	val, ok := acm.m.Load(arbitragerId)
	if !ok {
		return nil, false
	}
	return val.(service.Arbitrager), true
}

func (acm *arbitragerMap) Set(arbitragerId int, arbitrager service.Arbitrager) {
	acm.m.Store(arbitragerId, arbitrager)
}

func NewArbitragerCancellersMap() *arbitragerCancellersMap {
	return &arbitragerCancellersMap{}
}

type arbitragerCancellersMap struct {
	m sync.Map
}

func (acm *arbitragerCancellersMap) Load(arbitragerId int) (cancellerFunc context.CancelFunc, ok bool) {
	val, ok := acm.m.Load(arbitragerId)
	if !ok {
		return nil, false
	}
	return val.(context.CancelFunc), true
}

func (acm *arbitragerCancellersMap) Set(arbitragerId int, cancellerFunc context.CancelFunc) {
	acm.m.Store(arbitragerId, cancellerFunc)
}

type arbitrageUsecase struct {
	PublicResourceRepository    service.PublicResourceRepository
	PrivateResourceRepository   service.PrivateResourceRepository
	SettingRepository           service.SettingRepository
	ArbitrageOrderRepository    service.ArbitrageOrderRepository
	ArbitragerLogRepository     service.ArbitragerLogRepository
	ArbitragerSettingRepository service.ArbitragerSettingRepository
	arbitragePairs              []entity.ArbitragePair
	arbitragers                 *arbitragerMap
	transferFeeMap              map[string]map[string]float64
	tm                          *sync.Mutex
	config                      *config.Config
	arbitragerCancellers        *arbitragerCancellersMap
}

func (ao *arbitrageUsecase) GetBalances() (map[string]map[string]float64, error) {
	balanceMap := make(map[string]map[string]float64)
	for _, exchange := range ao.config.Exchanges {
		exchangeBalanceMap, err := ao.PrivateResourceRepository.Balances(exchange)
		if err != nil {
			logger.Get().Error(errors.Wrapf(err, "failed to get GetBalances: %s", exchange))
			continue
		}
		balanceMap[exchange] = exchangeBalanceMap
	}
	return balanceMap, nil
}

func (ao *arbitrageUsecase) GetTransferFees() (map[string]map[string]float64, error) {
	ao.tm.Lock()
	defer ao.tm.Unlock()
	if ao.transferFeeMap != nil {
		return ao.transferFeeMap, nil
	}
	transferFeeMap := make(map[string]map[string]float64)
	for _, exchange := range ao.config.Exchanges {
		exchangeTransferFeeMap, err := ao.PrivateResourceRepository.TransferFee(exchange)
		if err != nil {
			logger.Get().Error(errors.Wrapf(err, "failed to get transferFeeMap: %s", exchange))
			continue
		}
		transferFeeMap[exchange] = exchangeTransferFeeMap
	}
	ao.transferFeeMap = transferFeeMap
	return ao.transferFeeMap, nil
}

func (ao *arbitrageUsecase) GetTransferFee(exchange string, coin string) (float64, error) {
	ao.tm.Lock()
	defer ao.tm.Unlock()
	if tm, ok := ao.transferFeeMap[exchange]; ok {
		if fee, ok := tm[coin]; ok {
			return fee, nil
		}
	}
	m, err := ao.PrivateResourceRepository.TransferFee(exchange)
	if err != nil {
		return 0, err
	}
	ao.transferFeeMap[exchange] = m
	if tm, ok := ao.transferFeeMap[exchange]; ok {
		if fee, ok := tm[coin]; ok {
			return fee, nil
		}
	}
	return 0, errors.New("no coin")
}

func (ao *arbitrageUsecase) GetOrdersByArbitragerID(id int) ([]entity.ArbitrageOrder, error) {
	return ao.ArbitrageOrderRepository.GetOrdersByArbitragerID(id)
}

func (ao *arbitrageUsecase) GetOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	return ao.ArbitrageOrderRepository.GetOrdersByCurrencyPair(trading, settlement)
	panic("implement me")
}

func (ao *arbitrageUsecase) GetOrdersByExchange(exchange string) ([]entity.ArbitrageOrder, error) {
	return ao.ArbitrageOrderRepository.GetOrdersByExchange(exchange)
}

func (ao *arbitrageUsecase) GetOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	return ao.ArbitrageOrderRepository.GetOrdersByArbitragePair(exchange, trading, settlement)
}

func (ao *arbitrageUsecase) GetFilledOrdersByArbitragerID(id int) ([]entity.ArbitrageFilledOrder, error) {
	return ao.ArbitrageOrderRepository.GetFilledOrdersByArbitragerID(id)
}

func (ao *arbitrageUsecase) GetFilledOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	return ao.ArbitrageOrderRepository.GetFilledOrdersByCurrencyPair(trading, settlement)
}

func (ao *arbitrageUsecase) GetFilledOrdersByExchange(exchange string) ([]entity.ArbitrageFilledOrder, error) {
	return ao.ArbitrageOrderRepository.GetFilledOrdersByExchange(exchange)
}

func (ao *arbitrageUsecase) GetFilledOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	return ao.ArbitrageOrderRepository.GetFilledOrdersByArbitragePair(exchange, trading, settlement)
}

func NewArbitrageUsecase(pubResRep service.PublicResourceRepository, priResRep service.PrivateResourceRepository, settingRep service.SettingRepository, aoRep service.ArbitrageOrderRepository, arbLogRep service.ArbitragerLogRepository, arbSettingRep service.ArbitragerSettingRepository, conf *config.Config) ArbitrageUsecase {
	arbitragePairs := make([]entity.ArbitragePair, 0)
	arbitragers := NewArbitragerMap()
	arbitragerCancellers := NewArbitragerCancellersMap()
	return &arbitrageUsecase{
		pubResRep,
		priResRep,
		settingRep,
		aoRep,
		arbLogRep,
		arbSettingRep,
		arbitragePairs,
		arbitragers,
		nil,
		&sync.Mutex{},
		conf,
		arbitragerCancellers,
	}
}

func DisabledCurrencies() []string {
	return []string{"BTM", "DCN", "HOT"}
}

func (ao *arbitrageUsecase) GetPairs() ([]entity.ArbitragePair, error) {
	if len(ao.arbitragePairs) != 0 {
		return ao.arbitragePairs, nil
	}
	frozenCurrency := entity.NewFrozenCurrencySyncMap()
	exchangeSymbol := entity.NewExchangeSymbolSyncMap()
	arbitragePairs := make([]entity.ArbitragePair, 0)
	for _, v := range ao.config.Exchanges {
		pairs, err := ao.PublicResourceRepository.CurrencyPairs(v)
		if err != nil {
			return nil, errors.Wrapf(err, "failed to get currency pairs %v", v)
		}
		FrozenCurrencies, err := ao.PublicResourceRepository.FrozenCurrency(v)
		if err != nil {
			logger.Get().Errorf("failed to get frozen currencies on exchange %v", v)
			return nil, err
		}
		frozenCurrency.Set(v, FrozenCurrencies)
		var exchangePairs []models.CurrencyPair
		for _, pair := range pairs {
			isFrozen := false
			for _, fc := range FrozenCurrencies {
				if pair.Trading == fc || pair.Settlement == fc {
					isFrozen = true
				}
			}
			if !isFrozen {
				exchangePairs = append(exchangePairs, pair)
			}
		}
		exchangeSymbol.Set(v, exchangePairs)
	}

	exchangeList := listing.StringReplacer(ao.config.Exchanges)
	for exchangeComb := range listing.Combinations(exchangeList, 2, false, 5) {
		exchangeComb := exchangeComb.(listing.StringReplacer)
		currencyPair1 := exchangeSymbol.Get(exchangeComb[0])
		currencyPair2 := exchangeSymbol.Get(exchangeComb[1])
		exchange1 := exchangeComb[0]
		exchange2 := exchangeComb[1]
		disabledCurrencies := DisabledCurrencies()
		frozenCurrencies := frozenCurrency.GetAll()

		for _, c1 := range currencyPair1 {
			for _, c2 := range currencyPair2 {
				if c1.Settlement == c2.Settlement && c1.Trading == c2.Trading {
					isFrozen := false
					for _, fc := range frozenCurrencies {
						for _, f := range fc {
							if c1.Trading == f || c2.Trading == f || c1.Settlement == f || c2.Settlement == f {
								isFrozen = true
							}
						}
					}
					for _, dc := range disabledCurrencies {
						if c1.Trading == dc || c1.Settlement == dc {
							isFrozen = true
						}
					}
					cP := &entity.CurrencyPair{Trading: c1.Trading, Settlement: c1.Settlement}
					if !isFrozen {
						arbitragePairs = append(arbitragePairs,
							entity.ArbitragePair{
								ExchangeA: exchange1, ExchangeB: exchange2, CurrencyPair: cP,
							},
						)
					}
				}
			}
		}
	}
	ao.arbitragePairs = arbitragePairs
	return arbitragePairs, nil
}
