import {
  ArbitragePair,
  CurrencyPair,
  Board,
  BoardBar,
  OrderBookTick,
  KeySetting
} from '@/models/domain_pb'
import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb'
import { Message } from 'google-protobuf'

export class exchangeRepository {
  public async GetExchanges(): Promise<Array<string>> {
    let ret = Array<string>()
    //@ts-ignore
    const exchanges: Array<any> = await backend.exchangeHandler.GetExchanges()
    exchanges.forEach(exchange => {
      ret.push(exchange)
    })
    return ret
  }
  public async GetKeySetting(): Promise<Array<KeySetting>> {
    let ret = Array<KeySetting>()
    //@ts-ignore
    const keySettingsAny: Array<any> = await backend.exchangeHandler.GetKeys()
    for (const ksa of keySettingsAny) {
      const ks = new KeySetting()
      ks.setExchange(ksa.exchange)
      ks.setApiKey(ksa.api_key)
      ks.setSecKey(ksa.sec_key)
      ret.push(ks)
    }
    return ret
  }
  public async SetKeySetting(
    exchange: string,
    apiKey: string,
    secKey: string
  ): Promise<void> {
    const ks = new KeySetting()

    ks.setExchange(exchange)
    ks.setApiKey(apiKey)
    ks.setSecKey(secKey)
    //@ts-ignore
    await backend.exchangeHandler.SetKey(JSON.stringify(ks.toObject()))
  }
  public async SetApiKey(exchange: string, apiKey: string): Promise<void> {
    //@ts-ignore
    await backend.exchangeHandler.SetApiKey(exchange, apiKey)
  }
  public async SetSecKey(exchange: string, secKey: string): Promise<void> {
    //@ts-ignore
    await backend.exchangeHandler.SetSecKey(exchange, secKey)
  }
  public async DeleteKey(exchange: string): Promise<void> {
    //@ts-ignore
    await backend.exchangeHandler.DeleteKey(exchange)
  }
  public async GetKey(exchange: string): Promise<KeySetting> {
    //@ts-ignore
    const ksa = await backend.exchangeHandler.GetKey(exchange)
    let ks = new KeySetting()
    ks.setExchange(ksa.exchange)
    ks.setApiKey(ksa.api_key)
    ks.setSecKey(ksa.sec_key)
    return ks
  }
  public async GetPrecision(
    exchange: string,
    currencyPair: CurrencyPair
  ): Promise<{ PricePrecision: number; AmountPrecision: number }> {
    // @ts-ignore
    const precisionAny: any = await backend.exchangeHandler.GetPrecise(
      exchange,
      currencyPair.getTrading(),
      currencyPair.getSettlement()
    )
    return precisionAny
  }
  public async GetOrderBookTicks(): Promise<OrderBookTick[]> {
    const payload = Array<OrderBookTick>()
    // @ts-ignore
    const orderBookTicksAny: any[] = await backend.exchangeHandler.GetOrderBookTicks()
    for (const o of orderBookTicksAny) {
      const currencyPair = new CurrencyPair()
      currencyPair.setTrading(o.currency_pair.trading)
      currencyPair.setSettlement(o.currency_pair.settlement)
      const orderBookTick: OrderBookTick = new OrderBookTick()
      orderBookTick.setBestAskAmount(o.best_ask_amount)
      orderBookTick.setBestAskPrice(o.best_ask_price)
      orderBookTick.setBestBidAmount(o.best_bid_amount)
      orderBookTick.setBestBidPrice(o.best_bid_price)
      orderBookTick.setExchange(o.exchange)
      orderBookTick.setCurrencyPair(currencyPair)
      payload.push(orderBookTick)
    }
    return payload
  }
  public async GetCurrencyPairs(exchange: string): Promise<CurrencyPair[]> {
    // @ts-ignore
    const pairs: any[] = await window.backend.exchangeHandler.GetPairs(exchange)
    const currencyPairs: CurrencyPair[] = Array<CurrencyPair>()
    pairs.forEach((pair: any) => {
      const currencyPair = new CurrencyPair()
      currencyPair.setSettlement(pair.currency_pair.settlement)
      currencyPair.setTrading(pair.currency_pair.trading)
      currencyPairs.push(currencyPair)
    })
    return currencyPairs
  }
  public async GetBoard(
    exchange: string,
    currencyPair: CurrencyPair
  ): Promise<Board> {
    // @ts-ignore
    const boardsFromAPI: any = await window.backend.exchangeHandler.GetBoard(
      exchange,
      JSON.stringify(currencyPair.toObject())
    )
    const askBoardBars = Array<BoardBar>()
    boardsFromAPI.asks.forEach((ask: any) => {
      const askBoardBar = new BoardBar()
      askBoardBar.setPrice(ask.price)
      askBoardBar.setAmount(ask.amount)
      askBoardBars.push(askBoardBar)
    })
    const bidBoardBars = Array<BoardBar>()
    boardsFromAPI.bids.forEach((bid: any) => {
      const bidBoardBar = new BoardBar()
      bidBoardBar.setPrice(bid.price)
      bidBoardBar.setAmount(bid.amount)
      bidBoardBars.push(bidBoardBar)
    })
    const date_time = new google_protobuf_timestamp_pb.Timestamp()
    date_time.setSeconds(boardsFromAPI.date_time.seconds)
    date_time.setNanos(boardsFromAPI.date_time.nanos)
    const board = new Board()
    board.setAsksList(askBoardBars)
    board.setBidsList(bidBoardBars)
    board.setExchange(boardsFromAPI.exchange)
    board.setDateTime(date_time)
    board.setCurrencyPair(currencyPair)
    return board
  }
}

export const ExchangeRepository = new exchangeRepository()
