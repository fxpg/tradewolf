package interfaces

import (
	"context"
	"time"
)

type NonceRepository interface {
	GetNonceByUid(ctx context.Context, uid string) (time.Time, error)
	UpdateNonceByUid(ctx context.Context, uid string, nonce time.Time) error
}
