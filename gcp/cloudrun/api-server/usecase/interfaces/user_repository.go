package interfaces

import "context"

type UserRepository interface {
	GetAllowanceCallByDay(ctx context.Context, uid string) (int, error)
	GetAllowanceCallByMonth(ctx context.Context, uid string) (int, error)
	GetUidByApiKey(ctx context.Context, apikey string) (string, error)
}
