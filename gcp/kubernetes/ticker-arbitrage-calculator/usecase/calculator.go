package usecase

import (
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/service"
	"time"
)

type ArbitrageCalculatorUsecase interface {
	SetArbitrageWatchCondition(cond *model.ArbitrageWatchCondition) error
	FilterWatchingArbitrages(pair model.CurrencyPair) ([]model.ArbitrageWatchSetting, error)
}

type ArbitrageCalculatorImpl struct {
	arbitrageDataRepo service.ArbitrageDataRepository
	watchCondition    *model.ArbitrageWatchCondition
}

func NewArbitrageCalculatorUsecaseImpl(adr service.ArbitrageDataRepository, wd *model.ArbitrageWatchCondition) ArbitrageCalculatorUsecase {
	return &ArbitrageCalculatorImpl{
		arbitrageDataRepo: adr,
		watchCondition:    wd,
	}
}

func (aci *ArbitrageCalculatorImpl) SetArbitrageWatchCondition(cond *model.ArbitrageWatchCondition) error {
	aci.watchCondition = cond
	return nil
}

func (aci *ArbitrageCalculatorImpl) FilterWatchingArbitrages(pair model.CurrencyPair) ([]model.ArbitrageWatchSetting, error) {
	arbitrages, err := aci.arbitrageDataRepo.GetArbitrageTicker(pair)
	if err != nil {
		return nil, err
	}
	ret := make([]model.ArbitrageWatchSetting, 0)
	for _, a := range arbitrages {
		rateX := a.BestBidB / a.BestAskA
		rateY := a.BestBidA / a.BestAskB
		if (rateX > 2) || (rateY > 2) {
			continue
		}
		if rateX > 1+aci.watchCondition.ExpectedProfitRatio {
			ret = append(ret, model.ArbitrageWatchSetting{
				ArbitragePair:          a.ArbitragePair,
				TriggeredTime:          time.Now(),
				LimitTime:              time.Now().Add(time.Minute * 30),
				BuySide:                a.ArbitragePair.ExchangeA,
				ArbitrageRatio:         rateX,
				NowBuySideBestBidPrice: a.BestBidA,
				LastUpdateTime:         time.Now(),
				InitialBuyPrice:        a.BestAskA,
				ExpectedProfitRatio:    aci.watchCondition.ExpectedProfitRatio,
				ExpectedLossCutRatio:   aci.watchCondition.ExpectedLossCutRatio,
			})
		} else if rateY > 1+aci.watchCondition.ExpectedProfitRatio {
			ret = append(ret, model.ArbitrageWatchSetting{
				ArbitragePair:          a.ArbitragePair,
				TriggeredTime:          time.Now(),
				LimitTime:              time.Now().Add(time.Minute * 30),
				BuySide:                a.ArbitragePair.ExchangeB,
				ArbitrageRatio:         rateY,
				NowBuySideBestBidPrice: a.BestBidB,
				LastUpdateTime:         time.Now(),
				InitialBuyPrice:        a.BestAskB,
				ExpectedProfitRatio:    aci.watchCondition.ExpectedProfitRatio,
				ExpectedLossCutRatio:   aci.watchCondition.ExpectedLossCutRatio,
			})
		}
	}
	return ret, nil
}
