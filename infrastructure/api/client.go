package api

import (
	"sync"

	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/domain/service"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/private"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/public"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
)

func NewPublicResourceRepository(conf *config.Config) (service.PublicResourceRepository, error) {
	rep := &httpPublicClient{
		clientMap: NewPublicClientSyncMap(),
	}
	if len(conf.Exchanges) == 0 {
		return rep, errors.New("no conf exchanges")
	}
	for _, v := range conf.Exchanges {
		c, err := public.NewClient(v)
		if err != nil {
			logger.Get().Error(errors.Wrapf(err, "%", v))
			continue
		}
		rep.clientMap.Set(v, c)
	}
	return rep, nil
}

func NewApiKeyFunc(val string, setting service.SettingRepository) func() (string, error) {
	return func() (string, error) { return setting.GetApiKey(val) }
}
func NewSecKeyFunc(val string, setting service.SettingRepository) func() (string, error) {
	return func() (string, error) { return setting.GetSecKey(val) }
}

func NewPrivateResourceRepository(conf *config.Config, setting service.SettingRepository) (service.PrivateResourceRepository, error) {
	rep := &httpPrivateClient{
		clientMap: NewPrivateClientSyncMap(),
	}
	if len(conf.Exchanges) == 0 {
		return rep, errors.New("no conf exchanges")
	}
	for _, v := range conf.Exchanges {
		apiFunc := NewApiKeyFunc(v, setting)
		secFunc := NewSecKeyFunc(v, setting)
		pc, err := private.NewClient(conf.ProductionMode, v, apiFunc, secFunc)
		if err != nil {
			logger.Get().Error(errors.Wrap(err, v))
			continue
		}
		rep.clientMap.Set(v, pc)
	}
	return rep, nil
}

type httpPrivateClient struct {
	clientMap PrivateClientSyncMap
	setting   service.SettingRepository
}

func (h *httpPrivateClient) TransferFee(exchange string) (map[string]float64, error) {
	m := h.clientMap.Get(exchange)
	return m.TransferFee()
}

func (h *httpPrivateClient) TradeFeeRates(exchange string) (map[string]map[string]private.TradeFee, error) {
	m := h.clientMap.Get(exchange)
	return m.TradeFeeRates()
}

func (h *httpPrivateClient) TradeFeeRate(exchange string, trading string, settlement string) (private.TradeFee, error) {
	m := h.clientMap.Get(exchange)
	return m.TradeFeeRate(trading, settlement)
}

func (h *httpPrivateClient) Balances(exchange string) (map[string]float64, error) {
	return h.clientMap.Get(exchange).Balances()
}

func (h *httpPrivateClient) CompleteBalances(exchange string) (map[string]*models.Balance, error) {
	m := h.clientMap.Get(exchange)
	return m.CompleteBalances()
}

func (h *httpPrivateClient) CompleteBalance(exchange, coin string) (*models.Balance, error) {
	m := h.clientMap.Get(exchange)
	return m.CompleteBalance(coin)
}

func (h *httpPrivateClient) ActiveOrders(exchange string) ([]*models.Order, error) {
	m := h.clientMap.Get(exchange)
	return m.ActiveOrders()
}

func (h *httpPrivateClient) IsOrderFilled(exchange string, trading string, settlement string, orderNumber string) (bool, error) {
	m := h.clientMap.Get(exchange)
	return m.IsOrderFilled(trading, settlement, orderNumber)
}

func (h *httpPrivateClient) Order(exchange string, trading string, settlement string,
	orderType models.OrderType, price float64, amount float64) (string, error) {
	m := h.clientMap.Get(exchange)
	return m.Order(trading, settlement, orderType, price, amount)
}

func (h *httpPrivateClient) CancelOrder(exchange string, trading string, settlement string, orderType models.OrderType, orderNumber string) error {
	m := h.clientMap.Get(exchange)
	return m.CancelOrder(trading, settlement, orderType, orderNumber)
}

func (h *httpPrivateClient) Transfer(exchange string, typ string, addr string,
	amount float64, additionalFee float64) error {
	m := h.clientMap.Get(exchange)
	return m.Transfer(typ, addr, amount, additionalFee)
}

func (h *httpPrivateClient) Address(exchange string, c string) (string, error) {
	m := h.clientMap.Get(exchange)
	return m.Address(c)
}

func NewPrivateClientSyncMap() PrivateClientSyncMap {
	return PrivateClientSyncMap{make(map[string]private.PrivateClient), new(sync.Mutex)}
}

type privateClientMap map[string]private.PrivateClient

type PrivateClientSyncMap struct {
	privateClientMap
	m *sync.Mutex
}

func (sm *PrivateClientSyncMap) Get(exchange string) private.PrivateClient {
	sm.m.Lock()
	defer sm.m.Unlock()
	cli := sm.privateClientMap[exchange]
	return cli
}

func (sm *PrivateClientSyncMap) Set(exchange string, cli private.PrivateClient) {
	sm.m.Lock()
	defer sm.m.Unlock()
	sm.privateClientMap[exchange] = cli
}

type httpPublicClient struct {
	clientMap PublicClientSyncMap
}

/*func (h *httpPublicClient) Volume(exchange string, trading string, settlement string) (float64, error) {
	m := h.clientMap.Get(exchange)
	return m.Volume(trading, settlement)
}*/

func (h *httpPublicClient) CurrencyPairs(exchange string) ([]models.CurrencyPair, error) {
	m := h.clientMap.Get(exchange)
	return m.CurrencyPairs()
}

/*func (h *httpPublicClient) Rate(exchange string, trading string, settlement string) (float64, error) {
	m := h.clientMap.Get(exchange)
	return m.Rate(trading, settlement)
}
func (h *httpPublicClient) RateMap(exchange string) (map[string]map[string]float64, error) {
	m := h.clientMap.Get(exchange)
	return m.RateMap()
}*/
func (h *httpPublicClient) OrderBookTickMap(exchange string) (map[string]map[string]models.OrderBookTick, error) {
	m := h.clientMap.Get(exchange)
	return m.OrderBookTickMap()
}

/*func (h *httpPublicClient) VolumeMap(exchange string) (map[string]map[string]float64, error) {
	m := h.clientMap.Get(exchange)
	return m.VolumeMap()
}*/
func (h *httpPublicClient) FrozenCurrency(exchange string) ([]string, error) {
	m := h.clientMap.Get(exchange)
	return m.FrozenCurrency()
}
func (h *httpPublicClient) Board(exchange string, trading string, settlement string) (*models.Board, error) {
	m := h.clientMap.Get(exchange)
	return m.Board(trading, settlement)
}
func (h *httpPublicClient) Precise(exchange string, trading string, settlement string) (*models.Precisions, error) {
	m := h.clientMap.Get(exchange)
	return m.Precise(trading, settlement)
}

type publicClientMap map[string]public.PublicClient

func NewPublicClientSyncMap() PublicClientSyncMap {
	return PublicClientSyncMap{make(map[string]public.PublicClient), new(sync.Mutex)}
}

type PublicClientSyncMap struct {
	publicClientMap
	m *sync.Mutex
}

func (sm *PublicClientSyncMap) Get(exchange string) public.PublicClient {
	sm.m.Lock()
	defer sm.m.Unlock()
	cli, _ := sm.publicClientMap[exchange]
	return cli
}

func (sm *PublicClientSyncMap) GetAll() map[string]public.PublicClient {
	return sm.publicClientMap
}

func (sm *PublicClientSyncMap) Set(exchange string, cli public.PublicClient) {
	sm.m.Lock()
	defer sm.m.Unlock()
	sm.publicClientMap[exchange] = cli
}
