package service

import "gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"

type ArbitrageDataRepository interface {
	GetArbitrageTicker(currencyPair model.CurrencyPair) ([]model.Arbitrage, error)
}
