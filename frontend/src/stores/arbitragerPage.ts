import {
  Module,
  VuexModule,
  Mutation,
  Action,
  getModule,
  MutationAction
} from 'vuex-module-decorators'
import {
  ArbitragerLog,
  ArbitrageOrder,
  CurrencyPair,
  ArbitragePair,
  BoardBar,
  ArbitragePairArbitrage,
  NetArbitrageByArbitragePair
} from '@/models/domain_pb'
import store from '@/stores/index'
import { ArbitragerRepository } from '@/repositories/arbitrager'
import { ExchangeRepository } from '@/repositories/exchange'

@Module({
  dynamic: true,
  store,
  namespaced: true,
  name: 'ArbitragerPageModule'
})
export class arbitragerPageModule extends VuexModule {
  selectedArbitragePair: ArbitragePair = new ArbitragePair()
  selectedArbitragerID: number = 0
  selectedSettingSectionTab: number = 0
  selectedArbitragePairArbitrage: ArbitragePairArbitrage = new ArbitragePairArbitrage()
  exchangeSettingDialog: boolean = false
  arbitrageOrders: Array<ArbitrageOrder> = new Array<ArbitrageOrder>()
  arbitragerLogs: Array<ArbitragerLog> = new Array<ArbitragerLog>()
  exchangeABoardAsks: Array<BoardBar> = new Array<BoardBar>()
  exchangeABoardBids: Array<BoardBar> = new Array<BoardBar>()
  exchangeBBoardAsks: Array<BoardBar> = new Array<BoardBar>()
  exchangeBBoardBids: Array<BoardBar> = new Array<BoardBar>()
  arbitragePairArbitrage: Array<ArbitragePairArbitrage> = new Array<
    ArbitragePairArbitrage
  >()
  netArbitragesByArbitragePair: Array<NetArbitrageByArbitragePair> = new Array<NetArbitrageByArbitragePair>()
  transferFeeMap: { [key: string]: { [key: string]: number } } = {}
  balancesMap: { [key: string]: { [key: string]: number } } = {}

  get getSelectedArbitragePairArbitrage(): ArbitragePairArbitrage {
    return this.selectedArbitragePairArbitrage
  }
  get getTransferFeeMap(): { [key: string]: { [key: string]: number } } {
    return this.transferFeeMap
  }
  get getBalancesMap(): { [key: string]: { [key: string]: number } } {
    return this.balancesMap
  }
  get getArbitragePairArbitrage(): Array<ArbitragePairArbitrage> {
    return this.arbitragePairArbitrage
  }
  get getNetArbitragesByArbitragePair(): Array<NetArbitrageByArbitragePair> {
    return this.netArbitragesByArbitragePair
  }
  get getExchangeABoardAsks(): Array<BoardBar> {
    return this.exchangeABoardAsks
  }
  get getExchangeABoardBids(): Array<BoardBar> {
    return this.exchangeABoardBids
  }
  get getExchangeBBoardAsks(): Array<BoardBar> {
    return this.exchangeBBoardAsks
  }
  get getExchangeBBoardBids(): Array<BoardBar> {
    return this.exchangeBBoardBids
  }
  get getArbitrageOrders(): Array<ArbitrageOrder> {
    return this.arbitrageOrders
  }
  get getArbitragerLogs(): Array<ArbitragerLog> {
    return this.arbitragerLogs
  }
  get getExchangeSettingDialog(): boolean {
    return this.exchangeSettingDialog
  }
  get getSelectedArbitragePair(): ArbitragePair {
    return this.selectedArbitragePair
  }
  get getSelectedArbitragerID(): number {
    return this.selectedArbitragerID
  }
  get getSelectedSettingSectionTab(): number {
    return this.selectedSettingSectionTab
  }
  @Mutation
  public SET_SELECTED_ARBITRAGE_PAIR_ARBITRAGE(val: ArbitragePairArbitrage) {
    this.selectedArbitragePairArbitrage = val
  }
  @Mutation
  public SET_TRANSFER_FEE_MAP(val: {
    [key: string]: { [key: string]: number }
  }) {
    this.transferFeeMap = val
  }
  @Mutation
  public SET_BALANCES_MAP(val: { [key: string]: { [key: string]: number } }) {
    this.balancesMap = val
  }
  @Mutation
  public SET_ARBITRAGE_PAIR_ARBITRAGE(val: Array<ArbitragePairArbitrage>) {
    this.arbitragePairArbitrage = val
  }
  @Mutation
  public SET_NET_ARBITRAGES_BY_ARBITRAGE_PAIR_ARBITRAGE(val: Array<NetArbitrageByArbitragePair>) {
    this.netArbitragesByArbitragePair = val
  }
  @Mutation
  public SET_SELECTED_ARBITRAGER_ID(id: number) {
    this.selectedArbitragerID = id
  }
  @Mutation
  public SET_SELECTED_ARBITRAGE_PAIR(arbitragePair: ArbitragePair) {
    this.selectedArbitragePair = arbitragePair
  }
  @Mutation
  public SET_SELECTED_SETTING_SECTION_TAB(tab: number) {
    this.selectedSettingSectionTab = tab
  }
  @Mutation
  public SET_EXCHANGE_SETTING_DIALOG(val: boolean) {
    this.exchangeSettingDialog = val
  }
  @Mutation
  public SET_EXCHANGE_A_BOARD_ASKS(val: Array<BoardBar>) {
    this.exchangeABoardAsks = val
  }
  @Mutation
  public SET_EXCHANGE_A_BOARD_BIDS(val: Array<BoardBar>) {
    this.exchangeABoardBids = val
  }
  @Mutation
  public SET_EXCHANGE_B_BOARD_ASKS(val: Array<BoardBar>) {
    this.exchangeBBoardAsks = val
  }
  @Mutation
  public SET_EXCHANGE_B_BOARD_BIDS(val: Array<BoardBar>) {
    this.exchangeBBoardBids = val
  }
  @Mutation
  public SET_ARBITRAGE_ORDERS(val: Array<ArbitrageOrder>) {
    this.arbitrageOrders = val
  }
  @Mutation
  public SET_ARBITRAGER_LOGS(val: Array<ArbitragerLog>) {
    this.arbitragerLogs = val
  }
  @Action({ commit: 'SET_SELECTED_ARBITRAGE_PAIR_ARBITRAGE' })
  public async setSelectedArbitragePairArbitrage(
    arbitragePairArbitrage: ArbitragePairArbitrage
  ) {
    return arbitragePairArbitrage
  }
  @Action({ commit: 'SET_BALANCES_MAP' })
  public async setBalancesMap() {
    const ret = await ArbitragerRepository.GetBalances()
    return ret
  }
  @Action({ commit: 'SET_TRANSFER_FEE_MAP' })
  public async setTransferFeeMap() {
    const ret = await ArbitragerRepository.GetTransferFees()
    return ret
  }
  @Action({ commit: 'SET_ARBITRAGE_PAIR_ARBITRAGE' })
  public async setArbitragePairArbitrage() {
    const ret = await ArbitragerRepository.GetArbitrageByArbitragePair()
    return ret
  }
  @Action({ commit: 'SET_NET_ARBITRAGES_BY_ARBITRAGE_PAIR_ARBITRAGE' })
  public async setNetArbitragesByArbitragePair() {
    const ret = await ArbitragerRepository.GetNetArbitragesByArbitragePair()
    return ret
  }
  @Action({ commit: 'SET_EXCHANGE_A_BOARD_ASKS' })
  public async setExchangeABoardAsks(selectedArbitragePair: ArbitragePair) {
    const result = await ExchangeRepository.GetBoard(
      selectedArbitragePair.getExchangeA(),
      selectedArbitragePair.getCurrencyPair()!
    )
    return result.getAsksList()
  }
  @Action({ commit: 'SET_EXCHANGE_A_BOARD_BIDS' })
  public async setExchangeABoardBids(selectedArbitragePair: ArbitragePair) {
    const result = await ExchangeRepository.GetBoard(
      selectedArbitragePair.getExchangeA(),
      selectedArbitragePair.getCurrencyPair()!
    )
    return result.getBidsList()
  }
  @Action({ commit: 'SET_EXCHANGE_B_BOARD_ASKS' })
  public async setExchangeBBoardAsks(selectedArbitragePair: ArbitragePair) {
    const result = await ExchangeRepository.GetBoard(
      selectedArbitragePair.getExchangeB(),
      selectedArbitragePair.getCurrencyPair()!
    )
    return result.getAsksList()
  }
  @Action({ commit: 'SET_EXCHANGE_B_BOARD_BIDS' })
  public async setExchangeBBoardBids(selectedArbitragePair: ArbitragePair) {
    const result = await ExchangeRepository.GetBoard(
      selectedArbitragePair.getExchangeB(),
      selectedArbitragePair.getCurrencyPair()!
    )
    return result.getBidsList()
  }
  @Action({ commit: 'SET_EXCHANGE_SETTING_DIALOG' })
  public setExchangeSettingDialog(val: boolean) {
    return val
  }
  @Action({ commit: 'SET_ARBITRAGE_ORDERS' })
  public async setArbitrageOrders(id: number) {
    const ret = await ArbitragerRepository.GetOrdersByArbitragerID(id)
    return ret
  }
  @Action({ commit: 'SET_ARBITRAGER_LOGS' })
  public async setArbitragerLogs(id: number) {
    const ret = await ArbitragerRepository.GetArbitragerLogsByArbitragerID(id)
    return ret
  }
  @Action({ commit: 'SET_SELECTED_SETTING_SECTION_TAB' })
  public setSelectedSettingSectionTab(id: number) {
    return id
  }
  @Action({ commit: 'SET_SELECTED_ARBITRAGER_ID' })
  public setSelectedArbitragerID(id: number) {
    return id
  }
  @Action({ commit: 'SET_SELECTED_ARBITRAGE_PAIR' })
  public async setSelectedArbitragePair(payload: {
    currencyPair: CurrencyPair
    exchangeA: string
    exchangeB: string
  }) {
    const arbitragePair = new ArbitragePair()
    arbitragePair.setCurrencyPair(payload.currencyPair)
    arbitragePair.setExchangeA(payload.exchangeA)
    arbitragePair.setExchangeB(payload.exchangeB)
    return arbitragePair
  }
}

export const ArbitragerPageModule = getModule(arbitragerPageModule)
