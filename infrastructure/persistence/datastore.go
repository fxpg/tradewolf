package persistence

import (
	"strconv"

	"github.com/golang/protobuf/proto"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/domain/service"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
	"go.etcd.io/bbolt"
)

const ARBITRAGER_SETTING_BUCKET = "ARBITRAGER_SETTING"

func NewArbitragerSettingRepository(db *bbolt.DB) service.ArbitragerSettingRepository {
	return &ArbitragerSettingRepositoryBbolt{db}
}

type ArbitragerSettingRepositoryBbolt struct {
	DB *bbolt.DB
}

func (a *ArbitragerSettingRepositoryBbolt) FindIds() ([]int, error) {
	arbitragerSettingIds := make([]int, 0)
	return arbitragerSettingIds, a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		err = b.ForEach(func(k, v []byte) error {
			if len(v) != 0 {
				var arbitragerSetting entity.ArbitragerSetting
				err := proto.UnmarshalText(string(v), &arbitragerSetting)
				if err != nil {
					logger.Get().Error(err)
					return nil
				}
				id, err := strconv.Atoi(string(k))
				if err != nil {
					logger.Get().Error(err)
					return nil
				}
				arbitragerSettingIds = append(arbitragerSettingIds, id)
			}
			return nil
		})
		return err
	})
}

func (a *ArbitragerSettingRepositoryBbolt) LastId() (int, error) {
	var lastId int
	err := a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		id, err := b.NextSequence()
		lastId = int(id)
		return err
	})
	return lastId, err
}

func (a *ArbitragerSettingRepositoryBbolt) Insert(id int, arbitragerSetting *entity.ArbitragerSetting) error {
	return a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		err = b.Put([]byte(strconv.Itoa(id)), []byte(arbitragerSetting.String()))
		return err
	})
}

func (a *ArbitragerSettingRepositoryBbolt) FindById(id int) (*entity.ArbitragerSetting, error) {
	var arbitragerSetting entity.ArbitragerSetting
	err := a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		v := b.Get([]byte(strconv.Itoa(id)))
		if len(v) != 0 {
			err := proto.UnmarshalText(string(v), &arbitragerSetting)
			return err
		}
		return errors.New("no arbitrager setting")
	})
	return &arbitragerSetting, err
}

func (a *ArbitragerSettingRepositoryBbolt) FindAll() ([]entity.ArbitragerSetting, error) {
	arbitragerSettings := make([]entity.ArbitragerSetting, 0)
	return arbitragerSettings, a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		err = b.ForEach(func(k, v []byte) error {
			if len(v) != 0 {
				var arbitragerSetting entity.ArbitragerSetting
				err := proto.UnmarshalText(string(v), &arbitragerSetting)
				if err == nil {
					arbitragerSettings = append(arbitragerSettings, arbitragerSetting)
				}
			}
			return nil
		})
		return err
	})
}

func (a *ArbitragerSettingRepositoryBbolt) Delete(id int) error {
	return a.DB.Update(func(tx *bbolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(ARBITRAGER_SETTING_BUCKET))
		if err != nil {
			return err
		}
		err = b.Put([]byte(strconv.Itoa(id)), []byte(""))
		return err
	})
}

type ArbitragerLogRepositorySqlite struct {
	DB *gorm.DB
}

func (a *ArbitragerLogRepositorySqlite) Insert(log *entity.ArbitragerLog) error {
	if err := a.DB.Create(log).Error; err != nil {
		return err
	}
	return nil
}

func (a *ArbitragerLogRepositorySqlite) FindByArbitragerId(arbitragerId int64) ([]entity.ArbitragerLog, error) {
	logs := make([]entity.ArbitragerLog, 0)
	if err := a.DB.Where("arbitrager_id = ?", arbitragerId).Find(&logs).Error; err != nil {
		return nil, err
	}
	return logs, nil
}

func NewArbitragerLogRepository(db *gorm.DB) service.ArbitragerLogRepository {
	return &ArbitragerLogRepositorySqlite{db}
}

type ArbitrageOrderRepositorySqlite struct {
	DB *gorm.DB
}

func (a ArbitrageOrderRepositorySqlite) CreateOrder(order *entity.ArbitrageOrder) error {
	if err := a.DB.Create(order).Error; err != nil {
		return err
	}
	return nil
}

func (a ArbitrageOrderRepositorySqlite) FilledOrder(order *entity.ArbitrageFilledOrder) error {
	if err := a.DB.Create(order).Error; err != nil {
		return err
	}
	return nil
}

func (a ArbitrageOrderRepositorySqlite) GetOrdersByArbitragerID(id int) ([]entity.ArbitrageOrder, error) {
	orders := make([]entity.ArbitrageOrder, 0)
	if err := a.DB.Where("arbitrager_id = ?", id).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	orders := make([]entity.ArbitrageOrder, 0)
	if err := a.DB.Where("trading = ? AND settlement = ?", trading, settlement).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetOrdersByExchange(exchange string) ([]entity.ArbitrageOrder, error) {
	orders := make([]entity.ArbitrageOrder, 0)
	if err := a.DB.Where("exchange = ?", exchange).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	orders := make([]entity.ArbitrageOrder, 0)
	if err := a.DB.Where("exchange = ? AND trading = ? AND settlement = ?", exchange, trading, settlement).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetFilledOrdersByArbitragerID(id int) ([]entity.ArbitrageFilledOrder, error) {
	orders := make([]entity.ArbitrageFilledOrder, 0)
	if err := a.DB.Where("arbitrager_id = ?", id).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetFilledOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	orders := make([]entity.ArbitrageFilledOrder, 0)
	if err := a.DB.Where("trading = ? AND settlement = ?", trading, settlement).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetFilledOrdersByExchange(exchange string) ([]entity.ArbitrageFilledOrder, error) {
	orders := make([]entity.ArbitrageFilledOrder, 0)
	if err := a.DB.Where("exchange = ?", exchange).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func (a ArbitrageOrderRepositorySqlite) GetFilledOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	orders := make([]entity.ArbitrageFilledOrder, 0)
	if err := a.DB.Where("exchange = ? AND trading = ? AND settlement = ?", exchange, trading, settlement).Find(&orders).Error; err != nil {
		return nil, err
	}
	return orders, nil
}

func NewArbitrageOrderRepository(db *gorm.DB) service.ArbitrageOrderRepository {
	return &ArbitrageOrderRepositorySqlite{db}
}

type SettingRepositorySqlite struct {
	DB *gorm.DB
}

func NewSettingRepository(db *gorm.DB) service.SettingRepository {
	return &SettingRepositorySqlite{db}
}

func (s *SettingRepositorySqlite) SetApiKey(exchange string, apiKey string) error {
	err := s.DB.Where("exchange = ?", exchange).Update("api_key", apiKey).Error
	return err
}

func (s *SettingRepositorySqlite) SetSecKey(exchange string, secKey string) error {
	err := s.DB.Where("exchange = ?", exchange).Update("sec_key", secKey).Error
	return err
}

func (s *SettingRepositorySqlite) SetKey(setting *entity.KeySetting) error {
	if err := s.DB.Create(setting).Error; err != nil {
		return err
	}
	return nil
}

func (s *SettingRepositorySqlite) GetKeys(exchange string) ([]entity.KeySetting, error) {
	settings := make([]entity.KeySetting, 0)
	if err := s.DB.Where("exchange = ?", exchange).Find(&settings).Error; err != nil {
		return nil, err
	}
	return settings, nil
}

func (s *SettingRepositorySqlite) GetApiKey(exchange string) (string, error) {
	keySet, err := s.GetLastKey(exchange)
	if err != nil {
		return "", err
	}
	return keySet.ApiKey, err
}

func (s *SettingRepositorySqlite) GetSecKey(exchange string) (string, error) {
	keySet, err := s.GetLastKey(exchange)
	if err != nil {
		return "", err
	}
	return keySet.SecKey, err
}

func (s *SettingRepositorySqlite) GetLastKey(exchange string) (*entity.KeySetting, error) {
	var setting entity.KeySetting
	if err := s.DB.Where("exchange = ?", exchange).Last(&setting).Error; err != nil {
		return nil, err
	}
	if &setting == nil {
		return &setting, errors.Errorf("failed to get key setting in %s", exchange)
	}
	return &setting, nil
}

func (s *SettingRepositorySqlite) Delete(setting *entity.KeySetting) (bool, error) {
	db := s.DB.Where("exchange = ?", setting.Exchange).Delete(setting)
	if db.RecordNotFound() {
		return false, nil
	}
	if db.Error != nil {
		return false, db.Error
	}
	return true, nil
}
