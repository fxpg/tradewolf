package controller

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"math"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// var psArgs = GetEnv("PS_ARGS", "pid,ppid,%cpu,%mem,lstart,user,command")

type ArbitrageWatchManagerImpl struct {
	watchingTable          *tview.Table
	completeTable          *tview.Table
	arbitrageWatchSettings []model.ArbitrageWatchSetting
	m                      sync.Mutex

	completeArbitrageWatchSettings []model.ArbitrageWatchSetting
	completeM                      sync.Mutex

	FilterWord string
	Sort       model.SortType
	Filter     model.FilterType
}

func (p *ArbitrageWatchManagerImpl) SetSortType(t model.SortType) error {
	p.Sort = t
	return nil
}

type ArbitrageWatchManager interface {
	UpdateData(arbitrages []model.ArbitrageWatchSetting) error
	UpdateView() error

	PrimitiveOnWatchingTable() tview.Primitive
	SetDoneFuncOnWatchingTable(f func(tcell.Key)) *tview.Table
	SelectOnWatchingTable(row int, col int)
	SelectedOnWatchingTable() *model.ArbitrageWatchSetting
	SetSelectionChangedFuncOnWatchingTable(func(row, col int)) *tview.Table

	PrimitiveOnCompleteTable() tview.Primitive
	SetDoneFuncOnCompleteTable(f func(tcell.Key)) *tview.Table
	SelectOnCompleteTable(row int, col int)
	SelectedOnCompleteTable() *model.ArbitrageWatchSetting
	SetSelectionChangedFuncOnCompleteTable(func(row, col int)) *tview.Table

	SetFilterWord(text string) error
	SetSortType(t model.SortType) error
	SetFilterType(t model.FilterType) error
}

func (p *ArbitrageWatchManagerImpl) SetFilterType(t model.FilterType) error {
	p.Filter = t
	return nil
}

func (p *ArbitrageWatchManagerImpl) SetFilterWord(text string) error {
	p.FilterWord = text
	return nil
}

func (p *ArbitrageWatchManagerImpl) SetSelectionChangedFuncOnWatchingTable(f func(int, int)) *tview.Table {
	return p.watchingTable.SetSelectionChangedFunc(f)
}

func (p *ArbitrageWatchManagerImpl) SelectOnWatchingTable(row int, col int) {
	p.watchingTable.Select(row, col)
}

func (p *ArbitrageWatchManagerImpl) SelectedOnWatchingTable() *model.ArbitrageWatchSetting {
	settings := p.sortAndFilter()
	if len(settings) == 0 {
		return nil
	}
	row, _ := p.watchingTable.GetSelection()
	if row < 0 {
		return nil
	}
	if len(settings) < row {
		return nil
	}
	return &settings[row-1]
}

func (p *ArbitrageWatchManagerImpl) PrimitiveOnWatchingTable() tview.Primitive {
	return p.watchingTable
}

func (p *ArbitrageWatchManagerImpl) SetDoneFuncOnWatchingTable(f func(tcell.Key)) *tview.Table {
	return p.watchingTable.SetDoneFunc(f)
}
func (p *ArbitrageWatchManagerImpl) SetSelectionChangedFuncOnCompleteTable(f func(int, int)) *tview.Table {
	return p.watchingTable.SetSelectionChangedFunc(f)
}

func (p *ArbitrageWatchManagerImpl) SelectOnCompleteTable(row int, col int) {
	p.watchingTable.Select(row, col)
}

func (p *ArbitrageWatchManagerImpl) SelectedOnCompleteTable() *model.ArbitrageWatchSetting {
	settings := p.sortAndFilter()
	if len(settings) == 0 {
		return nil
	}
	row, _ := p.watchingTable.GetSelection()
	if row < 0 {
		return nil
	}
	if len(settings) < row {
		return nil
	}
	return &settings[row-1]
}

func (p *ArbitrageWatchManagerImpl) PrimitiveOnCompleteTable() tview.Primitive {
	return p.completeTable
}

func (p *ArbitrageWatchManagerImpl) SetDoneFuncOnCompleteTable(f func(tcell.Key)) *tview.Table {
	return p.completeTable.SetDoneFunc(f)
}

func NewArbitrageWatchManager() ArbitrageWatchManager {
	p := &ArbitrageWatchManagerImpl{
		watchingTable: tview.NewTable().Select(0, 0).SetFixed(1, 1).SetSelectable(true, false),
		completeTable: tview.NewTable().Select(0, 0).SetFixed(1, 1).SetSelectable(true, false),
		FilterWord:    "",
		Sort:          model.SORT_TRIGGER_DATETIME_DESC,
	}
	p.watchingTable.SetBorder(true).SetTitle("Arbitrage Pairs").SetTitleAlign(tview.AlignCenter)
	p.completeTable.SetBorder(true).SetTitle("Complete Arbitrages").SetTitleAlign(tview.AlignCenter)
	return p
}

var headers = []string{
	"ExchangeA",        //0
	"ExchangeB",        //1
	"CurrencyPair",     //2
	"BuySide",          //3
	"InitialArbitrage", //4
	"InitialBuyPrice",  //5
	"NowPrice",         //6
	"TriggerDateTime",  //7
	"LastUpdateTime",   //8
	"IsProfitNow",      //9
	"IsProfitted",      //10
}

func (p *ArbitrageWatchManagerImpl) UpdateData(arbitrages []model.ArbitrageWatchSetting) error {
	p.m.Lock()
	defer p.m.Unlock()
	for _, arbitrage := range arbitrages {
		existFlag := false
		for i, a := range p.arbitrageWatchSettings {
			if a.ArbitragePair == arbitrage.ArbitragePair && a.BuySide == arbitrage.BuySide {
				// 監視中裁定ペアリストに存在すれば、現在価格等のみ更新して、一覧へは追加しない
				p.arbitrageWatchSettings[i].NowBuySideBestBidPrice = arbitrage.NowBuySideBestBidPrice
				p.arbitrageWatchSettings[i].LastUpdateTime = arbitrage.LastUpdateTime
				if arbitrage.NowBuySideBestBidPrice > p.arbitrageWatchSettings[i].InitialBuyPrice {
					p.arbitrageWatchSettings[i].IsProfitNow = true
					p.arbitrageWatchSettings[i].IsProfitted = true
				} else {
					p.arbitrageWatchSettings[i].IsProfitNow = false
				}
				// BuySideのBestBidPrice最大値を更新する
				p.arbitrageWatchSettings[i].MaxBestBidPrice = math.Max(arbitrage.NowBuySideBestBidPrice, p.arbitrageWatchSettings[i].MaxBestBidPrice)
				existFlag = true
				break
			}
		}
		if !existFlag {
			p.arbitrageWatchSettings = append(p.arbitrageWatchSettings, arbitrage)
		}
	}
	return nil
}

func (p *ArbitrageWatchManagerImpl) sortAndFilter() []model.ArbitrageWatchSetting {
	p.m.Lock()
	settings := p.arbitrageWatchSettings
	p.m.Unlock()

	if p.Sort == model.SORT_TRIGGER_DATETIME_DESC {
		sort.SliceStable(settings, func(i, j int) bool { return settings[i].TriggeredTime.After(settings[j].TriggeredTime) })
	} else if p.Sort == model.SORT_TRIGGER_DATETIME {
		sort.SliceStable(settings, func(i, j int) bool { return settings[i].TriggeredTime.Before(settings[j].TriggeredTime) })
	}

	ret := make([]model.ArbitrageWatchSetting, 0)
	if p.Filter == model.FILTER_IS_PROFIT_NOW {
		for _, s := range settings {
			if s.IsProfitNow {
				ret = append(ret, s)
			}
		}
	} else if p.Filter == model.FILTER_IS_PROFITTED {
		for _, s := range settings {
			if s.IsProfitted {
				ret = append(ret, s)
			}
		}
	} else if p.Filter == model.FILTER_IS_SAME_TRADING_DETECTED {
		combs := combination(settings, 2)
		for _, combi := range combs {
			if combi[0].ArbitragePair.Trading == combi[1].ArbitragePair.Trading {
				ret = append(ret, combi[0])
				ret = append(ret, combi[1])
			}
		}
	} else {
		ret = settings
	}
	return ret
}

func combination(ar []model.ArbitrageWatchSetting, n int) (result [][]model.ArbitrageWatchSetting) {
	if n <= 0 || len(ar) < n {
		return
	}
	if n == 1 {
		for _, a := range ar {
			result = append(result, []model.ArbitrageWatchSetting{a})
		}
	} else if len(ar) == n {
		result = append(result, ar)
	} else {
		for _, a := range combination(ar[1:], n-1) {
			result = append(result, append([]model.ArbitrageWatchSetting{ar[0]}, a...))
		}
		result = append(result, combination(ar[1:], n)...)
	}
	return
}

func (p *ArbitrageWatchManagerImpl) UpdateView() error {
	table := p.watchingTable.Clear()
	// ヘッダーをつける
	for i, h := range headers {
		table.SetCell(0, i, &tview.TableCell{
			Text:            h,
			NotSelectable:   true,
			Align:           tview.AlignLeft,
			Color:           tcell.ColorYellow,
			BackgroundColor: tcell.ColorDefault,
		})
	}

	// 裁定ペアをテーブルへ組み込む
	var i int
	for _, a := range p.sortAndFilter() {
		str := a.ArbitragePair.ExchangeA + a.ArbitragePair.ExchangeB + a.ArbitragePair.Trading + a.ArbitragePair.Settlement
		if strings.Index(strings.ToLower(str), p.FilterWord) == -1 {
			continue
		}
		initialBuyPrice := strconv.FormatFloat(a.InitialBuyPrice, 'f', 8, 64)
		arbitrageRatio := strconv.FormatFloat(a.ArbitrageRatio, 'f', 8, 64)
		nowPrice := strconv.FormatFloat(a.NowBuySideBestBidPrice, 'f', 8, 64)
		triggeredDatetime := a.TriggeredTime.Format("2006/01/02 15:04:05")
		lastUpdateTime := a.LastUpdateTime.Format("2006/01/02 15:04:05")
		isProfitNow := ""
		if a.IsProfitNow {
			isProfitNow = "o"
		}
		isProfitted := ""
		if a.IsProfitted {
			isProfitted = "o"
		}
		table.SetCell(i+1, 0, tview.NewTableCell(a.ArbitragePair.ExchangeA))
		table.SetCell(i+1, 1, tview.NewTableCell(a.ArbitragePair.ExchangeB))
		table.SetCell(i+1, 2, tview.NewTableCell(a.ArbitragePair.Trading+"/"+a.ArbitragePair.Settlement))
		table.SetCell(i+1, 3, tview.NewTableCell(a.BuySide))
		table.SetCell(i+1, 4, tview.NewTableCell(arbitrageRatio))
		table.SetCell(i+1, 5, tview.NewTableCell(initialBuyPrice))
		table.SetCell(i+1, 6, tview.NewTableCell(nowPrice))
		table.SetCell(i+1, 7, tview.NewTableCell(triggeredDatetime))
		table.SetCell(i+1, 8, tview.NewTableCell(lastUpdateTime))
		table.SetCell(i+1, 9, tview.NewTableCell(isProfitNow))
		table.SetCell(i+1, 10, tview.NewTableCell(isProfitted))
		i++
	}
	return nil
}
