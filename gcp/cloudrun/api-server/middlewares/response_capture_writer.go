package middlewares

import "net/http"

type ResponseCaptureWriter struct {
	inner http.ResponseWriter

	body []byte
	header http.Header
	statusCode int
}

func NewResponseCaptureWriter(innerWriter http.ResponseWriter) *ResponseCaptureWriter {
	return &ResponseCaptureWriter{
		inner: innerWriter,
	}
}

func (w *ResponseCaptureWriter) CapturedBody() []byte {
	return w.body
}

func (w *ResponseCaptureWriter) CapturedHeader() http.Header {
	return w.header
}

func (w *ResponseCaptureWriter) CapturedStatusCode() int {
	return w.statusCode
}

func (w *ResponseCaptureWriter) Header() http.Header {
	return w.inner.Header()
}

func (w *ResponseCaptureWriter) Write(bs []byte) (int, error) {
	w.body = bs
	return w.inner.Write(bs)
}

func (w *ResponseCaptureWriter) WriteHeader(statusCode int) {
	w.header = w.inner.Header()
	w.statusCode = statusCode
	w.inner.WriteHeader(statusCode)
}
