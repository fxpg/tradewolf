module gitlab.com/fxpg/tradewolf

require (
	cloud.google.com/go v0.44.3
	firebase.google.com/go v3.12.0+incompatible // indirect
	github.com/DATA-DOG/go-sqlmock v1.3.3 // indirect
	github.com/Jeffail/gabs v1.4.0
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/OneOfOne/struct2ts v1.0.4 // indirect
	github.com/antonholmquist/jason v1.0.0
	github.com/ashman1984/shrimpy-go v0.0.0-20190831033609-7a1a57d48ff9 // indirect
	github.com/bluele/slack v0.0.0-20180528010058-b4b4d354a079
	github.com/dchest/htmlmin v1.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/favadi/protoc-go-inject-tag v1.0.0 // indirect
	github.com/fxpgr/go-exchange-client v0.0.0-20191210133504-2ee2b64dce60 // indirect
	github.com/go-openapi/errors v0.19.3 // indirect
	github.com/go-openapi/runtime v0.19.11 // indirect
	github.com/go-redis/redis/v7 v7.0.0-beta.5 // indirect
	github.com/golang/mock v1.3.2-0.20191024042307-0b73a1dbc3e1
	github.com/golang/protobuf v1.3.2
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/kokardy/listing v0.0.0-20140516154625-795534c33c5a
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/leaanthony/mewn v0.10.7
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/rs/xid v1.2.1
	github.com/selvatico/go-mocket v1.0.7
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/tidwall/gjson v1.3.2
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/wailsapp/wails v1.0.2
	go.etcd.io/bbolt v1.3.3
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/dig v1.7.0
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20191224085550-c709ea063b76 // indirect
	google.golang.org/api v0.9.0 // indirect
	google.golang.org/appengine v1.6.2 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
	google.golang.org/grpc v1.23.0 // indirect
	gopkg.in/AlecAivazis/survey.v1 v1.8.7 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	gopkg.in/yaml.v2 v2.2.4
	gopkg.in/yaml.v3 v3.0.0-20191120175047-4206685974f2 // indirect
)

go 1.13
