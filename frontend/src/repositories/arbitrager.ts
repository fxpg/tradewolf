import {
  ArbitragePair,
  CurrencyPair,
  ArbitragerSetting,
  ArbitragePairArbitrage,
  OrderBookTick,
  ArbitrageOrder,
  ArbitragerLog,
  Timestamp,
  NetArbitrageByArbitragePair,
  GormModel
} from '@/models/domain_pb'
import * as timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb'

class arbitragerRepository {
  public async GetArbitragerLogsByArbitragerID(
    arbitragerId: number
  ): Promise<Array<ArbitragerLog>> {
    const payload = Array<ArbitragerLog>()
    try {
      // @ts-ignore
      const logs: any[] = await backend.arbitrageHandler.GetArbitragerLogsByArbitragerID(
        Number(arbitragerId)
      )
      for (const l of logs) {
        const timestamp = new timestamp_pb.Timestamp()
        const arbitragerLogTimestamp = new Timestamp()
        timestamp.setSeconds(l.created_time.timestamp.seconds)
        arbitragerLogTimestamp.setTimestamp(timestamp)
        const log = new ArbitragerLog()
        log.setArbitragerId(arbitragerId)
        log.setCreatedTime(arbitragerLogTimestamp)
        log.setLogType(l.log_type)
        log.setMessage(l.message)
        payload.push(log)
      }
    } catch (err) {
      console.log(err)
    }
    return payload
  }
  public async GetOrdersByArbitragerID(
    arbitragerId: number
  ): Promise<Array<ArbitrageOrder>> {
    const payload = Array<ArbitrageOrder>()
    try {
      // @ts-ignore
      const orders: any[] = await backend.arbitrageHandler.GetOrdersByArbitragerID(
        Number(arbitragerId)
      )
      for (const ao of orders) {
        const timestamp = new timestamp_pb.Timestamp()
        const arbitragerOrderTimestamp = new Timestamp()
        timestamp.setSeconds(ao.created_time.timestamp.seconds)
        arbitragerOrderTimestamp.setTimestamp(timestamp)
        const arbitragerOrder = new ArbitrageOrder()
        arbitragerOrder.setExchange(ao.exchange)
        arbitragerOrder.setTrading(ao.trading)
        arbitragerOrder.setSettlement(ao.settlement)
        arbitragerOrder.setPrice(ao.price)
        arbitragerOrder.setOrderId(ao.order_id)
        arbitragerOrder.setAmount(ao.amount)
        arbitragerOrder.setOrderType(ao.order_type)
        arbitragerOrder.setArbitragerId(ao.arbitrager_id)
        arbitragerOrder.setCreatedTime(arbitragerOrderTimestamp)
        payload.push(arbitragerOrder)
      }
    } catch (err) {
      console.log(err)
    }
    return payload
  }
  public async GetNetArbitragesByArbitragePair(): Promise<
    Array<NetArbitrageByArbitragePair>
  > {
    const payload = Array<NetArbitrageByArbitragePair>()

    // @ts-ignore
    const res: any[] = await backend.arbitrageHandler.GetNetArbitragesByArbitragePair()
    for (const r of res) {
      const netArbitrageByArbitragePair = new NetArbitrageByArbitragePair()
      const currencyPair = new CurrencyPair()
      currencyPair.setTrading(r.currency_pair.trading)
      currencyPair.setSettlement(r.currency_pair.settlement)
      netArbitrageByArbitragePair.setCurrencyPair(currencyPair)
      netArbitrageByArbitragePair.setExchangeA(r.exchange_a)
      netArbitrageByArbitragePair.setExchangeB(r.exchange_b)
      netArbitrageByArbitragePair.setOrderBookInverse(r.order_book_inverse)
      netArbitrageByArbitragePair.setNetArbitrage(r.net_arbitrage)
      payload.push(netArbitrageByArbitragePair)
    }
    return payload
  }
  public async GetArbitrageByArbitragePair(): Promise<
    Array<ArbitragePairArbitrage>
  > {
    const payload = Array<ArbitragePairArbitrage>()

    // @ts-ignore
    const arbitrageByArbitragePair: any[] = await backend.arbitrageHandler.GetArbitrageByArbitragePair()
    for (const a of arbitrageByArbitragePair) {
      const trading = a.order_book_tick_a.currency_pair.trading
      const settlement = a.order_book_tick_a.currency_pair.settlement
      const exchange_a = a.order_book_tick_a.exchange
      const exchange_b = a.order_book_tick_b.exchange
      const currencyPair = new CurrencyPair()
      currencyPair.setTrading(trading)
      currencyPair.setSettlement(settlement)
      const orderBookTickA: OrderBookTick = new OrderBookTick()
      orderBookTickA.setBestAskAmount(a.order_book_tick_a.best_ask_amount)
      orderBookTickA.setBestAskPrice(a.order_book_tick_a.best_ask_price)
      orderBookTickA.setBestBidAmount(a.order_book_tick_a.best_bid_amount)
      orderBookTickA.setBestBidPrice(a.order_book_tick_a.best_bid_price)
      orderBookTickA.setExchange(exchange_a)
      orderBookTickA.setCurrencyPair(currencyPair)
      const orderBookTickB: OrderBookTick = new OrderBookTick()
      orderBookTickB.setBestAskAmount(a.order_book_tick_b.best_ask_amount)
      orderBookTickB.setBestAskPrice(a.order_book_tick_b.best_ask_price)
      orderBookTickB.setBestBidAmount(a.order_book_tick_b.best_bid_amount)
      orderBookTickB.setBestBidPrice(a.order_book_tick_b.best_bid_price)
      orderBookTickB.setExchange(exchange_b)
      orderBookTickB.setCurrencyPair(currencyPair)
      const arbitragePairArbitrage = new ArbitragePairArbitrage()
      arbitragePairArbitrage.setOrderBookTickA(orderBookTickA)
      arbitragePairArbitrage.setOrderBookTickB(orderBookTickB)
      payload.push(arbitragePairArbitrage)
    }
    return payload
  }
  public async GetArbitragerSetting(id: number): Promise<ArbitragerSetting> {
    // @ts-ignore
    const settingAny: any = await backend.arbitrageHandler.GetArbitragerSetting(
      id
    )
    const setting = new ArbitragerSetting()
    const currencyPair: CurrencyPair = new CurrencyPair()
    currencyPair.setTrading(settingAny.arbitrage_pair.currency_pair.trading)
    currencyPair.setSettlement(
      settingAny.arbitrage_pair.currency_pair.settlement
    )
    const arbitragePair: ArbitragePair = new ArbitragePair()
    arbitragePair.setExchangeA(settingAny.arbitrage_pair.exchange_a)
    arbitragePair.setExchangeB(settingAny.arbitrage_pair.exchange_b)
    arbitragePair.setCurrencyPair(currencyPair)
    setting.setArbitragePair(arbitragePair)
    setting.setArbitragerMode(settingAny.arbitrager_mode)
    setting.setArbitragerOperation(settingAny.arbitrager_operation)
    setting.setArbitragerState(settingAny.arbitrager_state)
    setting.setExpectedProfitRate(settingAny.expected_profit_rate)
    setting.setLossCutRate(settingAny.loss_cut_rate)
    setting.setAssetUseRate(settingAny.asset_use_rate)
    setting.setArbitrageWaitLimit(settingAny.arbitrage_wait_limit)
    return setting
  }

  public async GetBalances(): Promise<{
    [key: string]: { [key: string]: number }
  }> {
    let ret: { [key: string]: { [key: string]: number } } = {}
    // @ts-ignore
    const balancesMap = await backend.arbitrageHandler.GetBalances()
    return balancesMap
  }

  public async GetTransferFees(): Promise<{
    [key: string]: { [key: string]: number }
  }> {
    let ret: { [key: string]: { [key: string]: number } } = {}
    // @ts-ignore
    const transferFeeMap = await backend.arbitrageHandler.GetTransferFees()
    return transferFeeMap
  }

  public async GetArbitragePairs(): Promise<ArbitragePair[]> {
    // @ts-ignore
    const pairs: any[] = await backend.arbitrageHandler.GetPairs()

    const arbitragePairs: ArbitragePair[] = Array<ArbitragePair>()

    pairs.forEach((pair: any) => {
      const currencyPair = new CurrencyPair()
      currencyPair.setSettlement(pair.currency_pair.settlement)
      currencyPair.setTrading(pair.currency_pair.trading)
      const arbitragePair = new ArbitragePair()
      arbitragePair.setCurrencyPair(currencyPair)
      arbitragePair.setExchangeA(pair.exchange_a)
      arbitragePair.setExchangeB(pair.exchange_b)
      arbitragePairs.push(arbitragePair)
    })
    return arbitragePairs
  }
  public CreateArbitrager(setting: ArbitragerSetting): Promise<number> {
    const settingJson = JSON.stringify(setting.toObject())
    // @ts-ignore
    return backend.arbitrageHandler
      .CreateArbitrager(settingJson)
      .catch((error: any) => {
        console.log(error)
      })
  }
  public StartArbitrager(id: number): void {
    // @ts-ignore
    backend.arbitrageHandler.StartArbitrager(id).catch((error: any) => {
      console.log(error)
    })
  }
  public StopArbitrager(id: number): void {
    // @ts-ignore
    backend.arbitrageHandler.StopArbitrager(id).catch((error: any) => {
      console.log(error)
    })
  }
  public DeleteArbitrager(id: number): void {
    // @ts-ignore
    backend.arbitrageHandler.DeleteArbitrager(id).catch((error: any) => {
      console.log(error)
    })
  }
}
export const ArbitragerRepository = new arbitragerRepository()
