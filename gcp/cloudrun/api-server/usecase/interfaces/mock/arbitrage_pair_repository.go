// Code generated by MockGen. DO NOT EDIT.
// Source: usecase/interfaces/arbitrage_pair_repository.go

// Package mock_interfaces is a generated GoMock package.
package mock_interfaces

import (
	context "context"
	gomock "github.com/golang/mock/gomock"
	model "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	reflect "reflect"
)

// MockArbitragePairRepository is a mock of ArbitragePairRepository interface
type MockArbitragePairRepository struct {
	ctrl     *gomock.Controller
	recorder *MockArbitragePairRepositoryMockRecorder
}

// MockArbitragePairRepositoryMockRecorder is the mock recorder for MockArbitragePairRepository
type MockArbitragePairRepositoryMockRecorder struct {
	mock *MockArbitragePairRepository
}

// NewMockArbitragePairRepository creates a new mock instance
func NewMockArbitragePairRepository(ctrl *gomock.Controller) *MockArbitragePairRepository {
	mock := &MockArbitragePairRepository{ctrl: ctrl}
	mock.recorder = &MockArbitragePairRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockArbitragePairRepository) EXPECT() *MockArbitragePairRepositoryMockRecorder {
	return m.recorder
}

// GetAll mocks base method
func (m *MockArbitragePairRepository) GetAll(ctx context.Context) (model.ArbitragePairs, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAll", ctx)
	ret0, _ := ret[0].(model.ArbitragePairs)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAll indicates an expected call of GetAll
func (mr *MockArbitragePairRepositoryMockRecorder) GetAll(ctx interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAll", reflect.TypeOf((*MockArbitragePairRepository)(nil).GetAll), ctx)
}
