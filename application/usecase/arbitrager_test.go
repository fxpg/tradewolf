package usecase

import (
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/domain/service"
	mock_service "gitlab.com/fxpg/tradewolf/domain/service/mock"
	"gitlab.com/fxpg/tradewolf/infrastructure/api"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
	"gitlab.com/fxpg/tradewolf/infrastructure/config"
	"go.uber.org/dig"
)

func NewPublicRepositoryMock(t *testing.T, calls ...*gomock.Call) service.PublicResourceRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockPublicResourceRepository(ctrl)
	bidsKucoin := []models.BoardBar{
		models.BoardBar{
			Price:  100,
			Amount: 10,
		}, models.BoardBar{
			Price:  110,
			Amount: 10,
		}, models.BoardBar{
			Price:  120,
			Amount: 10,
		},
	}
	asksKucoin := []models.BoardBar{
		models.BoardBar{
			Price:  130,
			Amount: 10,
		}, models.BoardBar{
			Price:  140,
			Amount: 10,
		}, models.BoardBar{
			Price:  150,
			Amount: 10,
		},
	}
	boardKucoin := &models.Board{
		Bids: bidsKucoin,
		Asks: asksKucoin,
	}
	bidsBinance := []models.BoardBar{
		models.BoardBar{
			Price:  125,
			Amount: 10,
		}, models.BoardBar{
			Price:  135,
			Amount: 10,
		}, models.BoardBar{
			Price:  145,
			Amount: 10,
		},
	}
	asksBinance := []models.BoardBar{
		models.BoardBar{
			Price:  155,
			Amount: 10,
		}, models.BoardBar{
			Price:  165,
			Amount: 10,
		}, models.BoardBar{
			Price:  175,
			Amount: 10,
		},
	}
	boardBinance := &models.Board{
		Bids: bidsBinance,
		Asks: asksBinance,
	}
	m.EXPECT().Board("kucoin", gomock.Any(), gomock.Any()).Return(boardKucoin, nil).AnyTimes()
	m.EXPECT().Board("binance", gomock.Any(), gomock.Any()).Return(boardBinance, nil).AnyTimes()
	currencyPairs := []models.CurrencyPair{
		NewCurrencyPairModelMock("ETH", "BTC"),
		NewCurrencyPairModelMock("IOTA", "BTC"),
		NewCurrencyPairModelMock("XRP", "BTC"),
		NewCurrencyPairModelMock("LTC", "BTC"),
		NewCurrencyPairModelMock("DCN", "BTC"),
	}
	m.EXPECT().CurrencyPairs(gomock.Any()).Return(currencyPairs, nil).AnyTimes()
	m.EXPECT().FrozenCurrency(gomock.Any()).Return([]string{"IOTA"}, nil).AnyTimes()
	return m
}

func NewPrivateRepositoryMock(t *testing.T) service.PrivateResourceRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockPrivateResourceRepository(ctrl)
	m.EXPECT().Order(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return("2000", nil).AnyTimes()
	m.EXPECT().IsOrderFilled("kucoin", gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil).AnyTimes()
	m.EXPECT().IsOrderFilled("binance", gomock.Any(), gomock.Any(), gomock.Any()).Return(true, nil).AnyTimes()
	return m
}
func NewSettingRepositoryMock(t *testing.T) service.SettingRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockSettingRepository(ctrl)
	m.EXPECT().GetApiKey(gomock.Any()).Return("TEST_API_KEY", nil).AnyTimes()
	m.EXPECT().GetSecKey(gomock.Any()).Return("TEST_API_KEY", nil).AnyTimes()
	return m
}
func NewArbitrageOrderRepositoryMock(t *testing.T) service.ArbitrageOrderRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockArbitrageOrderRepository(ctrl)
	return m
}
func TestNewArbitragerUsecase(t *testing.T) {
	c := dig.New()

	if err := c.Provide(func() *config.Config {
		conf := config.ReadConfig("./config.yml")
		return conf
	}); err != nil {
		t.Error(err)
	}
	if err := c.Provide(api.NewPublicResourceRepository); err != nil {
		t.Error(err)
		t.Fatalf("dig error")
	}
	if err := c.Provide(api.NewPrivateResourceRepository); err != nil {
		t.Error(err)
		t.Fatalf("dig error")
	}
}

func NewCurrencyPairModelMock(trading, settlement string) models.CurrencyPair {
	return models.CurrencyPair{
		Trading:    trading,
		Settlement: settlement,
	}
}

func NewCurrencyPairMock(trading, settlement string) *entity.CurrencyPair {
	return &entity.CurrencyPair{
		Trading:    trading,
		Settlement: settlement,
	}
}

func NewArbitragePairMock() *entity.ArbitragePair {
	return &entity.ArbitragePair{
		ExchangeA:    "kucoin",
		ExchangeB:    "binance",
		CurrencyPair: NewCurrencyPairMock("ETH", "BTC"),
	}
}
func NewArbitragerLogRepositoryMockForTest(t *testing.T) service.ArbitragerLogRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockArbitragerLogRepository(ctrl)
	m.EXPECT().Insert(gomock.Any()).Return(nil).AnyTimes()
	return m
}

func NewArbitragerSettingRepositoryMockForTest(t *testing.T) service.ArbitragerSettingRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_service.NewMockArbitragerSettingRepository(ctrl)
	m.EXPECT().LastId().Return(1, nil).AnyTimes()
	m.EXPECT().Insert(gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	m.EXPECT().Delete(gomock.Any()).Return(nil).AnyTimes()
	m.EXPECT().FindIds().Return([]int{1}, nil).AnyTimes()
	return m
}

func NewArbitragerSettingMock(arbitragerOperation entity.ArbitragerOperation) *entity.ArbitragerSetting {
	return &entity.ArbitragerSetting{
		ArbitragePair:       NewArbitragePairMock(),
		ArbitragerMode:      entity.ArbitragerMode_TEST,
		ArbitragerOperation: arbitragerOperation,
		ArbitragerState:     entity.ArbitragerState_STOPPED,
		ExpectedProfitRate:  0.05,
		LossCutRate:         0.1,
		ArbitrageWaitLimit:  600,
	}
}

func TestArbitrageUsecase_CreateArbitrager(t *testing.T) {
	usecase := NewArbitrageUsecase(NewPublicRepositoryMock(t), NewPrivateRepositoryMock(t), NewSettingRepositoryMock(t), NewArbitrageOrderRepositoryMock(t), NewArbitragerLogRepositoryMockForTest(t), NewArbitragerSettingRepositoryMockForTest(t), &config.Config{
		Exchanges: []string{"kucoin", "binance"},
	})
	id, err := usecase.CreateArbitrager(NewArbitragerSettingMock(entity.ArbitragerOperation_LONG_SIDE), func(entity.ArbitragerLog) {})
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if id != 2 {
		t.Fatalf("failed test %#v", id)
	}
	id, err = usecase.CreateArbitrager(NewArbitragerSettingMock(entity.ArbitragerOperation_LONG_SIDE), func(entity.ArbitragerLog) {})
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if id != 2 {
		t.Fatalf("failed test %#v", id)
	}
	done := make(chan error, 0)
	go func() {
		err = usecase.StartArbitrager(id)
		done <- err
	}()
	time.Sleep(time.Second * 1)
	err = usecase.StopArbitrager(id)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	err = <-done
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	done = make(chan error, 0)
	go func() {
		err = usecase.StartArbitrager(id)
		done <- err
	}()
	time.Sleep(time.Second * 1)
	ab, err := usecase.GetBoards(id)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if len(ab) != 2 {
		t.Fatalf("failed test %#v", ab)
	}
	err = usecase.DeleteArbitrager(id)
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	ids, err := usecase.GetArbitragerIDs()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if len(ids) == 0 {
		t.Fatalf("failed test %#v", ids)
	}
	pairs, err := usecase.GetPairs()
	if err != nil {
		t.Fatalf("failed test %#v", err)
	}
	if len(pairs) == 0 {
		t.Fatalf("failed test %#v", pairs)
	}
}
