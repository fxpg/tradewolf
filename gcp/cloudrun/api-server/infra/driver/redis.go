package driver

import (
	"context"
	"fmt"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"log"

	"github.com/go-redis/redis"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/config"
)

func ConnectToRedis(ctx context.Context, c *config.Config) (*redis.Client, error) {
	addr := fmt.Sprintf("%s:%d", c.RedisConfig.Host, c.RedisConfig.Port)
	cli := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: c.RedisConfig.Password,
		DB:       c.RedisConfig.DB,
	})

	_, err := cli.WithContext(ctx).Ping().Result()
	if err != nil {
		return nil, qerror.WrapWith(err, model.ErrSystemRedisConnectionError)
	}
	log.Printf("connected to redis: %s", addr)

	return cli, nil
}
