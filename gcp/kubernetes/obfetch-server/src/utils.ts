import { CreateContextOptions } from "vm"
import ccxt from 'ccxt'
import ccxws from 'ccxws'


export function IsExchange(item: any): item is Exchange {
    return (exchangeList as readonly string[]).indexOf(item) >= 0
  }
const _exchangeList = ["ftx", "bibox", "binance" , "bitfinex" , "bitflyer" , "bitmex" , "bitstamp" , "bittrex" ,/* "cexio" ,*/ "coinbase" , "coinex" , "ethfinex" , "gateio" , "gemini" , "hitbtc" , "huobi" ,"liquid", "kucoin" , "okex" , "poloniex" , "upbit" , "zb"] as const
const exchangeList = ["ftx", "bibox", "binance" , "bitfinex" , "bitflyer" , "bitmex" , "bitstamp" , "bittrex" ,/* "cexio" ,*/ "coinbase" , "coinex" , "ethfinex" , "gateio" , "gemini" , "hitbtc" , "huobi" ,"liquid", "kucoin" , "okex" , "poloniex" , "upbit" , "zb"] as const
export type Exchange = typeof exchangeList[number]
export namespace Exchange {
  export const Bibox:Exchange = "bibox"
  export const Binance:Exchange = "binance"
  export const Bitfinex:Exchange = "bitfinex"
  export const Bitflyer:Exchange = "bitflyer"
  export const Bitmex:Exchange = "bitmex"
  export const Bitstamp:Exchange = "bitstamp"
  export const Bittrex:Exchange = "bittrex"
  // export const Cexio:Exchange = "cexio"
  export const Coinbase:Exchange = "coinbase"
  export const Coinex:Exchange = "coinex"
  export const Ethfinex:Exchange = "ethfinex"
  export const Ftx:Exchange = "ftx"
  export const Gateio:Exchange = "gateio"
  export const Gemini:Exchange = "gemini"
  export const Liquid:Exchange = "liquid"
  export const Hitbtc:Exchange = "hitbtc"
  export const Huobi:Exchange = "huobi"
  export const Kucoin:Exchange = "kucoin"
  export const Okex:Exchange = "okex"
  export const Poloniex:Exchange = "poloniex"
  export const Upbit:Exchange = "upbit"
  export const Zb:Exchange = "zb"
}

export const GetCcxwsClientConstructor = function (exchange: Exchange): any {    
    if(exchange == Exchange.Bitfinex) {
        return ccxws.bitfinex
        // super(exchange, new ccxt.bitfinex(), ccxws.bitfinex)
    }
    if(exchange == Exchange.Bitflyer) {
        return ccxws.bitflyer
        // super(exchange, new ccxt.bitflyer(), ccxws.bitflyer)
    }
    if(exchange == Exchange.Bitmex) {      
        return ccxws.bitmex
        // super(exchange, new ccxt.bitmex(), ccxws.bitmex)
    }
    if(exchange == Exchange.Bittrex) {      
        return ccxws.bittrex
        // super(exchange, new ccxt.bittrex(), ccxws.bittrex)
    }
    /*
    if(exchange == Exchange.Cexio) {
        return ccxws.cex
        // super(exchange, new ccxt.cex(), ccxws.cex)
    }
    */
    if(exchange == Exchange.Coinbase) {
        return ccxws.coinbase
        // super(exchange, new ccxt.coinbasepro(), ccxws.coinbasepro)
    }
    if(exchange == Exchange.Coinex) {
        return ccxws.coinex
        // super(exchange, new ccxt.coinex(), ccxws.coinex)
    }
    if(exchange == Exchange.Gateio) {
        return ccxws.gateio
        // super(exchange, new ccxt.gateio(), ccxws.gateio)
    }
    if(exchange == Exchange.Gemini) {
        return ccxws.gemini
        // super(exchange, new ccxt.gemini(),  ccxws.gemini)
    }
    if(exchange == Exchange.Hitbtc) {
        return ccxws.hitbtc
        // super(exchange, new ccxt.hitbtc(), ccxws.hitbtc)
    }
    if(exchange == Exchange.Kucoin) {
        return ccxws.kucoin
        // super(exchange, new ccxt.kucoin(), ccxws.kucoin)
    }
    if(exchange == Exchange.Poloniex) {
        return ccxws.poloniex
        // super(exchange, new ccxt.poloniex(), ccxws.poloniex)
    }
    if(exchange == Exchange.Liquid) {
        return ccxws.liquid
        // super(exchange, new ccxt.liquid(), ccxws.liquid)
    }
    if(exchange == Exchange.Bibox){
        return ccxws.bibox
        // super(exchange, new ccxt.bibox(), ccxws.bibox)
    }
    if(exchange == Exchange.Binance){
        return ccxws.binance
        // super(exchange, new ccxt.binance(), ccxws.binance)
    }
    if(exchange == Exchange.Bitstamp){
        return ccxws.bitstamp
        // super(exchange, new ccxt.bitstamp(), ccxws.bitstamp)
    }
    if(exchange == Exchange.Huobi){
        return ccxws.huobipro
        // super(exchange, new ccxt.huobipro(), ccxws.huobipro)
    }
    if(exchange == Exchange.Okex){
        return ccxws.okex
        // super(exchange, new ccxt.okex(), ccxws.okex)
    }
    if(exchange == Exchange.Upbit){
        return ccxws.upbit
        // super(exchange, new ccxt.upbit(), ccxws.upbit)
    }
    if(exchange == Exchange.Zb){
        return ccxws.zb
        // super(exchange, new ccxt.zb(), ccxws.zb)
    }
    if(exchange == Exchange.Ftx){
        return new ccxws.ftx()
        // super(exchange, new ccxt.zb(), ccxws.zb)
    }
    throw Error(`there is no exchange: ${exchange}`)
}

export const GetCcxtClient = function (exchange: Exchange): ccxt.Exchange|undefined {    
    if(exchange == Exchange.Bitfinex) {
        return new ccxt.bitfinex2()
        // super(exchange, new ccxt.bitfinex(), ccxws.bitfinex)
    }
    if(exchange == Exchange.Bitflyer) {
        return new ccxt.bitflyer()
        // super(exchange, new ccxt.bitflyer(), ccxws.bitflyer)
    }
    /* if(exchange == Exchange.Bitmex) {      
        return new ccxt.bitmex()
        // super(exchange, new ccxt.bitmex(), ccxws.bitmex)
    } */
    if(exchange == Exchange.Bittrex) {      
        return new ccxt.bittrex()
        // super(exchange, new ccxt.bittrex(), ccxws.bittrex)
    }
    /*
    if(exchange == Exchange.Cexio) {
        return new ccxt.cex()
        // super(exchange, new ccxt.cex(), ccxws.cex)
    }
    */
    if(exchange == Exchange.Coinbase) {
        return new ccxt.coinbase()
        // super(exchange, new ccxt.coinbasepro(), ccxws.coinbasepro)
    }
    if(exchange == Exchange.Coinex) {
        return new ccxt.coinex()
        // super(exchange, new ccxt.coinex(), ccxws.coinex)
    }
    if(exchange == Exchange.Gateio) {
        return new ccxt.gateio()
        // super(exchange, new ccxt.gateio(), ccxws.gateio)
    }
    if(exchange == Exchange.Gemini) {
        return new ccxt.gemini()
        // super(exchange, new ccxt.gemini(),  ccxws.gemini)
    }
    if(exchange == Exchange.Hitbtc) {
        return new ccxt.hitbtc()
        // super(exchange, new ccxt.hitbtc(), ccxws.hitbtc)
    }
    if(exchange == Exchange.Kucoin) {
        return new ccxt.kucoin()
        // super(exchange, new ccxt.kucoin(), ccxws.kucoin)
    }
    if(exchange == Exchange.Poloniex) {
        return new ccxt.poloniex()
        // super(exchange, new ccxt.poloniex(), ccxws.poloniex)
    }
    if(exchange == Exchange.Liquid) {
        return new ccxt.liquid()
        // super(exchange, new ccxt.liquid(), ccxws.liquid)
    }
    if(exchange == Exchange.Binance){
        return new ccxt.binance()
        // super(exchange, new ccxt.binance(), ccxws.binance)
    }
    if(exchange == Exchange.Bitstamp){
        return new ccxt.bitstamp()
        // super(exchange, new ccxt.bitstamp(), ccxws.bitstamp)
    }
    if(exchange == Exchange.Huobi){
        return new ccxt.huobipro()
        // super(exchange, new ccxt.huobipro(), ccxws.huobipro)
    }
    if(exchange == Exchange.Okex){
        return new ccxt.okex()
        // super(exchange, new ccxt.okex(), ccxws.okex)
    }
    if(exchange == Exchange.Upbit){
        return new ccxt.upbit()
        // super(exchange, new ccxt.upbit(), ccxws.upbit)
    }
    if(exchange == Exchange.Zb){
        return new ccxt.zb()
        // super(exchange, new ccxt.zb(), ccxws.zb)
    }
    if(exchange == Exchange.Ftx){
        return new ccxt.ftx()
        // super(exchange, new ccxt.zb(), ccxws.zb)
    }
    return undefined
}

const tryToFetchMarkets= async function (cli: ccxt.Exchange):Promise<boolean> {
    try {
        await cli.loadMarkets()
        return true
    } catch(e) {        
      if(e instanceof ccxt.NetworkError) {
        return false
      } else {
        throw e
      }   
    }
}


export const GetArbitragableMarkets = async function (exchange: Exchange): Promise<ccxt.Market[]> {
    const arbitragableMarkets: ccxt.Market[] = []

    const cli = GetCcxtClient(exchange)
    const targetExchanges = [Exchange.Binance, Exchange.Bitstamp, Exchange.Hitbtc, Exchange.Binance, Exchange.Poloniex, Exchange.Kucoin, Exchange.Bittrex]
    const elseClis = targetExchanges.filter(ex => ex !== exchange).filter((ex)=> {
        const cli = GetCcxtClient(ex)
        if (cli!==undefined) return true
        return false
    }).map((ex)=>{
        return GetCcxtClient(ex) as ccxt.Exchange
    })
    if(cli === undefined) {
        throw Error(`no exchange ${exchange}`)
    }
    await cli.loadMarkets()
    
    for (let elseCli of elseClis) {
        while(true) {
            try {
                const isLoadComplete = await tryToFetchMarkets(elseCli)
                if(isLoadComplete) {
                    break
                }
                console.log(`retry load on ${elseCli.name}`)
            } catch(e) {
                throw e
            }
        }
        for(let marketAKey in cli.markets) {
            for(let marketBKey in elseCli.markets) {
                if((cli.markets[marketAKey].base === elseCli.markets[marketBKey].base) &&
                (cli.markets[marketAKey].quote === elseCli.markets[marketBKey].quote)) {
                    arbitragableMarkets.push(cli.markets[marketAKey])
                }
            }
        }
    }
    console.log(arbitragableMarkets.length)
    const ret = arbitragableMarkets.filter((x,i,self)=>{
        return self.indexOf(x) === i
    })
    console.log(ret.length)
    return ret
}