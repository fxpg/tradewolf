package registry

import (
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/controller"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/infrastructure"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/usecase"
	"go.uber.org/dig"
	"time"
)

func New() (*dig.Container, error) {
	c := dig.New()
	// WatchCondition
	if err := c.Provide(func() *model.ArbitrageWatchCondition {
		return &model.ArbitrageWatchCondition{
			ExpectedProfitRatio:  0.01,
			ExpectedLossCutRatio: 0.01,
			Duration:             time.Minute * 30,
		}
	}); err != nil {
		return nil, err
	}
	// Exchange List
	c.Provide(func() []string {
		return []string{"hitbtc", "binance", "poloniex"}
	})
	// Repositories
	if err := c.Provide(infrastructure.NewArbitrageDataRepositoryClient); err != nil {
		return nil, errors.WithStack(err)
	}
	// UseCases
	if err := c.Provide(usecase.NewArbitrageCalculatorUsecaseImpl); err != nil {
		return nil, errors.WithStack(err)
	}
	// Managers
	if err := c.Provide(controller.NewArbitrageWatchManager); err != nil {
		return nil, errors.WithStack(err)
	}
	// Controllers
	if err := c.Provide(controller.NewGuiController); err != nil {
		return nil, errors.WithStack(err)
	}

	return c, nil
}
