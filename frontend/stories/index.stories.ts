import { storiesOf } from '@storybook/vue'
import {
  NewMockArbitragePairArbitrage,
  NewMockArbitragePairs,
  NewMockBoard,
  NewMockCurrencyPairs,
  NewMockLogs
} from '../src/models/mocks'
storiesOf('Arbitrager', module).add('Dashboard', () => ({
  template: `
  <v-container id="arbitrager" class="mt-0 pt-0" fluid tag="section">
    <v-row class="text-center mt-0">
    <v-col cols="12" class="pa-0">
        <v-layout row wrap>
          <v-flex class="align-start">
            <v-btn class="font-weight-thin" tile text small>ホーム</v-btn>
            <v-btn class="font-weight-regular" tile text small>Arbitrager</v-btn>
            <v-btn class="font-weight-regular" tile text small>Trader</v-btn>
            <v-btn class="font-weight-thin" tile text small>取引所</v-btn>
            <v-btn class="font-weight-thin" tile text small>設定</v-btn>
          </v-flex>
          <v-flex>
            <span class="caption">arbitrager tool - TRADEWOLF</span>
          </v-flex>
        </v-layout>
      </v-col>
      <v-col cols="12" class="pa-0">
        <arbitrager-pair-info :arbitragePair=arbitragePair></arbitrager-pair-info>
      </v-col>
      <v-col cols="auto" md="3" sm="5" class="pl-1 pr-0">
        <v-card width="100%" height="fit-content" color="transparent" class="mt-0 pt-0">
          <arbitrager-pairs :arbitragesByArbitragePair=arbitragesByArbitragePair></arbitrager-pairs>
        </v-card>
      </v-col>
      <v-col cols="auto" md="6" sm="8" class="pr-1 pl-1">
        <v-row no-gutters>
          <v-col cols="6">
            <v-card width="100%" height="fit-content" class="ma-0 pr-1" color="transparent">
              <div>
                <arbitrager-board-bars :boardType="'asks'":boardBars=askBoardBarsA></arbitrager-board-bars>
              </div>
              <div>
                <arbitrager-board-bars :boardType="'bids'":boardBars=bidBoardBarsA></arbitrager-board-bars>
              </div>
            </v-card>
          </v-col>
          <v-col cols="6">
            <v-card width="100%" height="fit-content" class="ma-0 pr-1" color="transparent">
              <div>
                <arbitrager-board-bars :boardType="'asks'":boardBars=askBoardBarsB></arbitrager-board-bars>
              </div>
              <div>
                <arbitrager-board-bars :boardType="'bids'":boardBars=bidBoardBarsB></arbitrager-board-bars>
              </div>
            </v-card>
          </v-col>
        </v-row>
      </v-col>
      <v-col cols="auto" md="3" sm="4" class="pr-1 pl-0 pb-1">
        <arbitrager-setting :arbitragePair=arbitragePair></arbitrager-setting>
      </v-col>
      <v-col cols="12" class="pr-1 pl-1 pb-0 pt-0">
        <v-card width="100vw" height="fit-content">
          <arbitrager-log-board :logs="logs"></arbitrager-log-board>
        </v-card>
      </v-col>
    </v-row>
  </v-container>`,
  data() {
    const currncyPairs = NewMockCurrencyPairs()
    const boardA = NewMockBoard('kucoin', currncyPairs[0])
    const boardB = NewMockBoard('binance', currncyPairs[0])
    return {
      askBoardBarsA: boardA.getAsksList(),
      bidBoardBarsA: boardA.getBidsList(),
      askBoardBarsB: boardB.getAsksList(),
      bidBoardBarsB: boardB.getBidsList(),
      arbitragesByArbitragePair: NewMockArbitragePairArbitrage(),
      arbitragePair: NewMockArbitragePairs()[0],
      logs: NewMockLogs(1)
    }
  }
}))

storiesOf('Arbitrager', module).add('OrderBook', () => ({
  template: `
  <v-card width="50vw" height="300px">
    <arbitrager-board-bars :boardType="'asks'":boardBars=askBoardBars></arbitrager-board-bars>
    <arbitrager-board-bars :boardType="'bids'":boardBars=bidBoardBars></arbitrager-board-bars>
  </v-card>
  `,
  data() {
    const currncyPairs = NewMockCurrencyPairs()
    const board = NewMockBoard('kucoin', currncyPairs[0])
    return {
      askBoardBars: board.getAsksList(),
      bidBoardBars: board.getBidsList()
    }
  }
}))

storiesOf('Arbitrager', module).add('Pairs', () => ({
  template: `
  <v-card width="fit-content" height="fit-content">
    <arbitrager-pairs :arbitragesByArbitragePair=arbitragesByArbitragePair></arbitrager-pairs>
  </v-card>
  `,
  data() {
    return {
      arbitragesByArbitragePair: NewMockArbitragePairArbitrage()
    }
  }
}))

storiesOf('Arbitrager', module).add('Setting', () => ({
  template: `
  <v-card width="50vw" height="400px">
    <arbitrager-setting :arbitragePair=arbitragePair></arbitrager-setting>
  </v-card>
  `,
  data() {
    return {
      arbitragePair: NewMockArbitragePairs()[0]
    }
  }
}))

storiesOf('Arbitrager', module).add('Logs', () => ({
  template: `
  <v-card width="100vw" height="400px">
    <arbitrager-log-board :logs="logs"></arbitrager-log-board>
  </v-card>
  `,
  data() {
    const logs = NewMockLogs(1)
    return {
      logs: logs
    }
  }
}))
