import { ArbitragerSetting } from '@/models/domain_pb'

export function unixtimeToDateString(item: string): string {
  const dateTime = new Date(Number(item) * 1000)
  const year = dateTime.getFullYear()
  const month = dateTime.getMonth() + 1
  const day = dateTime.getDate()
  const hour = ('0' + dateTime.getHours()).slice(-2)
  var min = ('0' + dateTime.getMinutes()).slice(-2)
  var sec = ('0' + dateTime.getSeconds()).slice(-2)
  return year + '/' + month + '/' + day + ' ' + hour + ':' + min + ':' + sec
}

export function arbitragerMode(item: ArbitragerSetting) {
  if (item.getArbitragerMode() == 0) {
    return 'テストモード'
  }
  return '本番モード'
}
export function exchangePair(item: ArbitragerSetting) {
  return (
    item.getArbitragePair()!.getExchangeA() +
    ' - ' +
    item.getArbitragePair()!.getExchangeB()
  )
}
export function arbitragerOperation(item: ArbitragerSetting) {
  if (item.getArbitragerOperation() == 0) {
    return '買い建のみ'
  } else if (item.getArbitragerOperation() == 1) {
    return '売り建のみ'
  }
  return '売り買い建て'
}
export function arbitragerState(item: ArbitragerSetting) {
  if (item.getArbitragerState() == 0) {
    return '作成中'
  } else if (item.getArbitragerState() == 1) {
    return '停止中'
  } else if (item.getArbitragerState() == 2) {
    return '実行中'
  } else if (item.getArbitragerState() == 3) {
    return '削除中'
  }
  return ''
}
export function arbitragerStateIcon(item: ArbitragerSetting) {
  if (item.getArbitragerState() == 0) {
    return 'mdi-tools'
  } else if (item.getArbitragerState() == 1) {
    return 'mdi-pause'
  } else if (item.getArbitragerState() == 2) {
    return 'mdi-play'
  } else if (item.getArbitragerState() == 3) {
    return 'mdi-minus'
  }
  return ''
}
export function currencyPair(item: ArbitragerSetting) {
  return (
    item
      .getArbitragePair()!
      .getCurrencyPair()!
      .getTrading() +
    '/' +
    item
      .getArbitragePair()!
      .getCurrencyPair()!
      .getSettlement()
  )
}
