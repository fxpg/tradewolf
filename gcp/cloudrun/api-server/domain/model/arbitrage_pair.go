package model

// 裁定機会を取りうるペア
// A, Bの順序に意味はない
type ArbitragePair struct {
	ExchangeA string
	ExchangeB string

	CurrencyPair CurrencyPair
}

type ArbitragePairs []ArbitragePair

func (ap ArbitragePairs) GetCurrencyPairsByExchange(exchange string) []CurrencyPair {
	cps := make([]CurrencyPair, 0)

	for _, v := range ap {
		if v.ExchangeA == exchange || v.ExchangeB == exchange {
			cps = append(cps, v.CurrencyPair)
		}
	}

	// uniq ret
	uniqCps := make([]CurrencyPair, 0)
	m := make(map[string]bool)
	for _, v := range cps {
		vid := v.Trading + "_" + v.Settlement
		if _, ok := m[vid]; !ok {
			m[vid] = true
			uniqCps = append(uniqCps, v)
		}
	}

	return uniqCps
}