package mapper

import (
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/models"
)

func MapBoardBar(src model.BoardBar) *models.BoardBar {
	return &models.BoardBar{
		Amount: src.Amount,
		Price:  src.Price,
	}
}

func MapBoardBars(src []model.BoardBar) []*models.BoardBar {
	dst := make([]*models.BoardBar, 0, len(src))
	for _, item := range src {
		dst = append(dst, MapBoardBar(item))
	}
	return dst
}

func MapOrderBook(src *model.OrderBook) *models.OrderBook {
	return &models.OrderBook{
		Asks:       MapBoardBars(src.Asks),
		Bids:       MapBoardBars(src.Bids),
		Settlement: src.Settlement,
		Trading:    src.Trading,
	}
}

func MapOrderBooks(src []*model.OrderBook) models.OrderBooks {
	dst := make([]*models.OrderBook, 0, len(src))
	for _, item := range src {
		dst = append(dst, MapOrderBook(item))
	}
	return dst
}

func MapArbitragePair(src model.ArbitragePair) *models.ArbitragePair {
	return &models.ArbitragePair{
		Exchangea:  src.ExchangeA,
		Exchangeb:  src.ExchangeB,
		Settlement: src.CurrencyPair.Settlement,
		Trading:    src.CurrencyPair.Trading,
	}
}

func MapArbitragePairs(src model.ArbitragePairs) models.ArbitragePairs {
	dst := make([]*models.ArbitragePair, 0, len(src))
	for _, item := range src {
		dst = append(dst, MapArbitragePair(item))
	}
	return dst
}

func MapWithdrawalFee(src model.WithdrawalFee) *models.WithdrawalFee {
	return &models.WithdrawalFee{
		Currency: src.Currency,
		Amount:   src.Amount,
	}
}

func MapWithdrawalFees(src model.WithdrawalFees) models.WithdrawalFees {
	dst := make([]*models.WithdrawalFee, 0, len(src))
	for _, item := range src {
		dst = append(dst, MapWithdrawalFee(item))
	}
	return dst
}