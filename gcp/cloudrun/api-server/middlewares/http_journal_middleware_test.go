package middlewares

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"testing"
)

type MockHttpRequestJournalWriter struct {
	captured *RequestInfo
}

func (m *MockHttpRequestJournalWriter) Write(req *RequestInfo) {
	m.captured = req
}

type MockHttpResponseJournalWriter struct {
	captured *ResponseInfo
}

func (m *MockHttpResponseJournalWriter) Write(res *ResponseInfo) {
	m.captured = res
}

type MockResponseWriter struct {
	header http.Header
}

func (m *MockResponseWriter) Header() http.Header {
	return m.header
}

func (m *MockResponseWriter) Write(bs []byte) (int, error) {
	return len(bs), nil
}

func (m *MockResponseWriter) WriteHeader(statusCode int) {
}

func TestNewHttpJournalMiddleware_CapturedAll(t *testing.T) {
	// prepare
	requestJournalWriter := &MockHttpRequestJournalWriter{}
	requestBodyCaptureContentTypes := []string{"application/json"}
	responseJournalWtiter := &MockHttpResponseJournalWriter{}
	responseBodyCaptureContentTypes := []string{"application/json"}
	reqStub := &http.Request{
		Method:           http.MethodPost,
		Header:           http.Header{"Content-Type": []string{"application/json"}},
		Body:             ioutil.NopCloser(bytes.NewBufferString("hogehoge")),
	}
	reqStub = reqStub.WithContext(context.Background())
	resStub := &http.Response{
	}
	responseWriterMock := &MockResponseWriter{header: make(http.Header)}
	called := false
	handlerMock := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Response = resStub
		called = true
		w.Write([]byte("fugafuga"))
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
	})
	requestIdMiddleware := NewRequestIdMiddleware()

	// execute
	mid, newErr := NewHttpJournalMiddleware(requestJournalWriter, responseJournalWtiter,
		requestBodyCaptureContentTypes, responseBodyCaptureContentTypes)
	requestIdMiddleware(mid(handlerMock)).ServeHTTP(responseWriterMock, reqStub)

	// assertion
	if newErr != nil {
		t.Errorf("failed to initialize middleware: %+v", newErr)
	}
	if !called {
		t.Error("handler is not called")
	}
	if requestJournalWriter.captured.Request == nil {
		t.Error("request is not captured")
	}
	if !bytes.Equal(requestJournalWriter.captured.RequestBody, []byte("hogehoge")) {
		t.Error("request body is not captured")
	}
	if requestJournalWriter.captured.RequestId == "" {
		t.Error("request id is not captured")
	}
	if responseJournalWtiter.captured.Request == nil {
		t.Error("request is not captured")
	}
	if responseJournalWtiter.captured.Response == nil {
		t.Error("response is not captured")
	}
	if !bytes.Equal(responseJournalWtiter.captured.ResponseBody, []byte("fugafuga")) {
		t.Error("response body is not captured")
	}
	if responseJournalWtiter.captured.RequestId == "" {
		t.Error("request id is not captured")
	}
	if responseJournalWtiter.captured.ResponseHeader.Get("Content-Type") != "application/json" {
		t.Error("response header is not captured")
	}
	if responseJournalWtiter.captured.StatusCode != 200 {
		t.Error("status code is not captured")
	}
	if responseJournalWtiter.captured.RequestTime <= 0 {
		t.Error("request time is not set")	}
}
