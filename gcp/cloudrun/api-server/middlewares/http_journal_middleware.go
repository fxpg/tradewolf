package middlewares

import (
	"bytes"
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"io/ioutil"
	"net/http"
	"regexp"
	"time"
)

type RequestInfo struct {
	RequestId string
	Request *http.Request
	RequestBody []byte
}

type HttpRequestJournalWriter interface {
	Write(req *RequestInfo)
}

type ResponseInfo struct {
	RequestId string
	Request *http.Request
	Response *http.Response
	RequestTime time.Duration

	ResponseBody []byte
	ResponseHeader http.Header
	StatusCode int
}

type HttpResponseJournalWriter interface {
	Write(res *ResponseInfo)
}

func NewHttpJournalMiddleware(
	reqW HttpRequestJournalWriter,
	resW HttpResponseJournalWriter,
	requestBodyCaptureContentTypes []string,
	responseBodyCaptureContentTypes []string,
) (Middleware, error) {
	reqBodyContentTypes := make([]*regexp.Regexp, 0)
	for _, contentType := range requestBodyCaptureContentTypes {
		ct, err := regexp.Compile(contentType)
		if err != nil {
			return nil, qerror.WrapWith(err, model.ErrSystemInvalidArgs)
		}
		reqBodyContentTypes = append(reqBodyContentTypes, ct)
	}
	resBodyContentTypes := make([]*regexp.Regexp, 0)
	for _, contentType := range responseBodyCaptureContentTypes {
		ct, err := regexp.Compile(contentType)
		if err != nil {
			return nil, qerror.WrapWith(err, model.ErrSystemInvalidArgs)
		}
		resBodyContentTypes = append(resBodyContentTypes, ct)
	}

	return func(handler http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			startTime := time.Now()

			// Write to request journal
			requestId := GetRequestIdFromContext(r.Context())
			{
				contentType := r.Header.Get("Content-Type")
				var requestBody []byte
				if regexpMatches(reqBodyContentTypes, contentType) {
					requestBody = copyRequestBody(r)
				}
				requestInfo := &RequestInfo{
					RequestId:   requestId,
					Request:     r,
					RequestBody: requestBody,
				}
				reqW.Write(requestInfo)
			}

			// Ready to capture response body
			cw := NewResponseCaptureWriter(w)
			w = cw

			// call next middleware
			handler.ServeHTTP(w, r)

			// Write to response journal
			{
				elapsedTime := time.Now().Sub(startTime)
				contentType := cw.CapturedHeader().Get("Content-Type")
				var responseBody []byte
				if regexpMatches(resBodyContentTypes, contentType) {
					responseBody = cw.CapturedBody()
				}

				responseInfo := &ResponseInfo{
					RequestId:      requestId,
					Request:        r,
					Response:       r.Response,
					RequestTime:    elapsedTime,
					ResponseBody:   responseBody,
					ResponseHeader: cw.CapturedHeader(),
					StatusCode:     cw.CapturedStatusCode(),
				}
				resW.Write(responseInfo)
			}
		}
		return http.HandlerFunc(fn)
	}, nil
}

func copyRequestBody(r *http.Request) []byte {
	bs, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(qerror.WrapWith(err, model.ErrSystemUnexpected, "failed to read request body"))
	}
	r.Body = ioutil.NopCloser(bytes.NewBuffer(bs))
	return bs
}

func regexpMatches(regexs []*regexp.Regexp, str string) bool {
	for _, regex := range regexs {
		if regex.MatchString(str) {
			return true
		}
	}
	return false
}

/*
func NewHttpJournalMiddleware(reqW HttpRequestJournalWriter, resW HttpResponseJournalWriter) Middleware {
	return func(handler http.Handler) http.Handler {
		fn := func (w http.ResponseWriter, r *http.Request) {
			requestId := GetRequestIdFromContext(r.Context())
			startTime := time.Now()
			requestBody:= GetRequestBodyFromContext(r.Context())

			requestInfo := &RequestInfo{
				RequestId: requestId,
				Request:   r,
				RequestBody: requestBody,
			}
			reqW.Write(requestInfo)
			
			handler.ServeHTTP(w, r)

			requestTime := time.Now().Sub(startTime)
			// TODO: 直す
			//ResponseCaptureWriter := GetResponseCaptureWriterFromContext(r.Context())
			responseInfo := &ResponseInfo{
				RequestId: requestId,
				Request:   r,
				Response:  r.Response,
				RequestTime: requestTime,
				//ResponseBody: ResponseCaptureWriter.capturedBody,
			}
			resW.Write(responseInfo)
		}
		return http.HandlerFunc(fn)
	}
}
*/