package restapi

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/go-openapi/errors"
	loads "github.com/go-openapi/loads"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"github.com/golang/mock/gomock"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/config"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/controller"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/infra/driver"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/middlewares"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/restapi/operations"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	mock_interfaces "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces/mock"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils/journal"
	"go.uber.org/dig"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"
)

func getAPI() (*operations.TradewolfAPI, error) {
	swaggerSpec, err := loads.Analyzed(SwaggerJSON, "")
	if err != nil {
		return nil, err
	}
	api := operations.NewTradewolfAPI(swaggerSpec)
	return api, nil
}

func NewArbitragePairRepositoryMock(t *testing.T) interfaces.ArbitragePairRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_interfaces.NewMockArbitragePairRepository(ctrl)
	m.EXPECT().GetAll(gomock.Any()).Return(model.ArbitragePairs{model.ArbitragePair{
		ExchangeA: "kucoin",
		ExchangeB: "poloniex",
		CurrencyPair: model.CurrencyPair{
			Settlement: "BTC",
			Trading:    "ETH",
		},
	}, model.ArbitragePair{
		ExchangeA: "kucoin",
		ExchangeB: "poloniex",
		CurrencyPair: model.CurrencyPair{
			Settlement: "BTC",
			Trading:    "XRP",
		},
	}}, nil).AnyTimes()
	return m
}

func NewUserRepositoryMock(t *testing.T) interfaces.UserRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_interfaces.NewMockUserRepository(ctrl)
	m.EXPECT().GetUidByApiKey(gomock.Any(), gomock.Any()).Return("uiduiduid", nil).AnyTimes()
	m.EXPECT().GetAllowanceCallByMonth(gomock.Any(), gomock.Any()).Return(100000, nil).AnyTimes()
	m.EXPECT().GetAllowanceCallByDay(gomock.Any(), gomock.Any()).Return(100, nil).AnyTimes()
	return m
}

func NewCallCountRepositoryMock(t *testing.T) interfaces.CallCountRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_interfaces.NewMockCallCountRepository(ctrl)
	m.EXPECT().GetByMonth(gomock.Any(), gomock.Any()).Return(1000, nil).AnyTimes()
	m.EXPECT().GetByDay(gomock.Any(), gomock.Any()).Return(10, nil).AnyTimes()
	m.EXPECT().Increment(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	return m
}

func NewKeyPairRepositoryMock(t *testing.T) interfaces.KeyPairRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_interfaces.NewMockKeyPairRepository(ctrl)
	m.EXPECT().GetSecretKeyByApiKey(gomock.Any(), gomock.Any()).Return("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m", nil).AnyTimes()
	return m
}

func NewNonceRepositoryMock(t *testing.T) interfaces.NonceRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	timeOneHourAgo := time.Now().UTC().Add(time.Hour * -1)
	m := mock_interfaces.NewMockNonceRepository(ctrl)
	m.EXPECT().GetNonceByUid(gomock.Any(), gomock.Any()).Return(timeOneHourAgo, nil).AnyTimes()
	m.EXPECT().UpdateNonceByUid(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil).AnyTimes()
	return m
}

func NewOrderBookRepositoryMock(t *testing.T) interfaces.OrderBookRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	m := mock_interfaces.NewMockOrderBookRepository(ctrl)
	m.EXPECT().Get(gomock.Any(), gomock.Any(), gomock.Any()).Return(&model.OrderBook{
		CurrencyPair: model.CurrencyPair{Settlement: "BTC", Trading: "XRP"},
		Asks: []model.BoardBar{
			model.BoardBar{
				Price:  100,
				Amount: 10,
			}, model.BoardBar{
				Price:  110,
				Amount: 20,
			},
		},
		Bids: []model.BoardBar{
			model.BoardBar{
				Price:  90,
				Amount: 10,
			}, model.BoardBar{
				Price:  80,
				Amount: 20,
			},
		},
	}, nil).AnyTimes()
	return m
}

func configureAPIforTest(t *testing.T, api *operations.TradewolfAPI) http.Handler {
	c := dig.New()

	// 初期化タイムアウト
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	c.Provide(func() context.Context {
		return ctx
	})
	c.Provide(config.LoadConfig)
	c.Provide(driver.ConnectToRedis)
	c.Provide(driver.ConnectToFirestore)
	c.Provide(utils.NewStdLogger)
	for _, p := range usecase.Providers {
		c.Provide(p)
	}
	for _, p := range controller.Providers {
		c.Provide(p)
	}
	c.Provide(func() *testing.T { return t })
	c.Provide(NewArbitragePairRepositoryMock)
	c.Provide(NewCallCountRepositoryMock)
	c.Provide(NewKeyPairRepositoryMock)
	c.Provide(NewNonceRepositoryMock)
	c.Provide(NewOrderBookRepositoryMock)
	c.Provide(NewUserRepositoryMock)
	err := c.Invoke(func(
		in struct {
			dig.In
			ArbitragePairsController *controller.ArbitragePairsController
			OrderBooksController     *controller.OrderBooksController
			Logger                   utils.Logger
		},
	) {
		// configure the api here
		api.ServeError = func(w http.ResponseWriter, r *http.Request, err error) {
			in.Logger.Errorf(err.Error())
			errors.ServeError(w, r, err)
		}

		api.JSONConsumer = runtime.JSONConsumer()

		api.JSONProducer = runtime.JSONProducer()

		api.GetArbitragePairsHandler = operations.GetArbitragePairsHandlerFunc(in.ArbitragePairsController.Get)
		api.GetOrderBookByExchangeHandler = operations.GetOrderBookByExchangeHandlerFunc(in.OrderBooksController.Get)

		if api.GetOrderBookByExchangeHandler == nil {
			api.GetOrderBookByExchangeHandler = operations.GetOrderBookByExchangeHandlerFunc(func(params operations.GetOrderBookByExchangeParams) middleware.Responder {
				return middleware.NotImplemented("operation .GetOrderBookByExchange has not yet been implemented")
			})
		}
		if api.GetWithdrawalFeesByExchangeHandler == nil {
			api.GetWithdrawalFeesByExchangeHandler = operations.GetWithdrawalFeesByExchangeHandlerFunc(func(params operations.GetWithdrawalFeesByExchangeParams) middleware.Responder {
				return middleware.NotImplemented("operation .GetWithdrawalFeesByExchange has not yet been implemented")
			})
		}

		api.ServerShutdown = func() {}
	})
	if err != nil {
		t.Fatal(err)
	}

	// ジャーナルライター
	c.Invoke(func(cfg *config.Config) {
		switch cfg.Mode {
		case config.Production:
		case config.Debug:
			// TODO: Productionのログ出力先の変更(stackdriver?)
			c.Provide(journal.NewHttpRequestJournalLogWriter)
			c.Provide(journal.NewHttpResponseJournalLogWriter)
		default:
			panic("unknown mode")
		}
	})
	// Middlewareの登録
	c.Provide(middlewares.NewAuthenticateMiddleware, dig.Name("authenticateMiddleware"))
	c.Provide(middlewares.NewCheckCallAllowanceMiddleware, dig.Name("checkCallAllowanceMiddleware"))
	c.Provide(middlewares.NewUpdateNonceAuthenticateMiddleware, dig.Name("updateNonceMiddleware"))
	c.Provide(middlewares.NewDoNothingMiddleware, dig.Name("doNothingMiddleware"))
	c.Provide(func() middlewares.Middleware {
		return middlewares.NewRequestTimeoutMiddleware(5 * time.Second)
	}, dig.Name("requestTimeoutMiddleware"))
	c.Provide(middlewares.NewRequestIdMiddleware, dig.Name("requestIdMiddleware"))
	c.Provide(func(
		reqW middlewares.HttpRequestJournalWriter,
		resW middlewares.HttpResponseJournalWriter,
	) (middlewares.Middleware, error) {
		return middlewares.NewHttpJournalMiddleware(reqW, resW,
			[]string{"application/json"}, []string{"application/json"})
	}, dig.Name("httpJournalMiddleware"))
	var mid middleware.Builder
	err = c.Invoke(func(
		in struct {
			dig.In
			AuthenticateMiddleware       middlewares.Middleware `name:"authenticateMiddleware"`
			CheckCallAllowanceMiddleware middlewares.Middleware `name:"checkCallAllowanceMiddleware"`
			UpdateNonceMiddleware        middlewares.Middleware `name:"updateNonceMiddleware"`
			DoNothingMiddleware          middlewares.Middleware `name:"doNothingMiddleware"`
			RequestTimeoutMiddleware     middlewares.Middleware `name:"requestTimeoutMiddleware"`
			RequestIdMiddleware          middlewares.Middleware `name:"requestIdMiddleware"`
			HttpJournalMiddleware        middlewares.Middleware `name:"httpJournalMiddleware"`
		},
	) {
		// 上から順番に実行
		ms := []middlewares.Middleware{
			in.DoNothingMiddleware,
			in.RequestTimeoutMiddleware,
			in.RequestIdMiddleware,
			in.HttpJournalMiddleware,
			in.AuthenticateMiddleware,
			in.CheckCallAllowanceMiddleware,
			in.UpdateNonceMiddleware,
		}
		mid = createMiddleware(ms)
	})
	if err != nil {
		t.Fatal(err)
	}
	c.Invoke(func(c *config.Config, logger utils.Logger) {
		if c.Mode == config.Debug {
			// 開発時はプロファイラを起動
			go func() {
			}()
		}
	})
	return mid(api.Serve(emptyMiddleware))

}

func TestAuthenticateMiddleware_WithServer(t *testing.T) {
	api, err := getAPI()
	if err != nil {
		t.Fatal(err)
	}
	handler := configureAPI(api)
	err = api.Validate()
	if err != nil {
		t.Fatal(err)
	}
	ts := httptest.NewServer(handler)
	defer ts.Close()

	t.Run("[OK]X-API-KEY/X-API-SIGNATURE/X-API-TIMESTAMP", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().UnixNano()/ int64(time.Millisecond),10)
		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		signature := hex.EncodeToString(hm.Sum(nil))
		req.Header.Add("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Add("X-API-SIGNATURE", signature)
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		if res.StatusCode != 200 {
			t.Error(res.StatusCode)
			t.Error(res)
			b,_:= ioutil.ReadAll(res.Body)
			t.Error(string(b))
			t.Fatal("error")
		}
		errorRes, err:= parseErrResponse(res)
		if err == nil {
			t.Log(errorRes)
			t.Fatal("get api handler", err)
		}
		nonce = strconv.FormatInt(time.Now().UTC().UnixNano()/ int64(time.Millisecond),10)
		message = nonce + path + method + body
		hm=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		signature = hex.EncodeToString(hm.Sum(nil))
		req.Header.Set("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Set("X-API-SIGNATURE", signature)
		req.Header.Set("X-API-TIMESTAMP", nonce)
		res, err = http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		if err != nil {
			t.Fatal("get api handler", err)
		}
		if res.StatusCode != 200 {
			t.Error(res.StatusCode)
			t.Error(res)
			b,_:= ioutil.ReadAll(res.Body)
			t.Error(string(b))
			t.Fatal("error")
		}
		errorRes, err = parseErrResponse(res)
		if err == nil {
			t.Log(errorRes)
			t.Fatal("get api handler", err)
		}
	})
	t.Run("[NG]X-API-SIGNATURE", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().UnixNano()/1000000,10)

		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		req.Header.Add("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Add("X-API-SIGNATURE", "unko")
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		errRes, err := parseErrResponse(res)
		if err != nil {
			t.Log(res.StatusCode)
			t.Fatal("failed to parse error response")
		}
		if errRes.Code != model.ErrLogicSignatureInvalid {
			t.Log(errRes)
			t.Fatal("mismatch err code")
		}
	})
}

type errorResponse struct {
	Message string `json:"message"`
	Code int64 `json:code"`
}

func parseErrResponse(resp *http.Response) (*errorResponse, error){
	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	er := new(errorResponse)
	if err := json.Unmarshal(b, er); err != nil {
		return nil, err
	}
	return er,nil
}

func TestWithMock_AuthenticateMiddleware(t *testing.T) {
	api, err := getAPI()
	if err != nil {
		t.Fatal(err)
	}
	handler := configureAPIforTest(t, api)
	err = api.Validate()
	if err != nil {
		t.Fatal(err)
	}
	ts := httptest.NewServer(handler)
	defer ts.Close()

	t.Run("[OK]X-API-KEY/X-API-SIGNATURE/X-API-TIMESTAMP", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().UnixNano()/int64(time.Millisecond),10)

		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		signature := hex.EncodeToString(hm.Sum(nil))
		req.Header.Add("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Add("X-API-SIGNATURE", signature)
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		if res.StatusCode > 200 {
			t.Log(res)
			t.Fatal("error")
		}
		errorRes, err:= parseErrResponse(res)
		if err == nil {
			t.Log(errorRes)
			t.Fatal("get api handler", err)
		}
	})
	t.Run("[NG]Invalid X-API-TIMESTAMP", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().Add(time.Hour*-2).UnixNano()/1000000,10)
		time.Now()
		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		signature := hex.EncodeToString(hm.Sum(nil))
		req.Header.Add("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Add("X-API-SIGNATURE", signature)
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		errRes, err := parseErrResponse(res)
		if err != nil {
			t.Log(res.StatusCode)
			t.Fatal("failed to parse error response")
		}
		if errRes.Code != model.ErrLogicTimestampInvalid {
			t.Log(errRes)
			t.Fatal("mismatch err code")
		}
	})
	t.Run("[NG]Invalid X-API-SIGNATURE", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().UnixNano()/1000000,10)

		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("unko"))
		hm.Write([]byte(message))
		signature := hex.EncodeToString(hm.Sum(nil))
		req.Header.Add("X-API-KEY", "f908627f-d95a-4f96-b7e4-4263fdaa7fa6")
		req.Header.Add("X-API-SIGNATURE", signature)
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		errRes, err := parseErrResponse(res)
		if err != nil {
			t.Fatal("failed to parse error response")
		}
		if errRes.Code != model.ErrLogicSignatureInvalid {
			t.Log(errRes)
			t.Fatal("mismatch err code")
		}
	})
	t.Run("[NG]Invalid X-API-KEY", func(t *testing.T){
		method := "GET"
		path := "/v1/arbitrage-pairs"
		body := ""
		nonce := strconv.FormatInt(time.Now().UTC().UnixNano()/1000000,10)

		req, err:= http.NewRequest(method, ts.URL + path, nil)
		message := nonce + path + method + body
		hm:=hmac.New(sha256.New, []byte("Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m"))
		hm.Write([]byte(message))
		signature := hex.EncodeToString(hm.Sum(nil))
		req.Header.Add("X-API-KEY", "unko")
		req.Header.Add("X-API-SIGNATURE", signature)
		req.Header.Add("X-API-TIMESTAMP", nonce)

		res, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal("get api handler", err)
		}
		if res.StatusCode == 200 {
			t.Fatal("error")
			t.Log(res)
		}
	})
}
