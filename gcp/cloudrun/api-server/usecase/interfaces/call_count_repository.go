package interfaces

import "context"

type CallCountRepository interface {
	GetByDay(ctx context.Context, uid string) (int, error)
	GetByMonth(ctx context.Context, uid string) (int, error)
	// APIコールカウンタを1増加させる
	Increment(ctx context.Context, uid string, path string) error
}
