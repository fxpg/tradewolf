package infrastructure

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/service"
	"time"
)

func NewArbitrageDataRepositoryClient(exchanges []string) service.ArbitrageDataRepository {
	return &ArbitrageDataRepositoryImpl{
		exchanges: exchanges,
		pool: &redis.Pool{
			MaxIdle: 80,
			// MaxActive: 12000, // max number of connections
			Dial: func() (redis.Conn, error) {
				c, err := redis.Dial("tcp", "localhost:6379")
				if err != nil {
					panic(err.Error())
				}
				return c, err
			},
		},
	}
}

type ArbitrageDataRepositoryImpl struct {
	pool      *redis.Pool
	exchanges []string
}

func (adr *ArbitrageDataRepositoryImpl) GetArbitrageTicker(currencyPair model.CurrencyPair) (ret []model.Arbitrage, err error) {
	c := adr.pool.Get()
	defer c.Close()
	bestAskOnExchangeA, err := redis.Float64(c.Do("get", fmt.Sprintf("ticker:%s:%s:%s:ask", currencyPair.Exchange, currencyPair.Trading, currencyPair.Settlement)))
	if err != nil {
		return []model.Arbitrage{}, err
	}
	bestBidOnExchangeA, err := redis.Float64(c.Do("get", fmt.Sprintf("ticker:%s:%s:%s:bid", currencyPair.Exchange, currencyPair.Trading, currencyPair.Settlement)))
	if err != nil {
		return []model.Arbitrage{}, err
	}

	for _, oppositeExchange := range adr.exchanges {
		bestAskOnExchangeB, err := redis.Float64(c.Do("get", fmt.Sprintf("ticker:%s:%s:%s:ask", oppositeExchange, currencyPair.Trading, currencyPair.Settlement)))
		if err != nil {
			continue
		}
		bestBidOnExchangeB, err := redis.Float64(c.Do("get", fmt.Sprintf("ticker:%s:%s:%s:bid", oppositeExchange, currencyPair.Trading, currencyPair.Settlement)))
		if err != nil {
			continue
		}
		ret = append(ret, model.Arbitrage{
			ArbitragePair: model.ArbitragePair{
				Trading:    currencyPair.Trading,
				Settlement: currencyPair.Settlement,
				ExchangeA:  currencyPair.Exchange,
				ExchangeB:  oppositeExchange,
			},
			BestAskA: bestAskOnExchangeA,
			BestAskB: bestAskOnExchangeB,
			BestBidA: bestBidOnExchangeA,
			BestBidB: bestBidOnExchangeB,
			Time:     time.Now(),
		})
	}
	return ret, nil
}
