package service

import (
	"context"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/infrastructure/api/models"
)

type Arbitrager interface {
	GetID() (int, error)
	GetBoards() ([]*entity.Board, error)
	FindArbitrage() (*entity.Arbitrage, error)
	ExecuteArbitrage(context.Context, *entity.Arbitrage) error
	GetStatus() (entity.ArbitragerState, error)
	SetStatus(entity.ArbitragerState) error
	GetSetting() (*entity.ArbitragerSetting, error)
	SetTimeDuration(duration time.Duration)
	Log(log entity.ArbitragerLog)
	// SetOrderLogic() ([]entity.Logic,error)
	// order() ()
	// arbitrage() ()
	// ArbitrageEnable() ()
	// ArbitrageDisable() ()
}

type arbitrager struct {
	ID                        int
	PublicResourceRepository  PublicResourceRepository  `inject:""`
	PrivateResourceRepository PrivateResourceRepository `inject:""`
	PrivateRepositoryMock     PrivateResourceRepository
	SettingRepository         SettingRepository
	ArbitrageOrderRepository  ArbitrageOrderRepository
	ArbitragerLogRepository   ArbitragerLogRepository
	arbitragerSetting         *entity.ArbitragerSetting
	timeDuration              time.Duration
	logCallback               func(log entity.ArbitragerLog)
}

var mu sync.Mutex

func (a *arbitrager) Log(log entity.ArbitragerLog) {
	a.ArbitragerLogRepository.Insert(&log)
	a.logCallback(log)
}

func (a *arbitrager) GetSetting() (*entity.ArbitragerSetting, error) {
	return a.arbitragerSetting, nil
}
func (a *arbitrager) getPrivateRepositoryByMode() PrivateResourceRepository {
	mu.Lock()
	defer mu.Unlock()
	if a.arbitragerSetting.ArbitragerMode == entity.ArbitragerMode_TEST {
		if a.PrivateRepositoryMock == nil {
			a.PrivateRepositoryMock = NewPrivateRepositoryMock()
		}
		return a.PrivateRepositoryMock
	}
	return a.PrivateResourceRepository
}

func NewArbitrager(id int, pubResRep PublicResourceRepository, priResRep PrivateResourceRepository, settingRepository SettingRepository, aoRepository ArbitrageOrderRepository, arblogsRep ArbitragerLogRepository, arbitragerSetting *entity.ArbitragerSetting, logCallback func(log entity.ArbitragerLog)) Arbitrager {
	return &arbitrager{ID: id, PublicResourceRepository: pubResRep, PrivateResourceRepository: priResRep, SettingRepository: settingRepository, ArbitrageOrderRepository: aoRepository, ArbitragerLogRepository: arblogsRep, arbitragerSetting: arbitragerSetting, timeDuration: time.Second, logCallback: logCallback}
}

func (a *arbitrager) SetTimeDuration(duration time.Duration) {
	a.timeDuration = duration
}

func (a *arbitrager) GetStatus() (entity.ArbitragerState, error) {
	state := a.arbitragerSetting.GetArbitragerState()
	return state, nil
}

func (a *arbitrager) SetStatus(state entity.ArbitragerState) error {
	a.arbitragerSetting.ArbitragerState = state
	return nil
}

func (a *arbitrager) Stop() error {
	return nil
}

func OrderTypeString(orderType models.OrderType) string {
	if orderType == models.Bid {
		return "SELL"
	}
	return "BUY"
}

func (a *arbitrager) makePosition(ctx context.Context, orderType models.OrderType, side *entity.Side) error {
	trading := side.CurrencyPair.Trading
	settlement := side.CurrencyPair.Settlement

	orderId, err := a.getPrivateRepositoryByMode().Order(
		side.Exchange,
		trading,
		settlement,
		orderType,
		side.Rate,
		side.Amount,
	)
	if err != nil {
		return errors.Wrap(err, "failed to make position")
	}
	a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: "make position challenge",
		CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
	err = a.ArbitrageOrderRepository.CreateOrder(&entity.ArbitrageOrder{
		Exchange:     side.Exchange,
		Trading:      trading,
		Settlement:   settlement,
		OrderId:      orderId,
		Price:        float32(side.Rate),
		Amount:       float32(side.Amount),
		OrderType:    OrderTypeString(orderType),
		CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
		ArbitragerId: strconv.Itoa(a.ID),
	})
	if err != nil {
		fmt.Println(err)
	}
	arbitrageTimeLimit := time.Now().Add(a.timeDuration * time.Duration(a.arbitragerSetting.ArbitrageWaitLimit))
	fillCheckTicker := time.NewTicker(a.timeDuration * 3)
	defer fillCheckTicker.Stop()

OpenLoop:
	for {
		select {
		case <-ctx.Done():
			a.Log(entity.ArbitragerLog{
				LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: "ctx done",
				CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
			})
			return nil
		case <-fillCheckTicker.C:
			isFilled, err := a.getPrivateRepositoryByMode().IsOrderFilled(side.Exchange, trading, settlement, orderId)
			if err != nil {
				continue
			}
			if isFilled {
				err = a.ArbitrageOrderRepository.FilledOrder(&entity.ArbitrageFilledOrder{
					Exchange:     side.Exchange,
					Trading:      trading,
					Settlement:   settlement,
					OrderId:      orderId,
					Price:        float32(side.Rate),
					Amount:       float32(side.Amount),
					OrderType:    OrderTypeString(orderType),
					CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
					ArbitragerId: strconv.Itoa(a.ID),
				})
				if err != nil {
					fmt.Println(err)
				}
				break OpenLoop
			}
			if !arbitrageTimeLimit.After(time.Now()) {
				err := a.getPrivateRepositoryByMode().CancelOrder(side.Exchange, trading, settlement, models.Bid, orderId)
				if err != nil {
					continue
				}
				a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("canceled make position order %v", orderId),
					CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
				return errors.New("failed to make position")
			}
		}
	}
	return nil
}

func (a *arbitrager) closePosition(ctx context.Context, orderType models.OrderType, side *entity.Side) error {
	trading := side.CurrencyPair.Trading
	settlement := side.CurrencyPair.Settlement

	arbitrageTimeLimit := time.Now().Add(a.timeDuration * time.Duration(a.arbitragerSetting.ArbitrageWaitLimit))
	checkTicker := time.NewTicker(a.timeDuration * 3)
	defer checkTicker.Stop()

	var closeRate float64
CloseCheckLoop:
	for {
		select {
		case <-ctx.Done():
			a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: "ctx done",
				CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
			return errors.New("ctx done")
		case <-checkTicker.C:
			board, err := a.PublicResourceRepository.Board(side.Exchange, trading, settlement)
			if err != nil {
				continue
			}
			if orderType == models.Bid && board.BestBidPrice() >= side.Rate {
				closeRate = side.Rate * (1 + a.arbitragerSetting.ExpectedProfitRate)
				break CloseCheckLoop
			}
			if orderType == models.Ask && board.BestAskPrice() <= side.Rate {
				closeRate = side.Rate * (1 - a.arbitragerSetting.ExpectedProfitRate)
				break CloseCheckLoop
			}
			if !arbitrageTimeLimit.After(time.Now()) {
				a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("close checker is over"),
					CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
				return errors.New("time is over")
			}
			a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("close check is False: [expectedProfitRate]%v", 1+a.arbitragerSetting.ExpectedProfitRate),
				CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
			if orderType == models.Bid {
				a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("close check is False: [bestBidPrice]%v < [expectedProfitPrice]%v", board.BestBidPrice(), side.Rate),
					CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
			}
			if orderType == models.Ask {
				a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("close check is False: [bestAskPrice]%v > [expectedProfitPrice]%v", board.BestAskPrice(), side.Rate),
					CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
			}

		}
	}
	orderId, err := a.getPrivateRepositoryByMode().Order(
		side.Exchange,
		trading,
		settlement,
		orderType,
		closeRate,
		side.Amount,
	)
	if err != nil {
		return errors.Wrap(err, "failed to order")
	}
	a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: "close position challenge",
		CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
	err = a.ArbitrageOrderRepository.CreateOrder(&entity.ArbitrageOrder{
		Exchange:     side.Exchange,
		Trading:      trading,
		Settlement:   settlement,
		OrderId:      orderId,
		Price:        float32(closeRate),
		Amount:       float32(side.Amount),
		OrderType:    OrderTypeString(orderType),
		CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
		ArbitragerId: strconv.Itoa(a.ID),
	})
	if err != nil {
		fmt.Println(err)
	}
CloseLoop:
	for {
		select {
		case <-ctx.Done():
			a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: "ctx done",
				CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
			return errors.New("ctx done")
		case <-checkTicker.C:
			isFilled, err := a.getPrivateRepositoryByMode().IsOrderFilled(side.Exchange, trading, settlement, orderId)
			if err != nil {
				continue
			}
			if isFilled {
				err = a.ArbitrageOrderRepository.FilledOrder(&entity.ArbitrageFilledOrder{
					Exchange:     side.Exchange,
					Trading:      trading,
					Settlement:   settlement,
					OrderId:      orderId,
					Price:        float32(closeRate),
					Amount:       float32(side.Amount),
					OrderType:    OrderTypeString(orderType),
					CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
					ArbitragerId: strconv.Itoa(a.ID),
				})
				if err != nil {
					fmt.Println(err)
				}
				break CloseLoop
			}
			if !arbitrageTimeLimit.After(time.Now()) {
				err := a.getPrivateRepositoryByMode().CancelOrder(side.Exchange, trading, settlement, orderType, orderId)
				if err != nil {
					continue
				}
				a.Log(entity.ArbitragerLog{LogType: entity.LogType_INFO, ArbitragerId: int64(a.ID), Message: fmt.Sprintf("canceled close position order %v", orderId),
					CreatedTime: &entity.Timestamp{Timestamp: ptypes.TimestampNow()}})
				return errors.New("failed to close position")
			}
		}
	}
	return nil
}

func (a *arbitrager) executeLongArbitrage(ctx context.Context, arbitrage *entity.Arbitrage) error {
	err := a.makePosition(ctx, models.Ask, arbitrage.BuySide)
	if err != nil {
		return errors.Wrap(err, "failed to make position")
	}
	err = a.closePosition(ctx, models.Bid, arbitrage.BuySide)
	if err != nil {
		return errors.Wrap(err, "failed to close position")
	}
	return nil
}

func (a *arbitrager) executeShortArbitrage(ctx context.Context, arbitrage *entity.Arbitrage) error {
	err := a.makePosition(ctx, models.Bid, arbitrage.SellSide)
	if err != nil {
		return errors.Wrap(err, "failed to make position")
	}
	err = a.closePosition(ctx, models.Ask, arbitrage.SellSide)
	if err != nil {
		return errors.Wrap(err, "failed to close position")
	}
	return nil
}

func (a *arbitrager) ExecuteArbitrage(ctx context.Context, arbitrage *entity.Arbitrage) error {
	if a.arbitragerSetting.ArbitragerOperation == entity.ArbitragerOperation_LONG_SIDE {
		// MAKE LONG POSITION AND CLOSE
		err := a.executeLongArbitrage(ctx, arbitrage)
		if err != nil {
			return errors.Wrap(err, "failed to execute long arbitrage")
		}
	} else if a.arbitragerSetting.ArbitragerOperation == entity.ArbitragerOperation_SHORT_SIDE {
		// MAKE SHORT POSITION AND CLOSE
		err := a.executeShortArbitrage(ctx, arbitrage)
		if err != nil {
			return errors.Wrap(err, "failed to execute short arbitrage")
		}
	} else {
		// MAKE BOTH LONG AND SHORT POSITION AND CLOSE
		wg := sync.WaitGroup{}
		go func() {
			wg.Add(1)
			defer wg.Done()
			err := a.executeLongArbitrage(ctx, arbitrage)
			if err != nil {
				fmt.Println(err)
			}
			return
		}()
		go func() {
			wg.Add(1)
			defer wg.Done()
			err := a.executeShortArbitrage(ctx, arbitrage)
			if err != nil {
				fmt.Println(err)
			}
			return
		}()
		wg.Wait()

	}
	return nil
}

func (a *arbitrager) FindArbitrage() (*entity.Arbitrage, error) {
	currencyPair := a.arbitragerSetting.ArbitragePair.CurrencyPair

	exchangeABoard, err := a.PublicResourceRepository.Board(a.arbitragerSetting.ArbitragePair.ExchangeA,
		currencyPair.Trading,
		currencyPair.Settlement)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get exchange_a board in find arbitrage")
	}
	exchangeBBoard, err := a.PublicResourceRepository.Board(a.arbitragerSetting.ArbitragePair.ExchangeB,
		currencyPair.Trading,
		currencyPair.Settlement)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get exchange_b board in find arbitrage")
	}

	bestBuyPriceAtA := exchangeABoard.BestAskPrice()
	bestBuyAmountAtA := exchangeABoard.BestAskAmount()
	bestSellPriceAtA := exchangeABoard.BestBidPrice()
	bestSellAmountAtA := exchangeABoard.BestBidAmount()
	bestBuyPriceAtB := exchangeBBoard.BestAskPrice()
	bestBuyAmountAtB := exchangeBBoard.BestAskAmount()
	bestSellPriceAtB := exchangeBBoard.BestBidPrice()
	bestSellAmountAtB := exchangeBBoard.BestBidAmount()

	expectedProfitRate := a.arbitragerSetting.ExpectedProfitRate

	if isArbitrageChance(expectedProfitRate, bestSellPriceAtA, bestBuyPriceAtB) {
		// When selling at A is better
		sellSide := entity.NewSide(a.arbitragerSetting.ArbitragePair.ExchangeA, bestSellPriceAtA, bestSellAmountAtA, a.arbitragerSetting.ArbitragePair.CurrencyPair)
		buySide := entity.NewSide(a.arbitragerSetting.ArbitragePair.ExchangeB, bestBuyPriceAtB, bestBuyAmountAtB, a.arbitragerSetting.ArbitragePair.CurrencyPair)
		return entity.NewArbitrage(buySide, sellSide), nil
	} else if isArbitrageChance(expectedProfitRate, bestSellPriceAtB, bestBuyPriceAtA) {
		// When selling at B is better
		sellSide := entity.NewSide(a.arbitragerSetting.ArbitragePair.ExchangeB, bestSellPriceAtB, bestSellAmountAtB, a.arbitragerSetting.ArbitragePair.CurrencyPair)
		buySide := entity.NewSide(a.arbitragerSetting.ArbitragePair.ExchangeA, bestBuyPriceAtA, bestBuyAmountAtA, a.arbitragerSetting.ArbitragePair.CurrencyPair)
		return entity.NewArbitrage(buySide, sellSide), nil
	} else {
		// Otherwise(no chance)
		a.Log(entity.ArbitragerLog{
			LogType:      entity.LogType_ERROR,
			ArbitragerId: int64(a.ID),
			Message:      fmt.Sprintf("no arbitrage: %s-%s %s_%s", a.arbitragerSetting.ArbitragePair.ExchangeA, a.arbitragerSetting.ArbitragePair.ExchangeB, currencyPair.Trading, currencyPair.Settlement),
			CreatedTime:  &entity.Timestamp{Timestamp: ptypes.TimestampNow()},
		})
		return nil, nil
	}
}

func (a *arbitrager) GetBoards() ([]*entity.Board, error) {
	ret := make([]*entity.Board, 0)
	aBoard, err := a.PublicResourceRepository.Board(
		a.arbitragerSetting.ArbitragePair.ExchangeA,
		a.arbitragerSetting.ArbitragePair.CurrencyPair.Trading,
		a.arbitragerSetting.ArbitragePair.CurrencyPair.Settlement,
	)
	if err != nil {
		return ret, err
	}
	bBoard, err := a.PublicResourceRepository.Board(
		a.arbitragerSetting.ArbitragePair.ExchangeB,
		a.arbitragerSetting.ArbitragePair.CurrencyPair.Trading,
		a.arbitragerSetting.ArbitragePair.CurrencyPair.Settlement,
	)
	if err != nil {
		return ret, err
	}
	aBoardAsks := make([]*entity.BoardBar, 0)
	aBoardBids := make([]*entity.BoardBar, 0)
	for _, ask := range aBoard.Asks {
		aBoardAsks = append(aBoardAsks, &entity.BoardBar{Price: ask.Price, Amount: ask.Amount})
	}
	for _, bid := range aBoard.Bids {
		aBoardBids = append(aBoardBids, &entity.BoardBar{Price: bid.Price, Amount: bid.Amount})
	}
	bBoardAsks := make([]*entity.BoardBar, 0)
	bBoardBids := make([]*entity.BoardBar, 0)
	for _, ask := range bBoard.Asks {
		bBoardAsks = append(bBoardAsks, &entity.BoardBar{Price: ask.Price, Amount: ask.Amount})
	}
	for _, bid := range bBoard.Bids {
		bBoardBids = append(bBoardBids, &entity.BoardBar{Price: bid.Price, Amount: bid.Amount})
	}

	ab := &entity.Board{DateTime: ptypes.TimestampNow(), CurrencyPair: a.arbitragerSetting.ArbitragePair.CurrencyPair, Exchange: a.arbitragerSetting.ArbitragePair.ExchangeA, Bids: aBoardBids, Asks: aBoardAsks}
	bb := &entity.Board{DateTime: ptypes.TimestampNow(), CurrencyPair: a.arbitragerSetting.ArbitragePair.CurrencyPair, Exchange: a.arbitragerSetting.ArbitragePair.ExchangeB, Bids: bBoardBids, Asks: bBoardAsks}
	ret = append(ret, ab)
	ret = append(ret, bb)
	return ret, nil
}

func (a *arbitrager) GetID() (int, error) {
	return a.ID, nil
}

func isArbitrageChance(expectedProfitRate float64, sellPrice float64, buyPrice float64) bool {
	return sellPrice/buyPrice >= 1+expectedProfitRate
}
