import * as firebase from '@firebase/testing'

let db
// let projectId = `tradewolf-local-${Date.now()}`
let projectId = `tradewolf`

async function setup(auth:any) {
   const app = await firebase.initializeTestApp({
     projectId: projectId,
     auth: auth
   }); 
   db = app.firestore(); 
   let data:{[key:string]:object} = {
       // 'keys/252iyTMoJjj139YcRQok': {
       'keys/252iyTMoJjj139YcRQok': {
       apikey: 'f908627f-d95a-4f96-b7e4-4263fdaa7fa6',
       secretKey: 'Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m',
       uid: 'YAGSqhTT7ZZCCDXg1Nfq71Xk5zQ2'
     },
     'users/YAGSqhTT7ZZCCDXg1Nfq71Xk5zQ2': {
        plan: 'BOOST PLAN'
     },
       'allowance/BOOST PLAN': {
           allowanceByDay: 40000,
           allowanceByMonth: 1200000,
       }
   }
   // Add data
   for (const key in data) {
     const ref = db.doc(key);
     await ref.set(data[key]);
   }
    await db.collection("keys").where("apikey", "==", 'f908627f-d95a-4f96-b7e4-4263fdaa7fa6').get().then((snap)=>{
        for (const s of snap.docs)
            console.log(s.data())
    })
    await db.collection("users").get().then((snap=>{
        for (const s of snap.docs)
            console.log(s.data())
    }))
}

// Start your firestore emulator for (at least) firestore
// firebase emulators:start --only firestore

setup({ uid: "admin" }).then(()=>{
  console.log("firestore initialized")
  console.log("now you can use firestore")
  process.exit(0)
}).catch(err=>{
  console.error(err)
})
