import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context('@/components/base', true, /\.vue$/)

requireComponent.keys().forEach((fileName: string) => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, ''))
  )
  Vue.component(
    `Base${componentName}`,
    componentConfig.default || componentConfig
  )
})
const requireArbitragerComponent = require.context(
  '@/components/arbitrager',
  true,
  /\.vue$/
)
requireArbitragerComponent.keys().forEach((fileName: string) => {
  const componentConfig = requireArbitragerComponent(fileName)
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, ''))
  )
  Vue.component(
    `Arbitrager${componentName}`,
    componentConfig.default || componentConfig
  )
})
