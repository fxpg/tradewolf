package util

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"net/http"
)

func WriteError(w http.ResponseWriter, err error) {
	errorId := model.ErrorID(err)
	if model.IsSystemError(err) {
		errorId = 10000
		err = errors.New("internal error")
	}

	res := struct {
		Message string `json:"message"`
		Code int64 `json:code"`
	}{
		Message: model.ErrorMessage(err),
		Code: errorId,
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(res)
}
