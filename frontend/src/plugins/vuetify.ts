import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import { VuetifyPreset } from 'vuetify/types/presets'
//import 'vuetify/dist/vuetify.min.css'

import '@mdi/font/css/materialdesignicons.css'
Vue.use(Vuetify)

const options: VuetifyPreset = {
  theme: {
    dark: true,
    themes: {
      dark: {
        primary: '#E91E63',
        secondary: '#9C27b0',
        accent: '#9C27b0',
        info: '#00CAE3',
        base: '#282c34'
      }
    }
  },
  customProperties: true,
  icons: {
    iconfont: 'mdi'
  }
}
export default new Vuetify(options)
