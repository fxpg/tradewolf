package controller

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/models"

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

func str(s string) *string {
	return &s
}

// returns statusCode, errorObject
func toErrorResponse(err error) (int, *models.Error) {
	errorId := model.ErrorID(err)
	if model.IsSystemError(err) {
		errorId = 10000
	}

	return 500, &models.Error{
		Code:    errorId,
		Message: str(model.ErrorMessage(err)),
	}
}
