import requests, time, sys, hmac, hashlib, json
if sys.version_info.major <= 2:
    from urllib import urlencode
else:
    from urllib.parse import urlencode

class Exchange(object):
    def __init__(self, apikey, secretkey):
        self._apikey = apikey
        self._secretkey = secretkey

    def arbitrage_pairs(self):
        raise Exception("not implemented")

BASE_URL = "api.tradewolf.io"

class Tradewolf(Exchange):
    def __init__(self, apikey, secretkey):
        def httpGet(path, params):
            # path = "/v1/arbitrage-pairs"
            nonce = str(int(round(time.time() * 1000)))
            method = "GET"
            query = urlencode(params)
            message = nonce + path + method + ""
            headers = {
                "X-API-KEY": self._apikey,
                "X-API-TIMESTAMP": nonce,
                "X-API-SIGNATURE": hmac.new(self._secretkey.encode("utf8"), message.encode("utf8"), digestmod=hashlib.sha256).hexdigest(),
            }            
            return self.session.get("https://"+BASE_URL+path+"?"+query, headers=headers)

        super(Tradewolf, self).__init__(apikey, secretkey)
        self.session = requests.session()
        self.httpGet = httpGet

    def __del__(self):
        self.session.close()

    def arbitrage_pairs(self):
        ARBITRAGE_PAIR_PATH = "/v1/arbitrage-pairs"
        return self.httpGet(ARBITRAGE_PAIR_PATH, [])

    def orderbooks(self, exchange):
        ORDERBOOKS_PATH = "/v1/orderbooks"
        return self.httpGet(ORDERBOOKS_PATH, {"exchange": exchange})

    def withdrawal_fees(self, exchange):
        WITHDRAWAL_FEES_PATH = "/v1/withdrawal-fees"
        return self.httpGet(WITHDRAWAL_FEES_PATH, {"exchange": exchange})

tradewolf = Tradewolf("f908627f-d95a-4f96-b7e4-4263fdaa7fa6", "Y5aVLIiHlcJQGK3j4x9nQUf798wzrr0m")
#print(tradewolf.arbitrage_pairs().text)

# print(tradewolf.arbitrage_pairs().text)
# print(tradewolf.withdrawal_fees("poloniex").text)
k = tradewolf.orderbooks("poloniex")
print(k.text)
# print(k)
ob = json.loads(k.text)
# print(ob)
counter = 0
zero_counter = 0

for i in ob:
    if len(i["asks"]) == 0:
        zero_counter += 1
    counter += 1

print(counter)
print(zero_counter)

# for i in range(10):
#     print()
#     time.sleep(0.1)
