package middlewares

import (
	"context"
	"net/http"
	"time"
)


func NewRequestTimeoutMiddleware(timeout time.Duration) Middleware {
	return func(handler http.Handler) http.Handler {
		fn := func (w http.ResponseWriter, r *http.Request) {
			ctx, cancel := context.WithTimeout(r.Context(), timeout)
			defer cancel()
			r = r.WithContext(ctx)

			handler.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}