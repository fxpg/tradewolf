package interfaces

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

type WithdrawalFeeRepository interface {
	GetByExchange(ctx context.Context, exchange string) (model.WithdrawalFees, error)
}
