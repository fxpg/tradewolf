package middlewares

import (
	"context"
	"github.com/google/uuid"
	"net/http"
)

const (
	requestIdContextKey = "TW-Request-ID"
)

// Contextの"TW-Request-ID"にリクエストIDを格納する
// RequestIDの仕様(|は文字列結合, 括弧内は長さ)
//     Hostname(10) | yyyyMMddHHmmss
func NewRequestIdMiddleware() Middleware {
	return func(handler http.Handler) http.Handler {
		fn := func (w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), requestIdContextKey, uuid.New().String())
			r = r.WithContext(ctx)

			handler.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func GetRequestIdFromContext(ctx context.Context) string {
	return ctx.Value(requestIdContextKey).(string)
}