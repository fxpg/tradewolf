package repository

import (
	"context"
	"fmt"
	"github.com/patrickmn/go-cache"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
	"time"
)

func NewOrderBookCachedRepository(src interfaces.OrderBookRepository) interfaces.OrderBookRepository {
	return &OrderBookCachedRepository{
		src: src,
		c: cache.New(3 * time.Second, 1 * time.Second),
	}
}

type OrderBookCachedRepository struct {
	src interfaces.OrderBookRepository
	c *cache.Cache
}

func (o *OrderBookCachedRepository) Get(ctx context.Context, exchange string, currencyPair model.CurrencyPair) (*model.OrderBook, error) {
	res, ok := o.c.Get(cacheKey(exchange, currencyPair))
	if ok {
		return res.(*model.OrderBook), nil
	}

	val, err := o.src.Get(ctx, exchange, currencyPair)
	if err != nil {
		return nil, err
	}

	o.c.Set(cacheKey(exchange, currencyPair), val, cache.DefaultExpiration)
	return val, nil
}

func cacheKey(exchange string, pair model.CurrencyPair) string {
	return fmt.Sprintf("%s:%s", exchange, pair)
}