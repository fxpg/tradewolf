package middlewares

import (
	"net/http"
)

// TODO: Middlewareのサンプル。あとで消す
func NewDoNothingMiddleware() Middleware {
	return func(handler http.Handler) http.Handler {
		fn := func (w http.ResponseWriter, r *http.Request) {
			// なにもしない
			handler.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}