package controller

import (
	"github.com/go-openapi/runtime/middleware"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/controller/mapper"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/restapi/operations"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/utils"
)

func NewWithdrawalFeesController(
	u *usecase.GetWithdrawalFeesUsecase,
	logger utils.Logger,
) *WithdrawalFeesController {
	return &WithdrawalFeesController{
		u: u,
		logger: logger,
	}
}

type WithdrawalFeesController struct {
	u *usecase.GetWithdrawalFeesUsecase
	logger utils.Logger
}

func (c *WithdrawalFeesController) Get(params operations.GetWithdrawalFeesByExchangeParams) middleware.Responder {
	res, err := c.u.Exec(params.HTTPRequest.Context(), params.Exchange)
	if err != nil {
		if model.IsSystemError(err) {
			c.logger.Errorf(err.Error())
		}
		code, resErr := toErrorResponse(err)
		return operations.NewGetWithdrawalFeesByExchangeDefault(code).WithPayload(resErr)
	}

	return operations.NewGetWithdrawalFeesByExchangeOK().WithPayload(mapper.MapWithdrawalFees(res))
}
