import admin, { ServiceAccount } from 'firebase-admin'
import { FirestoreSimple } from 'firestore-simple'
import ccxt from 'ccxt'
import { symlinkSync } from 'fs';

const ROOT_PATH = 'tradewolf/public'

admin.initializeApp();
const firestore = admin.firestore();

interface ArbitragePair {
  id: string,
  trading: string,
  settlement: string,
  exchangeA: string,
  exchangeB: string
}

(async()=>{
  const firestoreSimple = new FirestoreSimple(firestore)
  const dao = firestoreSimple.collection<ArbitragePair>({ path: `${ROOT_PATH}/arbitrage_pair` })
  
  const exchanges = [new ccxt.ftx(), new ccxt.binance(), new ccxt.hitbtc, new ccxt.kucoin(), new ccxt.huobipro(), new ccxt.poloniex(), new ccxt.coinex(), /*new ccxt.cex(),*/ new ccxt.gateio(), new ccxt.gemini(), new ccxt.liquid(), new ccxt.upbit(), new ccxt.bitfinex2(), new ccxt.bittrex(), new ccxt.coinbase(), new ccxt.bitstamp(), new ccxt.zb()]
  let addArbitragePairs: ArbitragePair[] = []
  for(let ex of exchanges) {
    await ex.loadMarkets()
  }
  for (let i =0; i<exchanges.length-1; i++) {
    for (let j=i+1; j< exchanges.length; j++) {
      for(let marketAKey in exchanges[i].markets) {
        for(let marketBKey in exchanges[j].markets) {
          if((exchanges[i].markets[marketAKey].base === exchanges[j].markets[marketBKey].base) &&
          (exchanges[i].markets[marketAKey].quote === exchanges[j].markets[marketBKey].quote)) {
            addArbitragePairs.push({
              id: "",
              trading: exchanges[i].markets[marketAKey].base,
              settlement: exchanges[i].markets[marketAKey].quote,
              exchangeA: exchanges[i].name.toLowerCase(),
              exchangeB: exchanges[j].name.toLowerCase()
            })
          }
        }
      }
    }
  }
  const deleteArbitragePairs: ArbitragePair[] = await dao.fetchAll()

  let deleteList :ArbitragePair[][] =[]
  let i = 0;
  while(0 !== deleteArbitragePairs.length) {
    deleteList.push(deleteArbitragePairs.splice(0, 249))
    i = i + 249
  }
  for(let dl of deleteList) {
    await dao.bulkDelete(dl.map((arbitragePair,index)=>{return arbitragePair.id}))
  }
  let addList :ArbitragePair[][] =[]
  let j = 0;
  while(0 !== addArbitragePairs.length) {
    const addTarget = addArbitragePairs.splice(0, 249)
    addList.push(addTarget)
    j = j + 249
  }
  for(let al of addList) {
    await dao.bulkAdd(al)
  }
  console.log("done")
})();