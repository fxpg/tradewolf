package controller

import (
	"flag"
	"github.com/rivo/tview"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/domain/model"
	"gitlab.com/fxpg/tradewolf/gcp/kubernetes/ticker-arbitrage-calculator/usecase"
)

var (
	enableLog  = flag.Bool("log", false, "enable output log")
	filterWord = flag.String("proc", "", "use word to filtering process name when starting")
)

const (
	InputPanel int = iota + 1
	FilterDropDownPanel
	ArbitragedListPanel
	ArbitragedCompleteListPanel
	ArbitrageInfoPanel
	LogPanel
)

type GuiController interface {
	Confirm(message, doneLabel string, primitive tview.Primitive, doneFunc func())
	CloseAndSwitchPanel(removePrimitive string, primitive tview.Primitive)
	Modal(p tview.Primitive, width, height int) tview.Primitive
	// SwitchPanel(p tview.Primitive) *tview.Application
	UpdateArbitrageData(arbitrages []model.ArbitrageWatchSetting) error
	// UpdateViews()
	FilterWatchingArbitrages(pair model.CurrencyPair) ([]model.ArbitrageWatchSetting, error)
	CurrentPanelKind() int
	Run() error
	UpdateViews()
	UpdateArbitrageWatchMangerView() error
	Log(text string)
}

type gui struct {
	FilterInput           *tview.InputField
	calculatorUsecase     usecase.ArbitrageCalculatorUsecase
	ArbitrageWatchManager ArbitrageWatchManager
	NaviView              NaviView
	LogView               LogView
	ArbitrageInfoView     ArbitrageInfoView
	FilterTypeDropDown    FilterView
	App                   *tview.Application
	Pages                 *tview.Pages
	Panels
}

type Panels struct {
	Current int
	Panels  []tview.Primitive
	Kinds   []int
}

func NewGuiController(arbitrageWatchManager ArbitrageWatchManager, calculatorUsecase usecase.ArbitrageCalculatorUsecase) GuiController {
	filterInput := tview.NewInputField().SetLabel("cmd name:")
	filterInput.SetText("")
	arbitrageInfoView := NewArbitrageInfoView()
	naviView := NewNaviView()
	logView := NewLogView()
	filterView := NewFilterView()

	g := &gui{
		FilterInput:           filterInput,
		ArbitrageWatchManager: arbitrageWatchManager,
		App:                   tview.NewApplication(),
		ArbitrageInfoView:     arbitrageInfoView,
		NaviView:              naviView,
		LogView:               logView,
		FilterTypeDropDown:    filterView,
		calculatorUsecase:     calculatorUsecase,
	}
	g.Panels = Panels{
		Panels: []tview.Primitive{
			filterInput,
			filterView.Primitive(),
			arbitrageWatchManager.PrimitiveOnWatchingTable(),
			arbitrageWatchManager.PrimitiveOnCompleteTable(),
			arbitrageInfoView.Primitive(),
		},
		Kinds: []int{
			InputPanel,
			FilterDropDownPanel,
			ArbitragedListPanel,
			ArbitragedCompleteListPanel,
			ArbitrageInfoPanel,
		},
	}
	return g
}

func (g *gui) Confirm(message, doneLabel string, primitive tview.Primitive, doneFunc func()) {
	modal := tview.NewModal().
		SetText(message).
		AddButtons([]string{doneLabel, "Cancel"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			g.CloseAndSwitchPanel("modal", primitive)
			if buttonLabel == doneLabel {
				g.App.QueueUpdateDraw(func() {
					doneFunc()
				})
			}
		})

	g.Pages.AddAndSwitchToPage("modal", g.Modal(modal, 50, 29), true).ShowPage("main")
}

func (g *gui) CloseAndSwitchPanel(removePrimitive string, primitive tview.Primitive) {
	g.Pages.RemovePage(removePrimitive).ShowPage("main")
	g.SwitchPanel(primitive)
}

func (g *gui) Modal(p tview.Primitive, width, height int) tview.Primitive {
	return tview.NewGrid().
		SetColumns(0, width, 0).
		SetRows(0, height, 0).
		AddItem(p, 1, 1, 1, 1, 0, 0, true)
}

func (g *gui) SwitchPanel(p tview.Primitive) *tview.Application {
	g.UpdateViews()
	return g.App.SetFocus(p)
}

func (g *gui) UpdateArbitrageData(arbitrages []model.ArbitrageWatchSetting) error {
	return g.ArbitrageWatchManager.UpdateData(arbitrages)
}

func (g *gui) UpdateArbitrageWatchMangerView() error {
	g.App.QueueUpdateDraw(func() {
		g.ArbitrageWatchManager.UpdateView()
	})
	return nil
}
func (g *gui) UpdateViews() {
	// 各種UIコンポーネントのUpdate処理をまとめる
	g.ArbitrageInfoView.UpdateView(g)
	g.NaviView.UpdateView(g)
	g.LogView.UpdateView(g)
}

func (g *gui) FilterWatchingArbitrages(pair model.CurrencyPair) ([]model.ArbitrageWatchSetting, error) {
	return g.calculatorUsecase.FilterWatchingArbitrages(pair)
}

func (g *gui) CurrentPanelKind() int {
	return g.Panels.Kinds[g.Panels.Current]
}

func (g *gui) Log(text string) {
	lines := []string{text}
	g.LogView.AddText(lines)
}

func (g *gui) Run() error {
	g.SetKeybinds()
	if err := g.ArbitrageWatchManager.UpdateView(); err != nil {
		g.Log(err.Error())
		return err
	}
	g.Log("started program...")
	// when start app, set select index 0
	g.ArbitrageWatchManager.SelectOnWatchingTable(1, 0)
	g.ArbitrageWatchManager.SelectOnCompleteTable(1, 0)
	g.UpdateViews()

	infoGrid := tview.NewGrid().SetRows(0, 0).
		SetColumns(0, 0, 0).
		AddItem(g.ArbitrageWatchManager.PrimitiveOnWatchingTable(), 0, 0, 1, 2, 0, 0, true).
		AddItem(g.ArbitrageInfoView.Primitive(), 0, 2, 1, 1, 0, 0, true).
		AddItem(g.ArbitrageWatchManager.PrimitiveOnCompleteTable(), 1, 0, 1, 3, 0, 0, true)

	grid := tview.NewGrid().SetRows(1, -4, -1, 1).
		SetColumns(0, 0, 0).
		AddItem(g.FilterInput, 0, 0, 1, 1, 0, 0, true).
		AddItem(g.FilterTypeDropDown.Primitive(), 0, 1, 1, 1, 0, 0, true).
		AddItem(infoGrid, 1, 0, 1, 3, 0, 0, true).
		AddItem(g.LogView.Primitive(), 2, 0, 1, 3, 0, 0, false).
		AddItem(g.NaviView.Primitive(), 3, 0, 1, 3, 0, 0, false)

	g.Pages = tview.NewPages().
		AddAndSwitchToPage("main", grid, true)

	if err := g.App.SetRoot(g.Pages, true).Run(); err != nil {
		g.App.Stop()
		g.Log(err.Error())
		return err
	}

	return nil
}
