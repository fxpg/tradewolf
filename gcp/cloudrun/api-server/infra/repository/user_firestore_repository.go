package repository

import (
	"github.com/tattsun/qerror"
	"gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/usecase/interfaces"
)

import "cloud.google.com/go/firestore"

import "gitlab.com/fxpg/tradewolf/gcp/cloudrun/api-server/domain/model"

import "context"

func NewUserFirestoreRepository(cli *firestore.Client) (interfaces.UserRepository, error) {
	return &UserFirestoreRepository{
		cli: cli,
	}, nil
}

type UserFirestoreRepository struct {
	cli *firestore.Client
}

func (r *UserFirestoreRepository) GetAllowanceCallByDay(ctx context.Context, uid string) (int, error) {
	res, err := r.cli.Collection("users").Doc(uid).Get(ctx)
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	res, err = r.cli.Collection("allowance").Doc(res.Data()["plan"].(string)).Get(ctx)
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	return int(res.Data()["allowanceByDay"].(int64)), nil
}

func (r *UserFirestoreRepository) GetAllowanceCallByMonth(ctx context.Context, uid string) (int, error) {
	res, err := r.cli.Collection("users").Doc(uid).Get(ctx)
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	res, err = r.cli.Collection("allowance").Doc(res.Data()["plan"].(string)).Get(ctx)
	if err != nil {
		return 0, qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	return int(res.Data()["allowanceByMonth"].(int64)), nil
}

func (r *UserFirestoreRepository) GetUidByApiKey(ctx context.Context, apikey string) (string, error) {
	res, err := r.cli.Collection("keys").Where("apikey", "==", apikey).Documents(ctx).Next()
	if err != nil {
		return "", qerror.WrapWith(err, model.ErrSystemFirestoreRequestFailed)
	}
	return res.Data()["uid"].(string), nil
}