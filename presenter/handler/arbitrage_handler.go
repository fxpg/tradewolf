package handler

import (
	"fmt"
	"strconv"

	"github.com/golang/protobuf/jsonpb"
	"github.com/wailsapp/wails"
	"gitlab.com/fxpg/tradewolf/application/usecase"
	"gitlab.com/fxpg/tradewolf/domain/entity"
	"gitlab.com/fxpg/tradewolf/infrastructure/logger"
)

type ArbitrageHandler interface {
	GetPairs() ([]entity.ArbitragePair, error) // arbPair
	GetArbitrageByArbitragePair() ([]entity.ArbitragePairArbitrage, error)
	GetNetArbitragesByArbitragePair() ([]entity.NetArbitrageByArbitragePair, error)

	GetArbitragerIDs() ([]int, error)
	GetArbitragePairByArbitrager(int) (*entity.ArbitragePair, error)
	GetArbitragerSetting(int) (*entity.ArbitragerSetting, error)
	CreateArbitrager(string) (int, error)
	GetBoards(int) ([]*entity.Board, error)
	StartArbitrager(id string) error
	StopArbitrager(id string) error
	DeleteArbitrager(id string) error

	GetTransferFee(exchange string, coin string) (float64, error)
	GetTransferFees() (map[string]map[string]float64, error)
	GetBalances() (map[string]map[string]float64, error)

	GetOrdersByArbitragerID(id int) ([]entity.ArbitrageOrder, error)
	GetOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageOrder, error)
	GetOrdersByExchange(exchange string) ([]entity.ArbitrageOrder, error)
	GetOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageOrder, error)

	GetFilledOrdersByArbitragerID(id int) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByExchange(exchange string) ([]entity.ArbitrageFilledOrder, error)
	GetFilledOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageFilledOrder, error)

	GetArbitragerLogsByArbitragerID(int) ([]entity.ArbitragerLog, error)
	WailsInit(runtime *wails.Runtime) error
}

type arbitrageHandler struct {
	usecase usecase.ArbitrageUsecase
	runtime *wails.Runtime
}

func (ah *arbitrageHandler) GetBalances() (map[string]map[string]float64, error) {
	return ah.usecase.GetBalances()
}

func (ah *arbitrageHandler) GetTransferFee(exchange string, coin string) (float64, error) {
	return ah.usecase.GetTransferFee(exchange, coin)
}

func (ah *arbitrageHandler) GetTransferFees() (map[string]map[string]float64, error) {
	return ah.usecase.GetTransferFees()
}

func (ah *arbitrageHandler) GetArbitragerLogsByArbitragerID(id int) ([]entity.ArbitragerLog, error) {
	return ah.usecase.GetArbitragerLogsByArbitragerID(id)
}

func (ah *arbitrageHandler) GetOrdersByArbitragerID(id int) ([]entity.ArbitrageOrder, error) {
	return ah.usecase.GetOrdersByArbitragerID(id)
}

func (ah *arbitrageHandler) GetOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	return ah.usecase.GetOrdersByCurrencyPair(trading, settlement)
}

func (ah *arbitrageHandler) GetOrdersByExchange(exchange string) ([]entity.ArbitrageOrder, error) {
	return ah.usecase.GetOrdersByExchange(exchange)
}

func (ah *arbitrageHandler) GetNetArbitragesByArbitragePair() ([]entity.NetArbitrageByArbitragePair, error) {
	return ah.usecase.GetNetArbitragesByArbitragePair()
}
func (ah *arbitrageHandler) GetOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageOrder, error) {
	return ah.usecase.GetOrdersByArbitragePair(exchange, trading, settlement)
}

func (ah *arbitrageHandler) GetFilledOrdersByArbitragerID(id int) ([]entity.ArbitrageFilledOrder, error) {
	return ah.usecase.GetFilledOrdersByArbitragerID(id)
}

func (ah *arbitrageHandler) GetFilledOrdersByCurrencyPair(trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	return ah.usecase.GetFilledOrdersByCurrencyPair(trading, settlement)
}

func (ah *arbitrageHandler) GetFilledOrdersByExchange(exchange string) ([]entity.ArbitrageFilledOrder, error) {
	return ah.usecase.GetFilledOrdersByExchange(exchange)
}

func (ah *arbitrageHandler) GetFilledOrdersByArbitragePair(exchange string, trading string, settlement string) ([]entity.ArbitrageFilledOrder, error) {
	return ah.usecase.GetFilledOrdersByArbitragePair(exchange, trading, settlement)
}

func NewArbitrageHandler(usecase usecase.ArbitrageUsecase) ArbitrageHandler {
	return &arbitrageHandler{usecase, &wails.Runtime{}}
}

func (ah *arbitrageHandler) WailsInit(runtime *wails.Runtime) error {
	ah.runtime = runtime
	ah.runtime.Events.Emit("initalized arbitrageHandler")
	ah.runtime.Log.New("Arbitrager").Info("initalized arbitrageHandler")
	ids, err := ah.usecase.GetArbitragerIDs()
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	for _, i := range ids {
		err = ah.usecase.LoadArbitrager(i, func(log entity.ArbitragerLog) {
			ah.runtime.Events.Emit("Arbitrager", strconv.Itoa(int(log.ArbitragerId)), log.LogType.String(), log.Message)
		})
		if err != nil {
			logger.Get().Error(err)
			return err
		}
	}
	return nil
}

func (ah *arbitrageHandler) GetArbitragePairByArbitrager(id int) (*entity.ArbitragePair, error) {
	return ah.usecase.GetArbitragePairByArbitrager(id)
}

func (ah *arbitrageHandler) GetArbitragerSetting(id int) (*entity.ArbitragerSetting, error) {
	return ah.usecase.GetLoadedArbitragerSetting(id)
}

func (ah *arbitrageHandler) GetPairs() ([]entity.ArbitragePair, error) {
	ret, err := ah.usecase.GetPairs()
	if err != nil {
		logger.Get().Error(err)
		return ret, err
	}
	return ret, nil
}

func (ah *arbitrageHandler) GetArbitragerIDs() ([]int, error) {
	ret, err := ah.usecase.GetArbitragerIDs()
	if err != nil {
		logger.Get().Error(err)
		return ret, err
	}
	return ret, nil
}

func (ah *arbitrageHandler) GetArbitrageByArbitragePair() ([]entity.ArbitragePairArbitrage, error) {
	return ah.usecase.GetArbitrageByArbitragePair()
}

func (ah *arbitrageHandler) StartArbitrager(id string) error {
	i, err := strconv.Atoi(id)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	err = ah.usecase.StartArbitrager(i)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	return nil
}

func (ah *arbitrageHandler) CreateArbitrager(protoString string) (int, error) {
	arbitragerSetting := &entity.ArbitragerSetting{}
	err := jsonpb.UnmarshalString(protoString, arbitragerSetting)
	if err != nil {
		logger.Get().Error(err)
		return 0, err
	}
	logger.Get().Info(fmt.Sprintf("create arbitrager with:%#v", arbitragerSetting))
	ret, err := ah.usecase.CreateArbitrager(arbitragerSetting, func(log entity.ArbitragerLog) {
		ah.runtime.Events.Emit("Arbitrager", strconv.Itoa(int(log.ArbitragerId)), log.LogType, log.Message)
	})
	if err != nil {
		logger.Get().Error(err)
		return 0, err
	}
	return ret, nil
}

func (ah *arbitrageHandler) GetBoards(id int) ([]*entity.Board, error) {
	ret, err := ah.usecase.GetBoards(id)
	if err != nil {
		logger.Get().Error(err)
		return ret, err
	}
	return ret, nil
}

func (ah *arbitrageHandler) StopArbitrager(id string) error {
	i, err := strconv.Atoi(id)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	err = ah.usecase.StopArbitrager(i)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	return nil
}

func (ah *arbitrageHandler) DeleteArbitrager(id string) error {
	i, err := strconv.Atoi(id)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	err = ah.usecase.DeleteArbitrager(i)
	if err != nil {
		logger.Get().Error(err)
		return err
	}
	return nil
}
