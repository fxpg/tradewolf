provider "vultr" {
  rate_limit  = 3000
  retry_limit = 1
}
# VULTR_API_KEY

variable "ssh_key_windows" {

}


data "vultr_region" "main_cite" {
  filter {
    name   = "name"
    values = ["Tokyo"]
  }
}

data "vultr_os" "ubuntu" {
  filter {
    name   = "name"
    values = ["CoreOS Stable"]
  }
}

data "vultr_plan" "starter" {
  filter {
    name   = "price_per_month"
    values = ["20.00"]
  }

  filter {
    name   = "ram"
    values = ["4096"]
  }
}

resource "vultr_server" "rancher_develop" {
  region_id         = data.vultr_region.main_cite.id
  plan_id           = data.vultr_plan.starter.id
  os_id             = data.vultr_os.ubuntu.id
  ssh_key_ids       = [vultr_ssh_key.develop.id]
  hostname          = "FXPG-R01"
  label             = "FXPG-R01"
  tag               = "rancher_server_develop"
  firewall_group_id = vultr_firewall_group.for_rancher_server_develop.id
}

resource "vultr_ssh_key" "develop" {
  name    = "windows_machine"
  ssh_key = var.ssh_key_windows
}

resource "vultr_firewall_group" "for_rancher_server_develop" {
  description = "firewall group for rancher server develop"
}

resource "vultr_firewall_rule" "ssh" {
  firewall_group_id = vultr_firewall_group.for_rancher_server_develop.id
  network           = "165.76.243.129/32"
  protocol          = "tcp"
  from_port         = 22
}
resource "vultr_firewall_rule" "https" {
  firewall_group_id = vultr_firewall_group.for_rancher_server_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 443
}
resource "vultr_firewall_rule" "http" {
  firewall_group_id = vultr_firewall_group.for_rancher_server_develop.id
  network           = "0.0.0.0/0"
  protocol          = "tcp"
  from_port         = 80
}
